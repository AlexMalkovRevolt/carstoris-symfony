<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version21190906075339 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {

        $file = @fopen("D:\myprojects\carstoris-symfony\auto_db\mirshin116_table_oc_models.txt", "r");

        //'..\\..\\auto_db\\mirshin116_table_oc_car.txt'
        while (($buffer = fgets($file)) !== FALSE) {
            $line = substr($buffer, 1);
            $auto = explode('|', $line);
            $id = $auto[0];
            $brandId = $auto[2];
            $model = $auto[3];
            $version = $auto[4];
            $productionYear = $auto[6];

            $sql = "INSERT INTO auto_model (id, brand_id, model, production_year, version) VALUES (";

            $sql .= "$id, $brandId, '$model', '$productionYear', '$version'";
            $sql .= ');';

            $this->addSql($sql);
        }

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
    }
}
