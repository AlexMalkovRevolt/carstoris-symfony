if($('#map').length > 0) {

    $('.dalnewostochnyy_fo').mouseover(function() {
        $('.dfo_shadow').css('opacity', '1');
    });
    $('.dalnewostochnyy_fo').mouseout(function() {
        $('.dfo_shadow').css('opacity', '0');
    });

    $('.yuzhnyy_fo').mouseover(function() {
        $('.yfo_shadow').css('opacity', '1');
    });
    $('.yuzhnyy_fo').mouseout(function() {
        $('.yfo_shadow').css('opacity', '0');
    });

    $('.sibirskiy_fo').mouseover(function() {
        $('.sfo_shadow').css('opacity', '1');
        $('.sfo_shadow').css('z-index', '100000');
        $(this).css('z-index', '100000');
    });
    $('.sibirskiy_fo').mouseout(function() {
        $('.sfo_shadow').css('opacity', '0');
    });

    $('.severo_zapadnyy_fo').mouseover(function() {
        $('.szfo_shadow').css('opacity', '1');
    });
    $('.severo_zapadnyy_fo').mouseout(function() {
        $('.szfo_shadow').css('opacity', '0');
    });

    $('.centralnyy_fo').mouseover(function() {
        $('.cfo_shadow').css('opacity', '1');
    });
    $('.centralnyy_fo').mouseout(function() {
        $('.cfo_shadow').css('opacity', '0');
    });

    $('.priwolzhskiy_fo').mouseover(function() {
        $('.pfo_shadow').css('opacity', '1');
    });
    $('.priwolzhskiy_fo').mouseout(function() {
        $('.pfo_shadow').css('opacity', '0');
    });

    $('.uralskiy_fo').mouseover(function() {
        $('.ufo_shadow').css('opacity', '1');
    });
    $('.uralskiy_fo').mouseout(function() {
        $('.ufo_shadow').css('opacity', '0');
    });

    var waypoint = new Waypoint({
        element: document.getElementById('map'),
        // element:document.getElementsByClassName  ('map'),
        handler: function(direction) {
            setTimeout(function() { $('.dalnewostochnyy_fo').css('fill', '#e6e6e6') }, 100);
            setTimeout(function() { $('.sibirskiy_fo').css('fill', '#cccccc') }, 400);
            setTimeout(function() { $('.uralskiy_fo').css('fill', '#e6e6e6') }, 700);
            setTimeout(function() { $('.severo_zapadnyy_fo').css('fill', '#cccccc') }, 1000);
            setTimeout(function() { $('.priwolzhskiy_fo').css('fill', '#e6e6e6') }, 1300);
            setTimeout(function() { $('.centralnyy_fo').css('fill', '#e6e6e6') }, 1600);
            setTimeout(function() { $('.yuzhnyy_fo').css('fill', '#cccccc') }, 1900);
            setTimeout(function() {
                $('.city-marker').each(function (index) {
                    var item = $(this);
                    setTimeout(function () {
                    item.css('opacity', '1');
                    }, index * 100);
                });
            }, 2200);
            setTimeout(function() {
                $('.city-text').each(function (index) {
                    var item = $(this);
                    setTimeout(function () {
                    item.css('opacity', '1');
                    }, index * 100);
                });
            }, 2400);
        },
        offset: function() {
            if ($(window).width() > 575) {
                return this.element.offsetHeight * 1.4;
            } else {
                return this.element.offsetHeight * 4;
            }
        }
    });
}