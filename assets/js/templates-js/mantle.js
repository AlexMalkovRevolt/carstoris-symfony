var data = require('./bagsColor');
let selectedItem = $('#mantleSelectedItemImg');
let directory = '/img/configurator/mantles';
var materialType = 'romb',
    okantovkaType = 'bez-ushey',
    mcolor = 'beg',
    bcolor = 'beg',
    imgUrl;

function changeM() {
    $('.mantle-material-color').attr('src', directory + '/'+ materialType + '/'+ okantovkaType +'/'+ mcolor +'.jpg');
    imgUrl = '/img/configurator/bags/material-color/' + mcolor+'.png';
    let bagMaterialColor = $('#relatedItemImg').find('.bag-material-color');
    if ( $.inArray( mcolor, data ) > -1 ) {
        bagMaterialColor.attr('src', imgUrl);
        $('.related-item-material-color').attr('value', mcolor );
    } else {
        bagMaterialColor.attr('src', '/img/configurator/bags/material-color/black.png');
        $('.related-item-material-color').attr('value', 'black' );
    }
}

function changeB() {
    let bagBorderColor =  $('#relatedItemImg').find('.bag-border-color');
    $('.mantle-border-color').attr('src', directory + '/okantovka/'+ okantovkaType +'/'+ bcolor +'.png');
    imgUrl = '/img/configurator/bags/border-color/' + bcolor+'.png';
    if ( $.inArray( bcolor, data ) > -1 ) {
        bagBorderColor.attr('src', imgUrl);
        $('.related-item-border-color').attr('value', bcolor );
    } else {
        bagBorderColor.attr('src', '/img/configurator/bags/border-color/black.png');
        $('.related-item-border-color').attr('value', 'black' );
    }
    // $('#relatedItemImg').find('.bag-border-color').attr('src','/img/configurator/bags/material-color/' + bcolor+'.png');
    // $('.related-item-border-color').attr('value', bcolor );
}

$(document).ready(function () {

    $('[name="material-color"]').click(function(){
        mcolor = this.value;
        // console.log(mcolor);
        changeM();
        // console.log( $(this).parent().find('.info-tooltip-text').html() );
        var ruMColor = $(this).parent().find('.info-tooltip-text').html();
        $('#selectedModalMaterialColor').html(ruMColor);
    });

    $('[name="border-color"]').click(function(){
        bcolor = this.value;
        changeB();
        var ruBColor = $(this).parent().find('.info-tooltip-text').html();
        $('#selectedModalBorderColor').html(ruBColor);
    });

    $('[name="okantovka-type"]').click(function(){
        okantovkaType = this.value;
        changeB();
        changeM();
    });

    $('[name="mantle-types"]').click(function(){
        materialType = this.value;
        changeM();
    });

    $('.complect-type, .okantovka-type').click(function(){
        $(this).children('input').prop('checked', true);
        let mantleComplect = $('input[name="complect-type"]:checked').attr('value');
        let mantleSlug = $('input[name="okantovka-type"]:checked').attr('slug');
        let priceId = 'price_'+ mantleComplect + '_' + mantleSlug;
        let price = $('#' + priceId).html();
        let oldPrice = $('#' + priceId).attr('old-price');
        // $('#oldPriceContent>span, #selectedOldPrice').html(oldPrice);
        $('.product-price>span>span[itemprop="price"]').attr(price);
        $('.product-price>span>span[itemprop="price"]').html(price);
       let productId = $('#' + priceId).attr('product-id');
        $('#configuratorForm').attr('product-id', productId);
        // console.log($('#configuratorForm').attr('product-id'));
        // $('#selectedNewPrice').html(price + ' <span class="small">руб.</span>');
        // console.log(mantleComplect);
        // console.log(price);
        // $('#configuratorForm').attr('product-id', mantleProductId);
    });

    $('[name="embroidery-type"]').click(function(){

        var clickedLgValue = this.value;
        if( clickedLgValue == 'my-design'){
            $('.upload-file-progress').removeClass('d-none');
            $('.upload-file-btn').removeClass('d-none');
            $('.my-design-info').removeClass('d-none');

            $('.select-design-btn').addClass('d-none');
            $('.from-catalog-info').addClass('d-none');

        } else {
            $('.upload-file-progress').addClass('d-none');
            $('.upload-file-btn').addClass('d-none');
            $('.my-design-info').addClass('d-none');

            $('.select-design-btn').removeClass('d-none');
            $('.from-catalog-info').removeClass('d-none');
        }
    });

    // $( window ).resize(function() {
        var captionText = $('.caption-text').height();
        $('.caption-bg .bg-1').height(captionText+80+'px');
    // });

    if (width > 575) {
        if($('#mantlesSwiper').length > 0){
            $('#mantlesSwiper .swiper-slide .mantles-caption-3').each(function (index) {
                $(this).find('.slider-title').splitLines({
                    width: 350,
                    height: 20,
                    tag: ' <div class="c-text"><p class="title h1-responsive">',
                });
                $("<div></div>").insertAfter($(this).find('.title'));

                let i = 0;
                let z = [];
                let totalHeight = 0;

                $(this).find('.title').each(function (index) {
                    z.push($(this).width());
                    // console.log(i + ' ' + $(this).width());
                    // console.log(z[i - 1]);
                    var xx = z[i - 1] - z[i];
                    // console.log('xx: ' + xx);
                    // console.log($(this).parent().next().find('div').html());
                    $(this).parent().find('div').css('width', xx + 'px');
                    // console.log('height: ' + $(this).parent().height());
                    totalHeight += parseInt($(this).parent().height(), 10);
                    $(this).parent().parent().parent().find('.caption-title-bg').css('height', totalHeight + 'px');
                    i++;
                });
            });
        }

        // Mantles swiper caption
        if($('#mantlesSwiper').length > 0){
            $('#mantlesSwiper .swiper-slide .caption').each(function (index) {
                var bagCaptionBgHeight = $(this).find('.container .caption-bg').height();
                $(this).find('.caption-bg-left').css('height', bagCaptionBgHeight + 'px');
            });
        }
    }

});