var data = require('./bagsColor');
// console.log(data);
var clickedMValue = 'black',
    clickedBValue = 'black',
    ruMColor,
    ruBColor,
    imgUrl,
    extraImg = '/img/configurator/carpets/',
    mantleProductId = '301';
let mdirectory = '/img/configurator/carpets/material-color/romb/';
function changeMaterial() {
    $('#carpetConfiguratorImage').find('.carpet-material-color').attr('src', mdirectory + clickedMValue+'.jpg');
    $('#carpetSelectedItemImg').find('.carpet-material-color').attr('src', mdirectory + clickedMValue+'.jpg');
    imgUrl = '/img/configurator/bags/material-color/' + clickedMValue+'.png';
    if ( $.inArray( clickedMValue, data ) > -1 ) {
        // console.log('true - image found');
        $('#relatedItemImg').find('.bag-material-color').attr('src', imgUrl);
        $('.related-item-material-color').attr('value', clickedMValue );
    } else {
        // console.log('false - image not found');
        $('#relatedItemImg').find('.bag-material-color').attr('src', '/img/configurator/bags/material-color/black.png');
        $('.related-item-material-color').attr('value', 'black' );
    }
}
function changeBorder() {
    $('#carpetConfiguratorImage').find('.carpet-border-color').attr('src','/img/configurator/carpets/border-color/'+clickedBValue+'.png');
    $('#carpetSelectedItemImg').find('.carpet-border-color').attr('src','/img/configurator/carpets/border-color/'+clickedBValue+'.png');
    imgUrl = '/img/configurator/bags/border-color/'+ clickedBValue+'.png';
    if ( $.inArray( clickedMValue, data ) > -1 ) {
        $('#relatedItemImg').find('.bag-border-color').attr('src', imgUrl);
        $('.related-item-border-color').attr('value', clickedBValue );
        // console.log('true - image found');
    } else {
        // console.log('false - image not found');
        $('#relatedItemImg').find('.bag-border-color').attr('src', '/img/configurator/bags/border-color/black.png');
        $('.related-item-border-color').attr('value', 'black' );
    }
}
function changePrice(){
    let shildik = $('#shildik');
    let podpyatnik = $('#podpyatnik');
    let carpetPriceElement = $('#selectedCarpetPrice');
    let newPrice = parseInt(carpetPriceElement.attr('content'), 10);
    if(shildik.prop('checked')){
        newPrice += parseInt(shildik.attr('price'), 10);
    }
    if(podpyatnik.prop('checked')){
        newPrice += parseInt(podpyatnik.attr('price'), 10);
    }
    return newPrice;
}

$(document).ready(function () {
    $('#shildikModal').click(function(){
        $('#selectedModalShildik').toggleClass('hidden');
    });

    $('#podpyatnik').change(function(){
        $('#selectedCarpetPrice').html(changePrice());
        if($('#podpyatnik').prop('checked')){
            $('#selectedModalPodpyatnik').removeClass('hidden');
            $('#selectedMarkaAutoText').html($('#podpyatnik').val());
        }else{
            $('#selectedModalPodpyatnik').addClass('hidden');
        }
    });


    let logoSelect = $('[name="marka-avto"]');
    $('#shildik').change( function(){
        let price = $('#shildik').attr('price');
        let productPrice = $(this).attr('content');
        $('#selectedCarpetPrice').html(changePrice());
            // console.log()
        if($(this).prop('checked')){
            $('#selectedModalShildik').removeClass('hidden');
            if(logoSelect.val() === '0'){
                logoSelect.addClass('not-valid');
                $('#buyBtn').removeClass('add-to-cart').attr('data-toggle', 'none');
            }
            else{
                $('select[name="marka-avto"]').removeClass('not-valid');
                $('#buyBtn').addClass('add-to-cart').attr('data-toggle', 'modal');
            }

        }else{
            logoSelect.removeClass('not-valid');
            $('#selectedModalShildik').addClass('hidden');
            $('#buyBtn').addClass('add-to-cart').attr('data-toggle', 'modal');
        }

        // if(logoSelect.val() === '0'){
        //     $('select[name="marka-avto"]').addClass('not-valid');
        //     $('#buyBtn').attr('data-toggle', 'none').removeClass('add-to-cart');
        // }
        // else{
        //     $('select[name="marka-avto"]').removeClass('not-valid');
        //     $('#buyBtn').attr('data-toggle', 'modal').addClass('add-to-cart');
        // }


    });

    logoSelect.change(function(){

        if($(this).val() !== '0' ){
            $('#relatedLogoValue').removeClass('d-none');
            $(this).removeClass('not-valid');
            $('#buyBtn').attr('data-toggle', 'modal').addClass('add-to-cart');
        }
        else{
            $('#relatedLogoValue').addClass('d-none');
            $(this).addClass('not-valid');
            $('#buyBtn').attr('data-toggle', 'none').removeClass('add-to-cart');
        }
    });


    $('[name="carpet-types"]').click(function(){
        if(this.value == 'romb'){
            mdirectory = '/img/configurator/carpets/material-color/romb/';
            changeMaterial();
            changeBorder();
        } else {
            mdirectory = '/img/configurator/carpets/material-color/soty/';
            mantleProductId = '302';
            $('#configuratorForm').attr('product-id', mantleProductId);
            changeMaterial();
            changeBorder();
        }
    });

    $('[name="material-color"]').click(function(){
        clickedMValue = this.value;
        changeMaterial();
        console.log( $(this).parent().find('.info-tooltip-text').html() );
        ruMColor = $(this).parent().find('.info-tooltip-text').html();
        $('#selectedModalMaterialColor').html(ruMColor);
    });

    $('[name="border-color"]').click(function(){
        clickedBValue = this.value;
        changeBorder();
        ruBColor = $(this).parent().find('.info-tooltip-text').html();
        $('#selectedModalBorderColor').html(ruBColor);
    });

    $('.checkmark').click(function(){
        let productId = $(this).siblings('input').attr('sub-product-id');
        $('form#configuratorForm').attr('product-id', productId);
    });

    $('.complect-type').click(function(){
        let price = $(this).children('input[name="complect-type"]').attr('price');

        $('#selected-params>.selected-item-params>.params-title').html($(this).children('.kit-name').text());

        $('#selectedCarpetPrice').html(price).attr('content', price);
        if($(this).children('.kit-name').text() === "Коврик в багажник" && $('#podpyatnik').prop('checked')){
            console.log($('#podpyatnik').prop('checked'));
            $('#podpyatnik').prop('checked', false);
            checkExtra('podpyatnik', '190');
        }
        mantleProductId = $(this).find('input').attr('sub-product-id');
        $('#configuratorForm').attr('product-id', mantleProductId);

        // console.log( $(this).find('input').attr('sub-product-id') );
    });

    function checkExtra(value, valuePrice) {
        $('#'+value).change(function(){
            let notInBagazhnik = (!$('.complect-type').text() === "Коврик в багажник");
            if(notInBagazhnik){
                if(this.checked){
                    $('.carpet-'+ value).attr('src', extraImg + this.value + '.png');
                    let price = $('#selectedCarpetPrice').html().replace(/\D*/gi, '');
                    let newPrice = parseInt(price) + parseInt(valuePrice);
                    $('#selectedCarpetPrice').html(newPrice);
                }
                else {
                    $('.carpet-'+ value).attr('src', '');
                    let price = $('#selectedCarpetPrice').html().replace(/\D*/gi, '');
                    let newPrice = parseInt(price) - parseInt(valuePrice);
                    $('#selectedCarpetPrice').html(newPrice);
                }
            }
        });
    }
    checkExtra('shildik', '190');
    checkExtra('podpyatnik', '590');

    if (width > 575) {
        // Main swiper caption text split
        if($('#carpetsSwiper').length > 0){
            $('#carpetsSwiper .swiper-slide').each(function (index) {
                $(this).find('.slider-title').splitLines({
                    width: 350,
                    height: 20,
                    tag: ' <div class="c-text"><p class="title h1-responsive">',
                });
                $("<div></div>").insertAfter($(this).find('.title'));

                let i = 0;
                let z = [];
                let totalHeight = 0;

                $(this).find('.title').each(function (index) {
                    z.push($(this).width());
                    // console.log(i + ' ' + $(this).width());
                    // console.log(z[i - 1]);
                    var xx = z[i - 1] - z[i];
                    // console.log('xx: ' + xx);
                    // console.log($(this).parent().next().find('div').html());
                    $(this).parent().find('div').css('width', xx + 'px');
                    // console.log('height: ' + $(this).parent().height());
                    totalHeight += parseInt($(this).parent().height(), 10);
                    $(this).parent().parent().parent().find('.caption-bg').css('height', totalHeight + 'px');
                    i++;
                });
            });

            // Mantles swiper caption
            if($('#carpetsSwiper').length > 0){
                $('#carpetsSwiper .swiper-slide .caption').each(function (index) {
                    var bagCaptionBgHeight = $(this).find('.container .caption-bg').height();
                    console.log(bagCaptionBgHeight);
                    $(this).find('.caption-bg-left').css('height', bagCaptionBgHeight + 'px');
                });
            }
        }
    }

    // shildik/podpyatnik modal show/hide
    $('#shildikInfo, #podpyatnikInfo').click(function(){
        $(this).siblings('.saddle-modal').toggleClass('d-block');
        $(this).siblings('.saddle-modal-podlozhka').addClass('d-block');
    });

    $('.saddle-modal-podlozhka').click(function(){
        $(this).siblings('.saddle-modal').removeClass('d-block');
        $(this).removeClass('d-block');
    });
});

$('#loadMoreLogo').click(function () {
    console.log('a');
    $('#selectLogoFromList').removeClass('hide-scroll');
    $(this).addClass('d-none');
    $("#selectLogoFromList").animate({scrollLeft: width}, 1000);
});