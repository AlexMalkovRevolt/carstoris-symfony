$(document).ready(function () {
    $('#podlozhka, #choseNavigatorClose').click(function () {
        $('#chooseNavigator, #podlozhka').addClass('d-none');
    });
    $('#contactAddressNavigator').click(function () {
        $('#chooseNavigator, #podlozhka').removeClass('d-none');
    });
    if($('.dashed-left-line').length > 0) {
        var title2 = $('.main-title.second').width();
        var dashedLine = $('.dashed-left-line').width();
        var containerWidth = $('.container').width();
        console.log('caption-text:' + title2);
        console.log('container:' + containerWidth);
        console.log('dashed line:' + dashedLine);
        $('.dashed-left-line').width(dashedLine - containerWidth / 2 - 5 + 'px');
    }
    if($('.carstoris-gallery').length > 0) {
        $('.carstoris-gallery').removeClass('d-none');
    }
});

