$(document).ready(function () {
    let borty =  $('.zashitnye-borty');
    $('#forSalon').click(function () {
        borty.collapse('show');
        $('#forAnimalsBagTypes').find('input').prop('disabled', false);

        if( $('input[name=for-salon-type]:checked').val() == 'for-all' ) {
            borty.removeClass('d-none');
            $('.zashitnye-borty').find('input').prop('disabled', false);
        }
    });
    $('#forTrunk').click(function () {
        $('#forAnimalsBagTypes').collapse('hide');
        $('#forAnimalsBagTypes').find('input').prop('disabled', true);

        borty.addClass('d-none');
        $('.zashitnye-borty').find('input').prop('disabled', true);
    });

    $('[name="for-salon-type"]').click(function(){
        if(this.value == 'for-all'){
            borty.removeClass('d-none');
            // $('.zashitnye-borty').addClass('d-block');
            $('.zashitnye-borty').find('input').prop('disabled', false);
        } else {
            // $('.zashitnye-borty').removeClass('d-block');
            borty.addClass('d-none');
            $('.zashitnye-borty').find('input').prop('disabled', true);
        }
    });

    $('[name="embroidery-type"]').click(function(){
        var clickedLgValue = this.value;
        if( clickedLgValue == 'my-design'){
            $('.upload-file-progress').removeClass('d-none');
            $('.upload-file-btn').removeClass('d-none');
            $('.my-design-info').removeClass('d-none');

            $('.select-design-btn').addClass('d-none');
            $('.from-catalog-info').addClass('d-none');

        } else {
            $('.upload-file-progress').addClass('d-none');
            $('.upload-file-btn').addClass('d-none');
            $('.my-design-info').addClass('d-none');

            $('.select-design-btn').removeClass('d-none');
            $('.from-catalog-info').removeClass('d-none');
        }
    });

    var captionText = $('.caption-text').height();
    console.log('caption-text:'+captionText);
    $('.caption-bg .bg-1').height(captionText+70+'px');

    if (width > 575) {
        if($('#foranimalsSwiper').length > 0){
            $('#foranimalsSwiper .swiper-slide .foranimals-caption-2').each(function (index) {
                $(this).find('.slider-title').splitLines({
                    width: 350,
                    height: 20,
                    tag: ' <div class="c-text"><p class="title h1-responsive">',
                });
                $("<div></div>").insertAfter($(this).find('.title'));

                let i = 0;
                let z = [];
                let totalHeight = 0;

                $(this).find('.title').each(function (index) {
                    z.push($(this).width());
                    // console.log(i + ' ' + $(this).width());
                    // console.log(z[i - 1]);
                    var xx = z[i - 1] - z[i];
                    // console.log('xx: ' + xx);
                    // console.log($(this).parent().next().find('div').html());
                    $(this).parent().find('div').css('width', xx + 'px');
                    // console.log('height: ' + $(this).parent().height());
                    totalHeight += parseInt($(this).parent().height(), 10);
                    $(this).parent().parent().parent().find('.caption-title-bg').css('height', totalHeight + 'px');
                    i++;
                });
            });
        }

        // Mantles swiper caption
        if($('#foranimalsSwiper').length > 0){
            $('#foranimalsSwiper .swiper-slide .caption').each(function (index) {
                var bagCaptionBgHeight = $(this).find('.container .caption-bg').height();
                console.log(bagCaptionBgHeight);
                $(this).find('.caption-bg-left').css('height', bagCaptionBgHeight + 'px');
                $(this).find('.caption-bg-right').css('height', bagCaptionBgHeight + 'px');
            });
        }
    }
});
