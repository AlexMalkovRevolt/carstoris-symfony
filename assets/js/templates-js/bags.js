var mcolor, 
     tcolor, 
     bcolor,
     imgUrl,
     imageCheck = new Image();
let logoPrice = 300;
$(document).ready(function () {

     $('[name="material-color"]').click(function(){
          var clickedMValue = this.value;
          $('#bagConfiguratorImage').find('.bag-material-color').attr('src','/img/configurator/bags/material-color/'+clickedMValue+'.png');
          $('#bagSelectedItemImg').find('.bag-material-color').attr('src','/img/configurator/bags/material-color/'+clickedMValue+'.png');
          imgUrl = '/img/configurator/folders/without_zipper/material-color/'+clickedMValue+'.png';
          imageCheck.src = imgUrl;
          imageCheck.onload = function() {
               $('#relatedItemImg').find('.folder-material-color').attr('src', imgUrl);
               $('.related-item-material-color').attr('value', clickedMValue );

          }
          imageCheck.onerror = function() {

               $('#relatedItemImg').find('.folder-material-color').attr('src', '/img/configurator/folders/without_zipper/material-color/black.png');
               $('.related-item-material-color').attr('value', 'black' );
          }


          var ruMColor = $(this).parent().find('.info-tooltip-text').html();
          $('#selectedModalMaterialColor').html(ruMColor);
     });

     $('[name="thread-color"]').click(function(){
          var clickedTValue = this.value;
          $('#bagConfiguratorImage').find('.bag-thread-color').attr('src','/img/configurator/bags/thread-color/'+clickedTValue+'.png');
          $('#bagSelectedItemImg').find('.bag-thread-color').attr('src','/img/configurator/bags/thread-color/'+clickedTValue+'.png');
          imgUrl = '/img/configurator/folders/without_zipper/thread-color/'+clickedTValue+'.png';
          imageCheck.src = imgUrl;
          imageCheck.onload = function() {
               $('#relatedItemImg').find('.folder-thread-color').attr('src', imgUrl);
               $('.related-item-thread-color').attr('value', clickedTValue );

          }
          imageCheck.onerror = function() {

               $('#relatedItemImg').find('.folder-thread-color').attr('src', '/img/configurator/folders/without_zipper/thread-color/black.png');
               $('.related-item-thread-color').attr('value', 'black' );
          }
          // $('#relatedItemImg').find('.folder-thread-color').attr('src','/img/configurator/folders/without_zipper/thread-color/'+clickedTValue+'.png');
          var ruTColor = $(this).parent().find('.info-tooltip-text').html();
          $('#selectedModalThreadColor').html(ruTColor);
     });

     $('[name="border-color"]').click(function(){
          var clickedBValue = this.value;
          $('#bagConfiguratorImage').find('.bag-border-color').attr('src','/img/configurator/bags/border-color/'+clickedBValue+'.png');
          $('#bagSelectedItemImg').find('.bag-border-color').attr('src','/img/configurator/bags/border-color/'+clickedBValue+'.png');
          imgUrl = '/img/configurator/folders/without_zipper/border-color/'+clickedBValue+'.png';
          imageCheck.src = imgUrl;
          imageCheck.onload = function() {
               $('#relatedItemImg').find('.folder-border-color').attr('src', imgUrl);
               $('.related-item-border-color').attr('value', clickedBValue );

          }
          imageCheck.onerror = function() {

               $('#relatedItemImg').find('.folder-border-color').attr('src', '/img/configurator/folders/without_zipper/border-color/black.png');
               $('.related-item-border-color').attr('value', 'black' );
          }
          // $('#relatedItemImg').find('.folder-border-color').attr('src','/img/configurator/folders/without_zipper/border-color/'+clickedBValue+'.png');
          var ruBColor = $(this).parent().find('.info-tooltip-text').html();
          $('#selectedModalBorderColor').html(ruBColor);
     });
     let isPriceChanged = false;
     $('[name="auto-brand"]').click(function(){
          var clickedLgValue = this.value;
          let price = + $('#productPrice').html();
          let oldPrice = + $('#productOldPrice').html().replace(/\D*/gi, '');
          if( clickedLgValue == 'without_logo'){
               $('#bagConfiguratorImage').find('.bag-logo').attr('src','');
               $('#bagSelectedItemImg').find('.bag-logo').attr('src','');
               $('#relatedItemImg').find('.folder-logo').attr('src','');
               $('.related-add-logo').addClass('d-none');
               $('.related-add-logo').find('input').prop('disabled', true);
               // $('.related-item-brand-logo').attr('value', 'without_logo' );
               if(isPriceChanged){
                    $('#productOldPrice').html((oldPrice - (logoPrice ) ) + ' руб.');
                    $('#productOldPrice').attr(('content', oldPrice + logoPrice) + ' руб.');
                    $('#productPrice').html(price - logoPrice);
                    $('#productPrice').attr('content', price + logoPrice);
               }
               isPriceChanged = false;
          } else {
               $('#bagConfiguratorImage').find('.bag-logo').attr('src', '/img/configurator/bags/auto-brands/' + clickedLgValue + '.png');
               $('#bagSelectedItemImg').find('.bag-logo').attr('src', '/img/configurator/bags/auto-brands/' + clickedLgValue + '.png');
               $('#relatedItemImg').find('.folder-logo').attr('src', '/img/configurator/folders/auto-brands/' + clickedLgValue + '.png');
               $('#relatedItemImg').find('.folder-logo').addClass('d-none')
               $('#relatedItemImg').find('.folder-logo').css('top','15%');
               $('.related-add-logo').removeClass('d-none');
               $('.related-add-logo').find('input').prop('disabled', false);
               // $('.related-item-brand-logo').attr('value', clickedLgValue );
               if(!isPriceChanged){
                    $('#productOldPrice').html((oldPrice + logoPrice ) + ' руб.');
                    $('#productOldPrice').attr(('content', oldPrice + logoPrice ) + ' руб.');
                    $('#productPrice').html(price + logoPrice);
                    $('#productPrice').attr('content', price + logoPrice);
               }
               isPriceChanged = true;
          }
     });

     if (width > 575) {
          // Bags swiper caption
          if($('#bagsSwiper').length > 0){
               var bagCaptionBgHeight = $('#bagsSwiper').find('.bags-caption-2 .container .caption-bg').height();

               $('#bagsSwiper').find('.caption-bg-left').css('click   ', bagCaptionBgHeight + 40 + 'px');
          }
     }
     $('#relatedLogoValue').on('click','input[type="checkbox"]',function(){
          let time = new Date();
          let relatedPrice = parseInt($('#relatedPrice').html());
          let relatedItemLogo =  $('#relatedItemImg').find('.folder-logo');
          relatedItemLogo.toggleClass('d-none');
          if(relatedItemLogo.hasClass('d-none')){
               $('#relatedPrice').html(relatedPrice - logoPrice);
          }
          else{
               $('#relatedPrice').html(relatedPrice + logoPrice);
          }
     });

});


function configColorImage(el, image) {
     var clickedBValue = el.value;
     $('#bagConfiguratorImage').find('.bag-border-color').attr('src','/img/configurator/bags/border-color/'+clickedBValue+'.png');

}

