var mcolor, tcolor, bcolor;
let logoPrice = 300;
$(document).ready(function () {
     $('.select-colors').removeClass('d-none');
     let selectedItem = $('#folderSelectedItemImg');
     let directory = '/img/configurator/folders/with_zipper';
     let zipper = 'with_zipper';
     var clickedMValue = 'black';
     var clickedTValue = 'black';
     var clickedBValue = 'black';
     $('[name="zipper"]').click(function(){
          zipper = $(this).attr('value');
          let wrapper = zipper.replace(/_zipper/g, '' )
         $('.folder-price-wrapper').addClass('hidden');
         $('#' + wrapper + 'ZipperWrapper').removeClass('hidden');
         $('#configuratorForm').attr('product-id', $(this).attr('product-id'));
         directory = '/img/configurator/folders/' + zipper;
         let configuratorImage = $('#folderConfiguratorImage');
         configuratorImage.find('.folder-material-color').attr('src', directory + '/material-color/'+ clickedMValue + '.png');
         configuratorImage.find('.folder-thread-color').attr('src', directory + '/thread-color/'+ clickedTValue + '.png');
         configuratorImage.find('.folder-border-color').attr('src', directory + '/border-color/'+ clickedBValue + '.png');
         selectedItem.find('.folder-thread-color').attr('src', directory + '/thread-color/' + clickedTValue + '.png');
         selectedItem.find('.folder-material-color').attr('src', directory + '/material-color/' + clickedMValue + '.png');
         selectedItem.find('.folder-border-color').attr('src', directory + '/border-color/' + clickedBValue + '.png');
         $('#relatedItemImg').find('.bag-material-color').attr('src','/img/configurator/bags/material-color/' +  clickedMValue + '.png');
     });
     $('[name="material-color"]').click(function(){
          clickedMValue = this.value;
          $('#folderConfiguratorImage').find('.folder-material-color').attr('src', directory + '/material-color/' + clickedMValue + '.png');
          selectedItem.find('.folder-material-color').attr('src', directory + '/material-color/' + clickedMValue +'.png');
          $('.related-item-material-color').attr('value', clickedMValue );
          $('#relatedItemImg').find('.bag-material-color').attr('src','/img/configurator/bags/material-color/' + clickedMValue + '.png');
          var ruMColor = $(this).parent().find('.info-tooltip-text').html();
          $('#selectedModalMaterialColor').html(ruMColor);
     });
     $('[name="thread-color"]').click(function(){
          clickedTValue = this.value;
          $('#folderConfiguratorImage').find('.folder-thread-color').attr('src', directory + '/thread-color/'+ clickedTValue + '.png');
          selectedItem.find('.folder-thread-color').attr('src', directory + '/thread-color/'+clickedTValue +'.png');
          $('.related-item-thread-color').attr('value', clickedTValue );
          $('#relatedItemImg').find('.bag-thread-color').attr('src','/img/configurator/bags/thread-color/' + clickedTValue + '.png');
          var ruTColor = $(this).parent().find('.info-tooltip-text').html();
          $('#selectedModalThreadColor').html(ruTColor);
     });
     $('[name="border-color"]').click(function(){
          clickedBValue = this.value;
          $('#folderConfiguratorImage').find('.folder-border-color').attr('src',directory + '/border-color/' + clickedBValue+'.png');
          selectedItem.find('.folder-border-color').attr('src',directory + '/border-color/'+clickedBValue+'.png');
          $('.related-item-border-color').attr('value', clickedBValue );
          $('#relatedItemImg').find('.bag-border-color').attr('src','/img/configurator/bags/border-color/' + clickedBValue+'.png');
          var ruBColor = $(this).parent().find('.info-tooltip-text').html();
          $('#selectedModalBorderColor').html(ruBColor);
     });
     let isPriceChanged = false;

     $('[name="auto-brand"]').click(function(){
          let clickedLgValue = this.value;
          let price = + $('#productPrice').html();
          let oldPrice = + $('#productOldPrice').html().replace(/\D*/gi, '');
         let price2 = + $('#product2Price').html();
         let oldPrice2 = + $('#product2OldPrice').html().replace(/\D*/gi, '');
          let relatedAddLogo = $('.related-add-logo');
          if( clickedLgValue == 'without_logo'){
              $('#folderConfiguratorImage').find('.folder-logo').attr('src','');
              selectedItem.find('.folder-logo').attr('src','');
              $('#relatedItemImg').find('.bag-logo').attr('src','');
              relatedAddLogo.addClass('d-none');
              relatedAddLogo.find('input').prop('disabled', true);
               $('#selectedModalLogo').html('Без логотипа');
               if(isPriceChanged){
                  $('#productOldPrice').html((oldPrice - (logoPrice + 200)) + ' руб.');
                  $('#productPrice').html(price - logoPrice);
                   $('#product2OldPrice').html((oldPrice2 - (logoPrice + 200)) + ' руб.');
                   $('#product2Price').html(price2 - logoPrice);
               }
               isPriceChanged = false;
          } else {
              $('#folderConfiguratorImage').find('.folder-logo').attr('src', '/img/configurator/folders/auto-brands/' + clickedLgValue + '.png');
              // $('#folderConfiguratorImage').find('.folder-logo').css({'top':'4%', 'width': '32em', 'left': '-9%'});
              selectedItem.find('.folder-logo').attr('src', '/img/configurator/folders/auto-brands/' + clickedLgValue + '.png');
              $('#folderSelectedItemImg').find('.folder-logo').css({'top':'4%', 'width': '16em', 'left': '-9%'});
              $('#relatedItemImg').find('.bag-logo').attr('src', '/img/configurator/bags/auto-brands/' + clickedLgValue + '.png');
              relatedAddLogo.removeClass('d-none');
              let logoValue = $(this).attr('value');
              $('.product-price>span').html();
              $('#selectedModalLogo').html(logoValue.charAt(0).toUpperCase() + logoValue.slice(1));
              relatedAddLogo.find('input').prop('disabled', false);
              if(!isPriceChanged){
                   $('#productOldPrice').html((oldPrice + logoPrice + 200) + ' руб.');
                   $('#productPrice').html(price + logoPrice);
                  $('#product2OldPrice').html((oldPrice2 + logoPrice + 200) + ' руб.');
                  $('#product2Price').html(price2 + logoPrice);
               }
              isPriceChanged = true;
          }
     });

    // $('#ModalSelectedIrem a.btn-decrease, #ModalSelectedIrem a.btn-increase').click(function () {
    //     let $itemId = $(this).attr('item-id');
    //     let action =  $(this).attr('class').replace('btn-', '')+ '-quantity';
    //     let $inputQuantity = $(this).parent().find('input');
    //     // console.log($inputQuantity);
    //     let $inputV1 = parseInt(
    //         $inputQuantity.val(),
    //         10
    //     );
    //     // let $inputV1 = parseInt($inputQuantity.val(), 10)+1;
    //     if(action === 'increase-quantity'){
    //         $inputV1 ++ ;
    //     }
    //     else{
    //         if(parseInt($inputQuantity.val())!== 1){
    //             $inputV1 -- ;
    //         }
    //         else{
    //             return;
    //         }
    //     }
    //     // console.log($inputQuantity);
    //     let dataToSend = 'item-id='+$itemId+'&action='+action+'&quantity='+'1';
    //     // console.log(dataToSend);
    //     $.post('/shoppingcart', dataToSend, function(result){
    //         // console.log(result);
    //         result = JSON.parse(result);
    //         // console.log(result);
    //         $('#menuCartItemsQuantity').html(result.itemsQuantity);
    //         $('#menuCartItemsAmount').html(result.cartAmount + ' руб.');
    //
    //         $inputQuantity.val(result.items[$itemId].quantity);
    //
    //         $('#SelectedOldPrice').html((result.items[$itemId].product.priceWithoutDiscount + result.items[$itemId].shieldPrice)*result.items[$itemId].quantity);
    //         $('#selectedNewPrice').html(result.items[$itemId].price*result.items[$itemId].quantity +  '<span class="small"> руб.</span>' ) ;
    //         if($('#cartCommonDiscount')){
    //             $('#cartCommonDiscount').html(result.cartAmountOldPrice - result.cartAmount);
    //             $('#cartCommonAmount').html(result.cartAmount);
    //         }
    //     });
    // });

     var captionText = $('.caption-text').height();
     $('.caption-bg .bg-1').height(captionText+70+'px');
     $('#selectLogo').css('height', 'auto');

    if (width > 575) {
        // Main swiper caption text split
        if($('#foldersSwiper').length > 0){
            // Mantles swiper caption
            if($('#foldersSwiper').length > 0){
                $('#foldersSwiper .swiper-slide .caption').each(function (index) {
                    var bagCaptionBgHeight = $(this).find('.container .caption-bg').height();
                    console.log(bagCaptionBgHeight);
                    $(this).find('.caption-bg-left').css('height', bagCaptionBgHeight + 'px');
                });
            }
        }
    }

    $('#relatedLogoValue').click(function(){
        let relatedPrice = parseInt($('#relatedPrice').html());
        let relatedItemLogo =  $('#relatedItemImg').find('.bag-logo');
        relatedItemLogo.toggleClass('d-none');
        if(relatedItemLogo.hasClass('d-none')){
            $('#relatedPrice').html(relatedPrice - logoPrice);
        }
        else{
            $('#relatedPrice').html(relatedPrice + logoPrice);
        }
    });
});