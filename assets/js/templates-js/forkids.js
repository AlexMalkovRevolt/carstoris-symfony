var data = require('./bagsColor');
let selectedItem = $('#for_kidsSelectedItemImgs');
// let directory = '/img/configurator/for_kids/na-spinku',
let directory = '/img/configurator/for_kids/' + $('.for_kids-material-color').attr('src').replace('/img/configurator/for_kids/', '').replace('/material-color/black.jpg', ''),
    bagtype = 'pod-kreslo';

var clickedMValue = 'black',//Выбранные цвет материала
    clickedTValue = 'black',//Выбранный цвет нити
    clickedBValue = 'black',//Выбранный цвет окантовки
    imgUrl;

// Change material color
function changeMaterial() {
    $('.for_kids-material-color').attr('src', directory + '/material-color/'+clickedMValue+'.jpg');
    selectedItem.find('.for_kids-material-color').attr('src', directory + '/material-color/'+clickedMValue+'.jpg');
    imgUrl = '/img/configurator/bags/material-color/'+clickedMValue+'.png';
    if ( $.inArray( clickedMValue, data ) > -1 ) {
        $('#relatedItemImg').find('.bag-material-color').attr('src', imgUrl);
        $('.related-item-material-color').attr('value', clickedMValue );
        // console.log('image found');
    } else {
        // console.log('image not found');
        $('#relatedItemImg').find('.bag-material-color').attr('src', '/img/configurator/bags/material-color/black.png');
        $('.related-item-material-color').attr('value', 'black' );
    }
    // $('#relatedItemImg').find('.bag-material-color').attr('src', imgUrl);
    var ruMColor = $('.info-tooltip-text.sc-'+clickedMValue).html();
    $('#selectedModalMaterialColor').html(ruMColor);
}

// Change thread color
function changeThread() {
    $('.for_kids-thread-color').attr('src', directory + '/thread-color/'+clickedTValue+'.png');
    selectedItem.find('.for_kids-thread-color').attr('src', directory + '/thread-color/'+clickedTValue+'.png');
    imgUrl = '/img/configurator/bags/thread-color/'+clickedTValue+'.png';
    if ( $.inArray( clickedTValue, data ) > -1 ) {
        $('#relatedItemImg').find('.bag-thread-color').attr('src', imgUrl);
        $('.related-item-thread-color').attr('value', clickedTValue );
        // console.log('image found');
    } else {
        // console.log('image not found');
        $('#relatedItemImg').find('.bag-thread-color').attr('src', '/img/configurator/bags/thread-color/black.png');
        $('.related-item-thread-color').attr('value', 'black' );
    }
    // $('#relatedItemImg').find('.bag-thread-color').attr('src','/img/configurator/bags/thread-color/'+clickedTValue+'.png');
    var ruTColor = $('.info-tooltip-text.sc-'+clickedTValue).html();
    $('#selectedModalThreadColor').html(ruTColor);
}

// Change border color
function changeBorder() {
    $('.for_kids-border-color').attr('src', directory + '/border-color/'+clickedBValue+'.png');
    $('#for_kidsrSelectedItemImgs').find('.for_kids-border-color').attr('src', directory + '/border-color/'+clickedBValue+'.png');
    imgUrl = '/img/configurator/bags/border-color/'+clickedBValue+'.png';
    if ( $.inArray( clickedBValue, data ) > -1 ) {
        $('#relatedItemImg').find('.bag-border-color').attr('src', imgUrl);
        $('.related-item-border-color').attr('value', clickedBValue );
        // console.log('image found');
    } else {
        // console.log('image not found');
        $('#relatedItemImg').find('.bag-border-color').attr('src', '/img/configurator/bags/border-color/black.png');
        $('.related-item-border-color').attr('value', 'black' );
    }
    // $('#relatedItemImg').find('.bag-border-color').attr('src','/img/configurator/bags/border-color/'+clickedBValue+'.png');
    var ruBColor = $('.info-tooltip-text.sc-'+clickedBValue).html();
    $('#selectedModalBorderColor').html(ruBColor);
}

// Change material-thread-borer color
function changeAll() {
        changeMaterial();
        changeThread();
        changeBorder();
}

// Search on div input with selected color and add attribute checked also class active
function compareSelection(value) {
    // $('#selectColor .select-colors label input').each(function (index) {
    $(value).each(function (index) {
        $(this).removeAttr('checked');
        $(this).parent().removeClass('active');
        if( $(this).attr('value') == clickedMValue ) {
            $(this).parent().addClass('active');
            $(this).prop('checked', true);
        }
    });
}

$(document).ready(function () {

    $('[name="material-color"]').click(function(){
            clickedMValue = this.value;
        changeMaterial();
    });

    $('[name="thread-color"]').click(function(){
        clickedTValue = this.value;
        changeThread();
    });

    $('[name="border-color"]').click(function(){
        clickedBValue = this.value;
        changeBorder();
    });

    // if selected bag-type will change form product-id with selected product id
    $('.complect-type').click(function(){
        var mantleProductId2 = $(this).find('input').attr('sub-product-id');
        $('#configuratorForm').attr('product-id', mantleProductId2);
    });

    // Если изменяется bag-type изменяются картинки и цены по акции
    $('[name="bag-type"]').change(function(){
        if($(this).val() === 'pod-kreslo'){
            $('#forkidsDescriptionNaSpinku').addClass('hidden');
            $('#forkidsDescriptionPodKreslo').removeClass('hidden');
            $('#promoForBack').addClass('hidden');
            $('#promoUnderBabyChair').removeClass('hidden');
        }
        else{
            $('#forkidsDescriptionNaSpinku').removeClass('hidden');
            $('#forkidsDescriptionPodKreslo').addClass('hidden');
            $('#promoForBack').removeClass('hidden');
            $('#promoUnderBabyChair').addClass('hidden');
        }
        bagtype = $(this).attr('value');
        directory = '/img/configurator/for_kids/' + bagtype;
        changeAll();
        let prodId = $(this).attr('sub-product-id');
        $('div.product-price-block').addClass('hidden');
        $('#productPriceBlock'+prodId).removeClass('hidden');
    });
    $('[name="mantle-style"]').click(function(){
        if(this.value == 'in-stock'){
            clickedTValue = clickedMValue;
            clickedBValue = clickedMValue;
            changeAll();

            // Search on div input with selected color and add attribute checked also class active
            compareSelection('#inStockTab .select-colors label input');

            // Switch tabs
            $('#inStockTab').addClass('active show');
            $('#selectColor').removeClass('active show');

            // Disable/Enable input files according active tab
            // Remove checked attr from input of non-active tab
            $('#inStockTab').find('input').prop('disabled', false);
            $('#selectColor').find('input').prop('disabled', true);
            $('#selectColor').find('input').removeAttr('checked');

            // .selected-color for in-stock selection, on select material color thread and border colors will be set and saved here
            $('#inStockTab .selected-color').prop('disabled', false);
            $('#inStockTab .selected-color').prop('checked', true);
            $('#inStockTab .selected-color').val(clickedMValue);

            // add attr checked to input with same value of selected color
            $('#inStockTab label input').each(function (index) {
                if( $(this).attr('value') == clickedMValue ) {
                    $(this).prop('checked', true);
                }
            });

        } else {
            // Switch tabs
            $('#selectColor').addClass('active show');
            $('#inStockTab').removeClass('active show');

            // Disable/Enable input files according active tab
            // Remove checked attr from input of non-active tab
            $('#inStockTab').find('input').prop('disabled', true);
            $('#inStockTab').find('input').removeAttr('checked');
            $('#selectColor').find('input').prop('disabled', false);

            // .selected-color disable, remove attr
            $('#inStockTab .selected-color').prop('disabled', true);
            $('#inStockTab .selected-color').removeAttr('checked');

            // Search on div input with selected color and add attribute checked also class active
            compareSelection('#selectColor .select-colors label input');
        }
    });

    $('#inStockTab [name="material-color"]').click(function(){
        clickedTValue = clickedMValue;
        clickedBValue = clickedMValue;
        changeAll();

        // Search on div input with selected color and add attribute checked also class active
        compareSelection('#selectColor .select-colors label input');

        // add to clicked attr checked
        $(this).prop('checked', true);

        // .selected-color enable, add attr
        $('#inStockTab .selected-color').val(clickedMValue);
        $('#inStockTab .selected-color').prop('checked', true);
    });

    if (width > 575) {
        if($('#forkidsSwiper').length > 0){
            $('#forkidsSwiper .swiper-slide .forkids-caption-2').each(function (index) {
                $(this).find('.slider-title').splitLines({
                    width: 350,
                    height: 20,
                    tag: ' <div class="c-text"><p class="title h1-responsive">',
                });
                $("<div></div>").insertAfter($(this).find('.title'));

                let i = 0;
                let z = [];
                let totalHeight = 0;

                $(this).find('.title').each(function (index) {
                    z.push($(this).width());
                    var xx = z[i - 1] - z[i];
                    $(this).parent().find('div').css('width', xx + 'px');
                    totalHeight += parseInt($(this).parent().height(), 10);
                    $(this).parent().parent().parent().find('.caption-title-bg').css('height', totalHeight + 'px');
                    i++;
                });
            });
        }

        // Mantles swiper caption
        if($('#forkidsSwiper').length > 0){
            $('#forkidsSwiper .swiper-slide .caption').each(function (index) {
                var bagCaptionBgHeight = $(this).find('.container .caption-bg').height();
                console.log(bagCaptionBgHeight);
                $(this).find('.caption-bg-left').css('height', bagCaptionBgHeight + 'px');
            });
        }
    }
});
