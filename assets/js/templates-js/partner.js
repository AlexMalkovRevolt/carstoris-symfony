$(document).ready(function () {
    $('#sendEditPartnerInfo').click( function(){
        // $('.pim-main').addClass('d-none');
        // $('.pim-message').removeClass('d-none');
        // $('#editPartnerInfoModal .modal-title').html('');
        let token = $('input[name="token"]').val();
        let message = $('#partnerEditInfoMessage').val();

        let url = '/partner/change-info-request';
        let data = 'token=' + token + '&&message=' + message;
        $.post(url, data, function(result){
            console.log(result);
        });
    });

    $('#promoMaterialRequest').click(function(){

    });
});
const writeBtn = document.querySelector('#linkReadBtn');
const inputEl = document.querySelector('#linkInputToCopy');

writeBtn.addEventListener('click', () => {
    const inputValue = inputEl.value;
    if (inputValue) {
        navigator.clipboard.writeText(inputValue)
            .then(() => {

                if (writeBtn.innerHTML !== 'Скопировано!') {

                    writeBtn.innerHTML = 'Скопировано!';
                    setTimeout(() => {
                        writeBtn.innerHTML = '<i class="fas fa-copy"></i>';
                    }, 1500);
                }
            })
            .catch(err => {
                console.log('Something went wrong', err);
            })
    }
});