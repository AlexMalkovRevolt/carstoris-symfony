$(document).ready(function () {
    $('#adminPartners').DataTable({
        "paging": false,
        "info": false,
        // "searching": false
        "language": {
            "zeroRecords": "Ничего не найдено"
        }
    });

    /**
     * Поиск партнеров в списке.
     * @type {jQuery}
     */
    partnersTable =  $('#adminPartners').DataTable();
    $('#tableSearch').keyup(function(){
        partnersTable.search($(this).val()).draw();
        let icon =  $(this).parent().find('.icon');
        if($(this).val().length  == 0){
            addRemoveClass(icon, 'icon-search', 'icon-cross' );
        } else {
            addRemoveClass(icon, 'icon-search', 'icon-cross' );
            icon.click(function () {
                $('#tableSearch').val('');
            });
        }
    })
    /**
     * //TODO: Не понятно, что делает функция. Нужно расписать. Ощущение, что она не доработана.
     *
     */
    $('#tableSearch').focusout(function () {
        let icon =  $(this).parent().find('.icon');
        if($(this).val().length  == 0){
            addRemoveClass(icon, 'icon-search', 'icon-cross' );
        }
    });

    /**
     * Удаление партнера из спаска.
     * //TODO: Нужно изменить логику. Партнер не должен удаляться. В базе лишь убирается отметка isActive/
     *
     */
    var item, data;

    $('a.p-delete').click(function(){
        item =  $(this).parent('td').parent('tr').attr('id').replace(/partner/, '');
        console.log(item);
        let ptitle = $(this).parent().find('span').html();
        $('#pTitle').html(ptitle);
        // item = $(this).parent().parent();
    });

    $('a.partner-list-item-delete').click(function(){
        let url = '/admin';
        data = 'id='+item;
        console.log(data);
        $.post(url, data, function(answer){
                console.log(answer);
                if(answer == 'ok'){
                    item = $('tr#partner'+item);
                    item.remove();
                    $('#deleteConfirmationModal').modal('hide');
                    $('#adminPartners').find('tr').each(function(i, v) {
                        $(v).find('.sorting_1').text(i);
                    });
            }
            else{
                alert('К сожалению что то пошле не так. Мы работаем над этой проблемой. Попробуйте позже.');
            }
        });
    });

    $('#editAdminModalSubmit').click(function () {
        let data = $('#editAdminModalForm').serialize();
        let url = '/admin/edit';
        $.post(url, data, function(result){
            console.log(result);
            if(result === 'ok'){
                console.log(data);
                $('#topMenuUserEmail').html($('#editAdminModalEmail').val());
                $('#editAdminModal').modal('hide');
            }
        });
    });

    $('#adminBonusPayBtn').click(function () {
        let bonusesIds = $('#adminPartnerListForm').serialize().replace(/partners-table=/g , '').replace(/&/, ',');
            $.post('/admin', 'bonus-ids=' + bonusesIds ,function(result){
            result = JSON.parse(result);
            result.forEach(function(item){
                let amount = $('#listToPay' + item).html();
                $('#listPayed' + item).html(amount);
                $('#listToPay'+ item).html(0);
                $('#partnerListCheckBox' + result).prop('checked', false);
                $('#partnerListCheckBox' + result).prop('disabled', true)
            });
        });
    });

    /**
     * Добавление и удаление классов.
     * @param object
     * @param addClass
     * @param removeClass
     */
    function addRemoveClass(object, addClass = null, removeClass = null){
        if(addClass != null){
            object.addClass(addClass);
        }
        if(removeClass != null){
            object.removeClass(removeClass);
        }
    }

    $('#add_partner_form_promocodeDiscount, #edit_partner_form_promocodeDiscount').keyup(function(){
        let val =  $(this).val();
        $(this).val(val.replace(/\D/g, '' ));
    });

    $('#add_partner_form_promocode').blur(function(){
        let value = $(this).val();
        // console.log(value);return;
        if(value !== ''){
            let url = '/admin/get-promocode/'+value;
            // if(value !== undefined){}
            $.get(url, function(result){
                console.log(result)
                if(result === 'false'){
                    $('#add_partner_form_promocode').addClass('error-input');
                    $('#add_partner_form_savePartner').prop('disabled', true);
                    $('label[for="add_partner_form_promocode"]').addClass('text-danger').html('Такой промокод уже есть, выберите другой');
                }
            });
        }
    });

    $('#add_partner_form_promocode').focus(function(){
        $(this).removeClass('error-input');
        $('#add_partner_form_savePartner').prop('disabled', false);
        $('label[for="add_partner_form_promocode"]').removeClass('text-danger').html('Промо код');
    });

    $('#adminPartners .edit-partner').click(function(){

        let partner = $(this).parents('tr').attr('id').replace(/partner/, '');
        let url = '/admin/partner-get/' + partner;
        $('#edit_partner_form_password').prop('required', false);
        $('label[for="edit_partner_form_password"]').removeClass('required');
        $.get(url, function(result){
            result = JSON.parse(result);
            console.log(result);
            if(result.status == 'success'){
                $('#adminEditPartnerModal#adminEditPartnerModal').modal('show');
                for (var key in result.data ) {
                    let input = $('#adminEditPartnerModal').find('#edit_partner_form_' + key);
                    input.val(result.data[key]);
                    input.prop('required', false);
                    $('label[for="edit_partner_form_' + key + '"]').removeClass('required');
                    $('form[name="edit_partner_form"]').attr('action', '/admin/partner-edit/' + partner);
                }
                $("#adminEditPartnerModal input#add_partner_form_promocode").keyup(function(){
                    $("#adminEditPartnerModal input#add_partner_form_linkCode").val("/promocode=" + this.value);
                });
            }
            else {
                alert('Sorry Error');
            }

        });
        console.log(partner);
    });



});

