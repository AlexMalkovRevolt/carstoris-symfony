if($('.fp.swiper-container').length > 0){
    var swiper = new Swiper('.fp.swiper-container', {
        autoHeight: true,
        loop: true,
        pagination: {
            el: '.fp.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return '<span class="' + className + '"><div></div></span>';
            },
        },
    });
}


if($('.cc-swiper').length > 0){
    var swiper = new Swiper('.cc-swiper', {
        spaceBetween: 30,
        loop:true,
        navigation: {
            nextEl: '.cc-next',
            prevEl: '.cc-prev',
        },
    });
}

if($('.swiper_groups').length > 0) {
    var swiper = new Swiper('.swiper_groups', {
        slidesPerView: 4,
        spaceBetween: 30,
        slidesPerGroup: 4,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.swiper_pagination_groups',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper_button_next',
            prevEl: '.swiper_button_prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },

        }
    });
}

function elementActivate(element) {
    element.siblings().removeClass('active');
    element.addClass('active');
}

$(document).ready(function () {
    width = document.body.clientWidth;
    if (width < 992) {
        $('.top-menu .navbar-nav').html('' +
            '<li data-toggle="collapse" data-target="#catalog" class="nav-item" aria-expanded="true">' +
            '<a href="#" class="nav-link catalog-menu active" id="catalogMenuTitle">Каталог <span class="icon icon-arrow-down"></span></a>' +
            '</li>' +
            '<ul class="sub-menu collapse show" id="catalog">' + $('.top-menu .navbar-nav').html() + '</ul>'+
            '<li data-toggle="collapse" data-target="#infoMenu" class="nav-item collapsed">' +
            '<a href="#" class="nav-link catalog-menu" id="infoMenuTitle">Информация <span class="icon icon-arrow-down"></span></a>' +
            '</li>' +
            '<ul class="sub-menu collapse" id="infoMenu">' +  $('.main-menu .navbar-nav').html() + '</ul>');
        $('.main-menu').addClass('d-none');
        $('#infoMenuTitle').click( function () {
            $('#catalog').collapse('hide');
            $('#catalogMenuTitle').removeClass('active');
        });
        $('#catalogMenuTitle').click( function () {
            $('#infoMenu').collapse('hide');
            $('#infoMenuTitle').removeClass('active');
        });
    }

    $('#addressTopMenu').keyup( function(){

        let searchKey = $(this).val();
        let token = $('#changeCityToken').val();
        let data = '?token=' + token + '&search-key=' + searchKey ;
        $.post('/location/search-location', data, function(result){
            $('#suggestionResult').html('').removeClass('d-none');
            result = JSON.parse(result);
            let locations = result.suggestions;
            locations.forEach(function(location){
            $('#suggestionResult').append(
                '<div class="change-city-result change-city-top-menu">'+location.value+'</div>'
            );
            });

        });
    });

    $('#addressTopMenu').focusout(function(){
        $('#suggestionResult').addClass('d-none');
    });

    // $('#changeYourCityModalTopMenu').on('mousedown', '.change-city-top-menu' ,function () {
    //     let city = $(this).text();
    //     let token = $('#changeCityToken').val();
    //     let data = '?index='+$(this).attr('index')+'&city=' + city + '&token=' + token;
    //     console.log(data);
    //     $.post('/location/change', data, function(result){
    //        result = JSON.parse(result);
    //         console.log(result);
    //         $('#topMenuChangeCityBtn>span').text(result.data.city_with_type);
    //         $('#changeYourCityModalTopMenu').modal('hide');
    //     });
    // });

    // $('#changeYourCityModalTopMenu').click(function(){
    //     let suggestionInput = $('#selectYourCityModal').children('input[name="adress"]').detach();
    //
    //     $('#changeYourCityModalTopMenu .modal-body').prepend(suggestionInput);
    // });


    $('.radio-select label').click(function(){
        elementActivate($(this));
    });
    $('.radio-img label').click(function(){
        elementActivate($(this));
    });
    $('.select-logo .logo-item').click(function () {
        $('.select-logo').find('.logo-item').removeClass('active');
        $(this).addClass('active');
    });

    $(".modal-white-backdrop").on('show.bs.modal',
        function(e){
                $('body').addClass("modal-white-backdrop")
            }).on('hide.bs.modal',function(e){
                window.setTimeout(function(){
                    $('body').removeClass("modal-white-backdrop")
                }, 500);
    });

    // If anchor add offset and smooth effect
    $('.anchor').click(function() {
        var offset = -200; // <-- change the value here
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top + offset
                }, 1000);
                return false;
            }
        }
    });

    /**
     * Menu sub menu open on mobile
     */
    if (width < 992) {
        $('.sub-nav').addClass('d-none')
    }

    $('.open-menu').click(function () {
        $(this).parent().find('.sub-nav').toggleClass('d-block d-none');
        $('.sub-menu-icon', this).toggleClass('open');
        });

    /**
     * добавление продукта в корзину по клику кнопки "купить".
     */
    $('#addRelatedProduct').click(function(e){
        e.preventDefault();
        $(this).children('.add-related-text').html('Добавлено');
        let dataToSend = $('#addRelatedProductForm').serialize();
        let productId = $('#addRelatedProductForm').attr('product-id');
        dataToSend += '&product-id=' + productId + '&action=add-item';
        dataToSend =  dataToSend.replace(/(related-item-)/g, '');

        $.post('/cart/', dataToSend, function(result){

            result = JSON.parse(result);
            $('#menuCartItemsQuantity').html(result.itemsQuantity);
            $('#menuCartItemsAmount').html(result.cartAmount + ' руб.');
            //
        });
    });

    $('button.add-to-cart').click(function(e){
        e.preventDefault();
        let dataToSend = $('#configuratorForm').serialize();
        let productId = $('#configuratorForm').attr('product-id');
        let images = {};
        console.log( $('.configurator-images .image-group').children())
        $('.configurator-images .image-group').children().each( function(index, value){
            let  key = $(this).attr('src').replace('/img/configurator/', '').replace(/^[a-z-_]+\//, '').replace(/\/.+$/, '');
            //

            if(key !== ""){
                images[key] = {};
             images[key]['path'] = $(this).attr('src');
             images[key]['class'] = $(this).attr('class');
            }
        });
        // console.log(JSON.stringify(images));
        dataToSend += '&images=' + JSON.stringify(images);
        $('#addRelatedProduct').children('.add-related-text').html('Добавить');
        console.log(dataToSend);
        if($('select[name="marka-avto"]').val() !== '0'){
            if($('select[name="marka-avto"]').val()){
                $('#selectedModalMarkaAuto').removeClass('hidden');
                $('#selectedMarkaAutoText').html($('select[name="marka-avto"]').val());
                $('#relatedLogoValue').removeClass('hidden');
            }
        else{
                $('#relatedLogoValue').addClass('hidden');
                $('#selectedModalMarkaAuto').addClass('hidden');

            }
        }

        // if($('.select-selected').text() !== 'Не выбрано'){
        //
        // }

        dataToSend += '&product-id=' + productId + '&action=add-item';
        // console.log(dataToSend)
        $.post('/cart/', dataToSend, function(result){
        // console.log(result);return;
            result = JSON.parse(result);
        // console.log(result);return;
            // // var lastEditedItem = result.lastEditedItem;
            let $itemId = $('#ModalSelectedItem .item-quantity>a').attr('item-id');
            let $inputQuantity = $('#ModalSelectedItemQuantityInput');
            $inputQuantity.val(result.items[result.lastEditedItem].quantity);
            // console.log(result.items[result.lastEditedItem]);
            $('#menuCartItemsQuantity').html(result.itemsQuantity);
            $('#menuCartItemsAmount').html(result.cartAmount + ' руб.');
            $('#selectedItemModal .loader-block').addClass('d-none');
            $('#selectedItemModal .btn-increase, #selectedItemModal .btn-decrease').attr(
                    'item-id',
                result.lastEditedItem
            );
            let logoPrice = (result.items[result.lastEditedItem].withLogo)? result.items[result.lastEditedItem].shieldPrice : 0;
            changeSelectedModalPrice(result, logoPrice);
        });



        // Selected item
        if (width > 576){
            window.setTimeout(function(){
                var getDiscountselectedItemParams = 0;
                var selectedParamsH = $('#selected-params').height();
                var selectedItemParams = $('.selected-item-params').height();
                $('.selected-item-price').css('margin-top', '-' + (selectedParamsH - selectedItemParams - 30) + 'px');
            }, 500);
        }
    });
    function changeSelectedModalPrice(result, logoPrice = 0){
        $('#selectedOldPrice').html((+ result.items[result.lastEditedItem].product.priceWithoutDiscount + logoPrice) * result.items[result.lastEditedItem].quantity);
        $('#selectedNewPrice').html((result.items[result.lastEditedItem].price) *  result.items[result.lastEditedItem].quantity + '<span class="small"> руб.</span>' );
    }

    // Increasing/Decreasing number of related item
    $('#relatedItemBtnIncrease, #relatedItemBtnDecrease').click(function () {
        $('#addRelatedProduct').children('.add-related-text').html('Добавить');
        let inputQuantity = $('#relatedItemQuantity');
        let action =  $(this).attr('class').replace('related-item-btn-', '') + '-quantity';
        let inputV1 = parseInt(
            inputQuantity.val(),
            10
        );

        if (action === 'increase-quantity') {
            inputV1++;
        } else {
            if (parseInt(inputQuantity.val()) !== 1) {
                inputV1--;
            } else {
                return;
            }
        }
        inputQuantity.val(inputV1);
    });

    $('#getDiscount, #akciiGetDiscount, #orderWithDiscount').click(function(){
        let form = $(this).parents('form');
        let phoneInput = $('input[form-data="phone'+form.attr('id')+'"]');
        let nameInput = $('input[form-data="name'+form.attr('id')+'"]');
        let data = form.serialize();
        let phone = phoneInput.val();
        let name = nameInput.val();
        nameInput.removeClass('not-valid');
        phoneInput.removeClass('not-valid');
        if(!nameValidation(name)){
            nameInput.addClass('not-valid'); return
        }
        if(!phoneValidation(phone)){
            phoneInput.addClass('not-valid'); return
        }

        let url = '/mailer/discount_modal';

        $.post(url, data, function(result){
            if(result ==='SKIDKA'){
                $('#discountInfoModal .text-center').html('<p class="h1 line-height-1-2 mb-3">Промо-код на скидку</p>\n' +
                    '                <a href="#!" class="h1 pink-link text-uppercase">skidka</a>\n' +
                    '                    <p class="lead line-height-1-2 mt-3 mb-5 mx-4">Используйте этот промо код при оплате заказа</p>\n' +
                    '                <a href="#!" class="btn btn-main text-sentence-case mx-4 waves-effect waves-light" data-dismiss="modal">Спасибо, понятно</a>');
                $('#discountInfoModal').modal();
                $('#discountModal').modal('hide');

            }

        });
    });
    $('input.not-valid').focus(function () {
        $(this).removeClass('not-valid');
    });

    $('.nav-link').click(function(){
        $('input.not-valid').removeClass('not-valid');
    });


    $('#callMeModalBtn, #contactsWaitForCallBtn, #becomeDealerModalBtn, #callMeButton, #bePartnerModalBtn').click(function(){
        let form = $(this).parents('form');
        let data = form.serialize();
        let modal = form.parents('.modal');

        let phoneInput = $('input[form-data="phone' + form.attr('id') + '"]');
        let nameInput = $('input[form-data="name'+form.attr('id')+'"]');
        console.log(phoneInput.val())
        let phone = phoneInput.val();
        let name = nameInput.val();
        phoneInput.removeClass('not-valid');
        nameInput.removeClass('not-valid');
        console.log(name);
        console.log(phone)
        if(!nameValidation(nameInput.val())){
            nameInput.addClass('not-valid'); return
        }
        if(!phoneValidation(phone)){
            phoneInput.addClass('not-valid'); return
        }
        let url = '/mailer/call_me_modal';
        if(form.attr('id') === 'becomeDealerModalForm'){
            url = '/mailer/become_dealer_modal';
        }
        if(form.attr('id') === 'bePartnerModalForm'){
            url = '/mailer/become_partner_modal';
        }

        $.post(url, data, function(result){
            if(result === 'SKIDKA'){
                $('#discountInfoModal .text-center').html('Ваша заявка прнята, наш менеджер свяжется с Вами в ближайщее время');
                $('#discountInfoModal').modal();
                modal.modal('hide');
            }
        });
    });

    $('#orderWithDiscount').click(function(){
        let data = $('#orderWithDiscountForm').serialize();
        let phone = $('#orderWithDiscountPhone').val();
        $('#orderWithDiscountPhone').removeClass('not-valid');
        if(!phoneValidation(phone)){

            $('#orderWithDiscountPhone').addClass('not-valid'); return
        }

        let url = '/mailer/order-with-discount';
        $.post(url, data, function(result){
            $('#discountInfoModal .text-center').html(' <p class="lead line-height-1-2 mb-3">Если у Вас нет промо кода со скидкой, воспользуйтесь этим</p>\n' +
                '            <a href="#!" class="lead pink-link text-uppercase">skidka</a>\n' +
                '                <p class="lead line-height-1-2 mt-3 mb-5 mx-4">Используйте этот код при оплате заказа и получите скидку.</p>\n' +
                '            <a href="#!" class="btn btn-main text-sentence-case mx-4 waves-effect waves-light" data-dismiss="modal">Спасибо, понятно</a>');
            $('#discountInfoModal').modal();
        });
    });
    $('a.btn-decrease, a.btn-increase').click(function () {
        let $itemId = $(this).attr('item-id');
        let action =  $(this).attr('class').replace('btn-', '')+ '-quantity';

        let $inputQuantity = $(this).parent().find('input');

        let dataToSend = 'item-id='+$itemId+'&action='+action+'&quantity=1';

            $.post('/cart/', dataToSend, function(result){

                result = JSON.parse(result);

                $('#menuCartItemsQuantity').html(result.itemsQuantity);
                $('#menuCartItemsAmount').html(result.cartAmount + ' руб.');
                $inputQuantity.val(result.items[result.lastEditedItem].quantity);
                $('#oldPrice' + result.lastEditedItem).html(result.items[result.lastEditedItem].product.priceWithoutDiscount*result.items[result.lastEditedItem].quantity);
                $('#newPrice' + result.lastEditedItem).html(result.items[result.lastEditedItem].price*result.items[result.lastEditedItem].quantity + '<span class="small"> руб.</span>' ) ;
                if($('#cartCommonDiscount')){
                    $('#cartCommonDiscount').html(result.cartAmountOldPrice - result.cartAmount);
                    $('#cartCommonAmount').html(result.cartAmount);
                }
                 changeSelectedModalPrice(result);
            });
     });

    $('a.remove-item').click(function () {
        $itemId = $(this).attr('item-id');
        let dataToSend = 'item-id='+$itemId+'&action=remove-item';
            $.post('/cart/', dataToSend, function(result){
                result = JSON.parse(result);
                if(result.item){
                    result.items.forEach(function(item){
                        reducePrice(item.id, item.price);
                    });
                    $('#menuCartItemsQuantity').html(result.itemsQuantity);
                    $('#menuCartItemsAmount').html(result.cartAmount  + ' руб.' );
                    $('#cartCommonDiscount').html(result.cartAmountOldPrice - result.cartAmount);
                    $('#cartCommonAmount').html(result.cartAmount + '<span class="small"> руб.</span>');
                    $('.shopping-cart-item[item-id="'+$itemId+'"] ').remove();
                }
                else{
                    location.reload();
                }
            });

        });

    function reducePrice( id,  price){


        if($('#newPrice' + id).html()){
            let oldPrice = $('#newPrice' + id).html().replace('<span class="small">руб.</span>', '');
            if(oldPrice !== price){
                $('#newPrice' + id).html(price + '<span class="small">руб.</span>');
            }
        }
    }

    $('#modalCartItemDecrease').click(function(){
        let value = $()
    });

    // if .select-colors exists
    if( $('.select-colors').length ) {
        $('.select-colors').removeClass('d-none');
    }

    if( $('#map').length > 0 && width > 991 ){
        var mapHeight = $('#map svg').height();

        $('.map_text').height(mapHeight - 70 +'px');
    }

    var logoBlockWidth = $('#selectColor').width();
    var logoBlockHeight = $('#selectColor').height();
    if( $('#selectLogo').length > 0 && width < 768 ){

        $('.auto-logo').width(logoBlockWidth/3-7+'px');
        // if screen width less than 768 set height of without-logo same as logo-images
        var autoLogoHeight = ($('#selectLogo').outerHeight()-34)/6-17;
        $('.without-logo').css( 'height', autoLogoHeight+'px' );

    }
    if( $('#selectLogo').length > 0 ){


    }

    //menu button toggle class open
    $('.menu-button').on('click', function () {
        $('.animated-menu-icon', this).toggleClass('open');
    });

    //add active class to catalog m
    $('.catalog-menu').on('click', function () {
        $(this).toggleClass('active');
    });

    // Telephone number input mask
    $(function() {
        $('.phone-no').mask('+7(999)999-99-99');
    });
    $('.btn-add-input').click(function () {
        let quantity =  $(this).siblings('input').val();
        // console.log(quantity);
    });

    // Scroll to top
    $('.back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });

    function phoneValidation(phone){
        let phoneNew = phone.replace(/\D/g, '');
        if(phoneNew.length < 11){
            return false;
        }
        else{
            return true;
        }
    }
    function nameValidation(name){

        let nameNew =  name.replace(/[^a-zа-яЁ]/gi, '');
        if(nameNew.length < 1){
            return false;
        }
        else{
            return true;
        }
    }
    // OnScroll close menu
    $(window).scroll(function(){
        $('#navbar1').collapse('hide');
        $(".menu-button .animated-menu-icon").removeClass("open");
    });

    // OnClick outside of menu close it
    var specifiedElement = document.getElementById('topMenu');
    //I'm using "click" but it works with any event
    document.addEventListener('click', function(event) {
        var isClickInside = specifiedElement.contains(event.target);
        if (!isClickInside) {
            //the click was outside the specifiedElement, do something
            // console.log('not topMenu');
            $('#navbar1').collapse('hide');
            // $("#navbar1").removeClass("show");
            $(".menu-button .animated-menu-icon").removeClass("open");
        }
    });

    if (width > 575) {
        // Main swiper caption text split
        if($('#mainSwiper').length > 0){
            $('#mainSwiper .swiper-slide').each(function (index) {
                let title =  $(this).find('.slider-title');

                $(this).find('.slider-title').splitLines({
                    width: 350,
                    height: 20,
                    tag: ' <div class="c-text c-text'+ index +'" ><p class="title h1-responsive">',
                });


                let i = 0;
                let z = [];
                let totalHeight = 0;

                $(this).find('.slider-title').attr('sork', 'sork')

                $(this).find('.title').each(function (index) {
                    $(this).attr('count', index);
                    z.push($(this).width());
                });
                $('<div style="padding-top:10px; "><button style="display:block; margin: auto;" class=\"btn btn-main waves-effect waves-light\"  data-toggle="modal" data-target="#discountModal">получить скидку<span class="line1"></span><span class="line2"></span></button></div>').insertAfter($(this).find('.title[count="1"]'));
                let allWidth = $(this).find('.title.h1-responsive[count="0"]').width();
                let secondWidth = $(this).find('.title.h1-responsive[count="1"]').width();

                $(this).find('.title').each(function (index) {
                    var xx = (z[i - 1] - z[i]) * 4;
                    if(xx > 470){
                        xx = allWidth - secondWidth;
                    }
                    $(this).parent().find('div').css('width', xx + 'px');
                    totalHeight += parseInt($(this).parent().height(), 10);
                    if (width > 1500){
                        // totalHeight += 1;
                    }
                    let sliderHeight = $(this).parent().parent().parent().find('.slider-title').height();
                    // console.log(sliderHeight);
                    $(this).parent().parent().parent().find('.caption-bg').css('height', sliderHeight   + 'px');
                    i++;
                });

            });
        }
    }

    // when hover #selectLogo stop scrolling window and scroll div content
    if (width > 575) {
        if ($('#selectLogo').length > 0) {
            $('#selectLogo').mouseover(function () {
                let item = $('#selectLogo .select-logo')[0];
                item.addEventListener('wheel', function (e) {
                    if (e.deltaY > 0) {
                        item.scrollLeft += 200;
                        e.preventDefault();
                    } else {
                        item.scrollLeft -= 200;
                        e.preventDefault();
                    }
                });
            });
        }
    }

    $('.gift-image').bind('click', function(){
        event.preventDefault();
        console.log($(this))
        let input =$(this).children('label').children('input');
        if(input.prop('checked')){
             input.prop('checked', false);
        }
        else{
            input.prop('checked', true);
        }
        let giftId = input.attr('value');
        let url = '/cart/gift/' + giftId;
        $.post(url, function(result){
            result = JSON.parse(result);
            $('#menuCartItemsQuantity').html(result.cartQuantity);
        });
    });


});



$('input[type="tel"]').click(function(){

    let checks = [
        /[+]/, /\d/, /\(/, /\d/, /\d/, /\d/, /\)/, /\d/, /\d/, /\d/, /-/, /\d/, /\d/, /-/, /\d/, /\d/
    ];
    let input = $(this);
    let inputValue =  input.val().split('');

    let caretPosition = 16;
        for(let i = 1; i < inputValue.length; i++ ){
            if(i !== inputValue.length){
               if(inputValue[i].search(checks[i]) === -1){
                    caretPosition = i;
                    break;
                }
            }

    }

     let range = document.createRange();
    setCaretPosition(this,caretPosition, caretPosition);
});

navigator.clipboard.writeText('Hello Alligator!')
    .then(() => {
        // Получилось!
    })
    .catch(err => {
        console.log('Something went wrong', err);
    });

$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 500) {
        $(".back-to-top").addClass("open");
    } else {
        $(".back-to-top").removeClass("open");
    }
});
function getCaretPosition(ctrl) {
    if (document.selection) {
        ctrl.focus();
        var range = document.selection.createRange();
        var rangelen = range.text.length;
        range.moveStart('character', -ctrl.value.length);
        var start = range.text.length - rangelen;
        return {
            'start': start,
            'end': start + rangelen
        };
    } else if (ctrl.selectionStart || ctrl.selectionStart == '0') {
        return {
            'start': ctrl.selectionStart,
            'end': ctrl.selectionEnd
        };
    } else {
        return {
            'start': 0,
            'end': 0
        };
    }
}


function setCaretPosition(ctrl, start, end) {
    if (ctrl.setSelectionRange) {
        ctrl.focus();
        ctrl.setSelectionRange(start, end);
    } else if (ctrl.createTextRange) {
        var range = ctrl.createTextRange();
        range.collapse(true);
        range.moveEnd('character', end);
        range.moveStart('character', start);
        range.select();
    }
};

