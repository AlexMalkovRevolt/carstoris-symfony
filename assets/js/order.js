// Telephone number input mask
$(function() {
    $('.phone-no').mask('+7(999)999-99-99');
});

$(document).ready(function () {
    /* Show/Hide delivery form */
    let isDeliveryPaymentAdded = false;
    let oldPriceAmount = $('#orderOldPriceAmount>span').html()*1;
    let orderAmountToPay = $('#orderAmountToPay>span').html()*1;
    let deliveryPrice = 100;
    $('[name="delivery-method"]').click(function(){
        if(this.value == 'delivery'){
            $('#deliveryToDoor').collapse('show');
            $('#street, #building').prop('disabled', false);
            if(!isDeliveryPaymentAdded){
                orderAmountToPay = orderAmountToPay+deliveryPrice;
                $('#orderAmountToPay>span').html(orderAmountToPay);
                $('#amountDeliveryBlock').removeClass('hidden');
                oldPriceAmount += deliveryPrice;
                isDeliveryPaymentAdded = true;
            }
        } else {
            $('#deliveryToDoor').collapse('hide');
            $('#street, #building').prop('disabled', true);
            if(isDeliveryPaymentAdded){
                orderAmountToPay = orderAmountToPay-deliveryPrice;
                $('#orderAmountToPay>span').html(orderAmountToPay);
                $('#amountDeliveryBlock').addClass('hidden');
                oldPriceAmount -= deliveryPrice;
                isDeliveryPaymentAdded = false;
            }
        }
    });

    $('[name="apartment-type"]').click(function(){
        let officeNo = $('#deliveryToDoor').find('input[name="office-no"]');
        let apartmentNo = $('#deliveryToDoor').find('[name="apartment-no"]');
        console.log(apartmentNo.attr('id'))
        if(this.value == 'office'){
            officeNo.removeClass('d-none');
            officeNo.addClass('d-block');
            apartmentNo.removeClass('d-block');
            apartmentNo.addClass('d-none');
            officeNo.prop('disabled', false);
            apartmentNo.prop('disabled', true);
        } else if(this.value == 'apartment') {
            apartmentNo.removeClass('d-none');
            apartmentNo.addClass('d-block');
            officeNo.removeClass('d-block');
            officeNo.addClass('d-none');
            officeNo.prop('disabled', true);
            apartmentNo.prop('disabled', false);
        }
    });

    // $('[name="apartment-type"]').click(function(){
    //     let officeNo = $('#officeNo');
    //     let apartmentNo = $('#apartmentNo');
    //     if(this.value == 'office'){
    //         officeNo.removeClass('d-none').addClass('d-block').prop('disabled', false);
    //         apartmentNo.removeClass('d-block').addClass('d-none').prop('disabled', true);
    //
    //     } else if(this.value == 'apartment') {
    //         apartmentNo.removeClass('d-none').addClass('d-block').prop('disabled', false);
    //         officeNo.removeClass('d-block').addClass('d-none').prop('disabled', true);
    //     }
    // });

    $('div.promocode_block').on('click', 'a.promocode_btn',  function(){

        let promocode = $('input[name="promocode"]').val();
        let dataToSend = 'promocode=' + promocode;
            console.log(dataToSend);
        $.post(
            '/order/code-activation/',
            dataToSend,
            function(result){
                console.log(result)
                console.log(result);
                result = JSON.parse(result);
               console.log(result);
               if(result.cartAmount){
                   $('input[name="promocode"], #promoActivationBtn').parent('div').addClass('d-none');
                   $('#promoActivationBtn').parent('div').parent().parent().html('<p>Вы ввели промокод ' + result.promocode
                      + '</p><p style="font-size: .8rem">Скидка по промокоду составляет ' +
                      + result.promoCodeDiscount + '</p>' +
                      ' <div class="row"> <div class="col-12"><p class="text-bold mb-2 mr-4">Ввести другой промокод</p>' +
                      ' </div> <div class="col-md-8 col-12 mb-3 mb-md-0"> <input type="text" class="form-control" name="promocode">' +
                      ' </div> <div class="col-md-4 col-12 text-right">' +
                      '<a href="#!" class="btn btn-outline-red waves-effect waves-light promocode_btn" id="promoActivationBtn">активировать</a>' +
                      '</div></div>' );
                   let va = $('.content.main').find('#promoActivationBtn');
                   $('#orderOldPriceAmount>span').html(result.cartAmountOldPrice);
                   $('#orderDiscountAmount>span').html(result.cartAmountOldPrice - result.cartAmount);
                   $('#orderAmountToPay>span').html(result.cartAmount);
               }
               else{
                   console.log('Partner Not Found');
               }

                /**
                 * cartAmount: 3100
                 * cartAmountOldPrice: 4700
                 */
            });
    });
    let otherCities = $('#deliveryToOtherCities');
    function changeCity() {
        let pickupKazan = $('#deliveryKazan');
        pickupKazan.find('input').prop('disabled', true);
        // pickupKazan.removeClass('d-inline');
        pickupKazan.addClass('d-none');
        otherCities.removeClass('d-none');
        otherCities.addClass('d-block');
    }

    $('.select-other-city').click(function(){
        changeCity();
        var t = $(this).text();

        $('.current-city').val('Россия, ' + t);
    });
    $('#topMenuChangeCityBtn').click(function(){
        $('#address').attr('trigeredby', 'top-menu')
    });

    $('.cities-modal-choose').click(function(){
        let postCode = $(this).attr('index');
        let url = '/order/cdek/pvz/';
        let city = $(this).text();
        if(address.attr('trigeredby') === 'top-menu'){
            setCity(city);
            $('#selectYourCityModal').modal('hide');
            return;
        }
        let data = 'citypostcode=' + postCode + '&city=' + city;
        let cost;
        let myMap;
        let openedBaloon;
        let spinner = '<div class="spinner-grow" role="status" style="font-size: 0.5em">\n' +
            '<span class="sr-only">Загрузка...</span>\n' +
            '</div>';
        if(postCode !== '420000'){
            $('#CCPrice').html(spinner);
            $('#CDPrice').html(spinner);
            $('#cdekMap ymaps').remove();


            $.post(url, data, function(result){
                $('#cdekMap').css({"width": "100%", "height" : "450px"});

                result = JSON.parse(result);


                if(result.answer === 'error'){

                }
                let cityCords = [result.cords.latitude, result.cords.longitude];
                myMap  = new ymaps.Map("cdekMap", {
                    center: cityCords,
                    zoom: 10
                });
                myMap.setCenter([result.cords.latitude, result.cords.longitude]);
                pvzs = result.message;
                cost = result.cost;

                if(result.answer === 'success'){
                    let pvzsList = '';
                    pvzs.forEach(function(pvz, index) {
                        if(pvz.coordX !== undefined) {
                            let placemarkName = 'placemark' + pvz.Code;
                            window[placemarkName] = new ymaps.Placemark([pvz.coordY, pvz.coordX]);
                            myMap.geoObjects.add(window[placemarkName]);
                            let nearestStation = "";
                            if (pvz.NearestStation) {
                                nearestStation = '<li><b>Ближайщее метро:</b> ' + pvz.NearestStation + '</li>';
                            }
                            let addressComment = '';
                            if (pvz.AddressComment) {
                                addressComment = '<li><b>Как найти:</b> ' + pvz.AddressComment + '</li>';
                            }
                            window[placemarkName].events.add('click', function (e) {
                                if (openedBaloon !== undefined) {
                                    openedBaloon.close();
                                }

                                // Создание независимого экземпляра балуна и отображение его в центре карты.
                                var balloon = new ymaps.Balloon(myMap);
                                // Здесь родительскими устанавливаются опции карты,
                                // где содержатся значения по умолчанию для обязательных опций.

                                balloon.options.setParent(myMap.options);

                                // Открываем балун месте размещения метки.
                                selectedPlacemark = pvz;
                                let selectBtn = '';
                                if (window.location.pathname !== '/contacts/') {
                                    selectBtn = '<a class="btn btn-outline-red waves-effect waves-light cdekmap-placemark-btn"' +
                                        'id="placemarkSelectBtn' + pvz.Code + '" href="#!">Выбрать</a>';
                                }

                                balloon.open(e.get('coords'), {
                                    contentHeader: 'г. ' + pvz.City + ': ' + pvz.Name,
                                    contentBody: '<ul>' +
                                        '<li><b>Адрес:</b> ' + pvz.Address + '</li>' +
                                        '<li><b>Телефон:</b> ' + pvz.Phone + '</li>' +
                                        nearestStation +
                                        '<li><b>Режим работы:</b> <ul>' +
                                        '<li>Пн-Пт: ' + pvz.WorkTimeY[0].periods.replace('\/', ' -     ') + '</li>' +
                                        // '<li>Вт: ' + pvz.WorkTimeY[1].periods + '</li>' +
                                        // '<li>Ср: ' + pvz.WorkTimeY[2].periods + '</li>' +
                                        // '<li>Чт: ' + pvz.WorkTimeY[3].periods + '</li>' +
                                        // '<li>Пт: ' + pvz.WorkTimeY[4].periods + '</li>' +
                                        // '<li><span class="red_text">Сб: ' + pvz.WorkTimeY[5].periods + '</span></li>' +
                                        '<li><span class="red_text">Сб-Вс: ' + pvz.WorkTimeY[5].periods.replace('\/', ' - ') + '</span></li>' +

                                        '</ul></li>' +
                                        addressComment +
                                        '</ul>' + selectBtn,
                                });

                                if (window.location.pathname !== '/contacts/') {

                                }

                                openedBaloon = balloon;
                            });

                            let ID = pvz.Code;

                            let pvzListItem = '<div class="card z-depth-0 bordered"> ' +
                            '<div class="card-header p-0" id="heading' + ID + '">' +

                            '<h5 class="mb-0 p-0">' +
                            '<button class="btn btn-link collapsed" style="width:90%" type="button" data-toggle="collapse"' +
                            'data-target="#collapse' + ID + '" aria-expanded="false" aria-controls="collapse' + ID + '">' +
                            'г. ' + pvz.City + ': ' + pvz.Address +
                            '</button>' +
                            '<span><label class="circle-radio-btn d-inline m-0" >\n' +
                            '<input id="input' + pvz.Code + '" type="radio" name="cdek-pvz-celect" value="' + pvz.Code + '">\n' +
                            '<span class="checkmark"></span>\n' +
                            '</label></span>' +
                            '</h5>' +
                            '</div>' +
                            '<div id="collapse' + ID + '" class="collapse" aria-labelledby="headingThree" data-parent="#accordionCdekList">' +
                            '<div class="card-body">' +
                            '<ul>' +
                            '<li><b>Адрес:</b> ' + pvz.Address + '</li>' +
                            '<li><b>Телефон:</b> ' + pvz.Phone + '</li>' +
                            nearestStation +
                            '<li><b>Режим работы:</b>' +
                            ' <ul>' +
                            '<li>Пн-Пт: ' +  (pvz.WorkTimeY[0] !== undefined)? pvz.WorkTimeY[0].periods.replace('\/', ' -     '): '-' + '</li>' +
                            '<li><span class="red_text">Сб-Вс: ' + (pvz.WorkTimeY[5] !== undefined)? pvz.WorkTimeY[5].periods.replace('\/', ' - ') : '-' + '</span></li>' +
                                '</ul>' +
                                '</li>' +
                                addressComment +
                                '</ul>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            pvzsList += pvzListItem;
                        }
                    });
                    $('#accordionCdekList').html(pvzsList);
                    $('div.card-body>p.text-bold').html('Выберите ближайший пункт выдачи');

                }
                else{
                    $('div.card-body>p.text-bold').html(
                        '<span class="red_text">'+result.message+'</span>'
                    );
                }

                $('#CCPrice').html(cost.cc.price);
                $('#CDPrice').html(cost.cd.price);
                let deliveryDate = new Date(cost.cd.deliveryDateMax);
                let options = {
                    month:'long',
                    day:'numeric',
                    weekday:'long',
                };
                $('#deliveryDate').html(deliveryDate.toLocaleString('ru', options));
                $('#selectedCity').attr('value', result.city.country + ', ' +  result.city.cityName);
            });
            $('#selectYourCityModal').modal('hide');
            $('#street').removeClass('collapse');
            changeCity();
        }else{
            $('#selectYourCityModal').modal('hide');
        }

    });

    let address = $("#address");
    let myMap;
    let pvzs;
    let cost;
    let selectedPlacemark;
    let openedBaloon;
    let street = $('input[name="street"]');
    address.suggestions({
        token: "711dc3fd59333801797a21bffa56c0f9a5a34769",
        type: "ADDRESS",
        hint: false,
        bounds: "city",
        constraints: {
            label: "",
            locations: {
                city_type_full: "город"
            }
        },
        /* Вызывается, когда пользователь выбирает одну из подсказок */
        onSelect: function(suggestion){

                // console.log(suggestion);return;
            let selectedCity = suggestion.data;
            if(address.attr('trigeredby') === 'top-menu'){
                setCity(suggestion.data.city);
                $('#selectYourCityModal').modal('hide');
                return;
            }
            if(suggestion.data.postal_code !== '420000'){
                $('#cityPostcode').attr('value',suggestion.data.postal_code);
                let url = '/order/cdek/pvz/';
                let data = 'citypostcode=' + selectedCity.postal_code;

                $('#cdekMap ymaps').remove();
                let spinner = '<div class="spinner-grow" role="status" style="font-size: 0.5em">\n' +
                              '<span class="sr-only">Загрузка...</span>\n' +
                              '</div>';

                $('#CCPrice').html(spinner); //Вставляем прелодер в цену доставки до терминала
                $('#CDPrice').html(spinner); //Вставляем прелодер в цену доставки до двери
                $('#deliveryDate').html(spinner); //Вставляем прелодер в дату доставки
                $.post(url, data, function(result){
                    $('#cdekMap').css({"width": "100%", "height" : "450px"});
                    result = JSON.parse(result);

                    let cityCords = [result.cords.latitude, result.cords.longitude];

                    if(selectedCity.postal_code === 190000 && selectedCity.postal_code === 101000){
                        cityCords = $('.cities-modal-choose[index="' + selectedCity.postal_code + '"]').attr('index').split(',');

                    }
                    myMap  = new ymaps.Map("cdekMap", {
                        center: cityCords,
                        zoom: 10
                    });
                    myMap.setCenter(cityCords);
                    pvzs = result.message;
                    cost = result.cost;

                    if(result.answer === 'success'){
                        let pvzsList = '';

                        pvzs.forEach(function(pvz) {
                            if(pvz.coordX !== undefined) {

                                let placemarkName = 'placemark' + pvz.Code;
                                console.log(placemarkName);
                                window[placemarkName] = new ymaps.Placemark([pvz.coordY, pvz.coordX]);
                                myMap.geoObjects.add(window[placemarkName]);
                                let nearestStation = "";
                                if (pvz.NearestStation) {
                                    nearestStation = '<li><b>Ближайщее метро:</b> ' + pvz.NearestStation + '</li>';
                                }
                                let addressComment = '';
                                if (pvz.AddressComment) {
                                    addressComment = '<li><b>Как найти:</b> ' + pvz.AddressComment + '</li>';
                                }
                                window[placemarkName].events.add('click', function (e) {
                                    if (openedBaloon !== undefined) {
                                        openedBaloon.close();
                                    }

                                    // Создание независимого экземпляра балуна и отображение его в центре карты.
                                    var balloon = new ymaps.Balloon(myMap);
                                    // Здесь родительскими устанавливаются опции карты,
                                    // где содержатся значения по умолчанию для обязательных опций.

                                    balloon.options.setParent(myMap.options);

                                    // Открываем балун месте размещения метки.
                                    selectedPlacemark = pvz;
                                    let selectBtn = '';
                                    if (window.location.pathname !== '/contacts/') {
                                        selectBtn = '<a class="btn btn-outline-red waves-effect waves-light cdekmap-placemark-btn"' +
                                            'id="placemarkSelectBtn' + pvz.Code + '" href="#!">Выбрать</a>';
                                    }
                                    balloon.open(e.get('coords'), {
                                        contentHeader: 'г. ' + pvz.City + ': ' + pvz.Name,
                                        contentBody: '<ul>' +
                                            '<li><b>Адрес:</b> ' + pvz.Address + '</li>' +
                                            '<li><b>Телефон:</b> ' + pvz.Phone + '</li>' +
                                            nearestStation +
                                            '<li><b>Режим работы:</b> <ul>' +
                                            '<li>Пн-Пт: ' + pvz.WorkTimeY[0].periods.replace('\/', ' -     ') + '</li>' +
                                            // '<li>Вт: ' + pvz.WorkTimeY[1].periods + '</li>' +
                                            // '<li>Ср: ' + pvz.WorkTimeY[2].periods + '</li>' +
                                            // '<li>Чт: ' + pvz.WorkTimeY[3].periods + '</li>' +
                                            // '<li>Пт: ' + pvz.WorkTimeY[4].periods + '</li>' +
                                            // '<li><span class="red_text">Сб: ' + pvz.WorkTimeY[5].periods + '</span></li>' +
                                            '<li><span class="red_text">Сб-Вс: ' + pvz.WorkTimeY[5].periods.replace('\/', ' - ') + '</span></li>' +

                                            '</ul></li>' +
                                            addressComment +
                                            '</ul>' + selectBtn,
                                    });

                                    if (window.location.pathname !== '/contacts/') {

                                    }

                                    openedBaloon = balloon;
                                });

                                let ID = pvz.Code;
                                let pvzListItem = '<div class="card z-depth-0 bordered"> ' +
                                    '<div class="card-header p-0" id="heading' + ID + '">' +

                                    '<h5 class="mb-0 p-0">' +
                                    '<button class="btn btn-link collapsed" style="width:90%" type="button" data-toggle="collapse"' +
                                    'data-target="#collapse' + ID + '" aria-expanded="false" aria-controls="collapse' + ID + '">' +
                                    'г. ' + pvz.City + ': ' + pvz.Address +
                                    '</button>' +
                                    '<span><label class="circle-radio-btn d-inline m-0" >\n' +
                                    '<input id="input' + pvz.Code + '" type="radio" name="cdek-pvz-celect" value="' + pvz.Code + '">\n' +
                                    '<span class="checkmark"></span>\n' +
                                    '</label></span>' +
                                    '</h5>' +
                                    '</div>' +
                                    '<div id="collapse' + ID + '" class="collapse" aria-labelledby="headingThree" data-parent="#accordionCdekList">' +
                                    '<div class="card-body">' +
                                    '<ul>' +
                                    '<li><b>Адрес:</b> ' + pvz.Address + '</li>' +
                                    '<li><b>Телефон:</b> ' + pvz.Phone + '</li>' +
                                    nearestStation +
                                    '<li><b>Режим работы:</b>' +
                                    ' <ul>' +
                                    '<li>Пн-Пт: ' + pvz.WorkTimeY[0].periods.replace('\/', ' -     ') + '</li>' +
                                    '<li><span class="red_text">Сб-Вс: ' + pvz.WorkTimeY[5].periods.replace('\/', ' - ') + '</span></li>' +
                                    '</ul>' +
                                    '</li>' +
                                    addressComment +
                                    '</ul>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                                pvzsList += pvzListItem;
                            }
                        });
                        $('#accordionCdekList').html(pvzsList);
                        $('div.card-body>p.text-bold').html('Выберите ближайший пункт выдачи');

                    }
                    else{
                        $('div.card-body>p.text-bold').html(
                            '<span class="red_text">'+result.message+'</span>'
                        );
                    }

                    $('#CCPrice').html(cost.cc.price);
                    $('#CDPrice').html(cost.cd.price);
                    let deliveryDate = new Date(cost.cd.deliveryDateMax);
                    let options = {
                        month: 'long',
                        day: 'numeric',
                        weekday: 'long',
                    };

                    $('#deliveryDate').html(deliveryDate.toLocaleString('ru', options));

                 });

                $('#selectYourCityModal').modal('hide');
                $('#street').removeClass('collapse');

                changeCity();
            }
            else{
                $('#deliveryKazan').removeClass('d-none');
                $('#selectYourCityModal').modal('hide');
                $('#toDoorKazan input').prop('disabled', false);
                $('#pickupKazan input').prop('disabled', false);
                otherCities.removeClass('d-block');
                $('#otherCityDeliveryToDoor').addClass('d-none');
                $('#street').addClass('collapse');
                $('#deliveryCompaniesVariation').addClass('collapse');
            }
            $('#selectedCity').attr('value', suggestion.data.country + ', ' +  suggestion.data.city);
        }
    });

    function setCity(city){
        let url = '/location/change';
        let data = 'city=' + city + '&token=' + $('#changeCityToken').val();
        $.post(url, data, function(result){
            result = JSON.parse(result);
            console.log(result);
            $('#topMenuChangeCityBtn')
                .attr('city-index', result.data.postal_code)
                .attr('city', result.data.city)
                .find('span').html(result.data.city_with_type);
        });


    }

    address.on('change', function () {

        $('#selectYourCityModal').modal('hide');
        // $('#selectYourCityModal').modal('hide')
    });

    $('#cdekMap').on('click', '.cdekmap-placemark-btn', function(){
        $('#mapSelectedPvzInputContainer').html('<input  type="radio" checked name="cdek-pvz-celect" value="' + selectedPlacemark.Code + '" hidden>');
        openedBaloon.close();
        getSelectedPvzMessage();
    });

    $('#selectCart, #selectList').click(function()
    {
        let hiddenElement = $(this).attr('hide-el');
        let shownElement = $(this).attr('show-el');
        $(hiddenElement).addClass('hidden');
        $(shownElement).removeClass('hidden');
        $('a.selected').removeClass('selected');
        $(this).addClass('selected');
    });

    $('#accordionCdekList').on('click', 'button', function()
    {
        let pvzCode = $(this).attr('data-target').replace('#collapse', '');
        selectedPlacemark = pvzs.find(function(pvz){
                return pvz.Code === pvzCode;
        });
        $('#mapSelectedPvzInputContainer').html('');
        let selectedPvzInput = selectedPlacemark.Code;
        // console.log(selectedPvzInput);
        $('input[value="' +selectedPvzInput+'"]').prop('checked', true);
        // $('input[value="' +$(this).attr('id')+'"]').prop('checked', true);
        getSelectedPvzMessage();
    });
    //
    // $('input[name="gift"]').click(function(){
    //     console.log('ok');
    // });

    $('#deliveryCompaniesVariation .checkmark').click(function () {
        if($(this).siblings('input').val() == 'to-door'){
            $('div[data-parent="#deliveryCompaniesVariation"]').removeClass('show');
            $('#otherCityDeliveryToDoor').removeClass('d-none');
            $('.cdek-pvz-celect').prop('checked', false);
            // $('#deliveryKazan').removeClass('d-none');
        }
        else if('intercity-bus'){
            $('div[data-parent="#deliveryCompaniesVariation"]').addClass('show');
            $('#otherCityDeliveryToDoor').addClass('d-none');
        }
    });

    $('#toDoorKazan .checkmark').click(function(){
        // $('#deliveryCompaniesVariation .checkmark').prop('checked', false);

    });

    function getSelectedPvzMessage()
    {
        $('#selectPvzMessage').html(
            'Выбран пункт по адресу: г. ' +selectedPlacemark.City + ', ' +
            selectedPlacemark.Address
        );
    }


});

// $(document).ready(function(){
//     var otherCity = $('#topMenuChangeCityBtn').attr('city-index');
//     if(otherCity !== '420000'){
//         let url = '/order/cdek/pvz/';
//         let city = $(this).text();
//         let postCode = otherCity;
//         let openedBaloon;
//         let data = 'citypostcode=' + postCode + '&city=' + $('#topMenuChangeCityBtn').attr('city');
//         $.post(url, data, function (result) {
//
//             $('#cdekMap').css({"width": "100%", "height" : "450px"});
//
//             result = JSON.parse(result);
//
//
//             if(result.answer === 'error'){
//
//             }
//             let cityCords = [result.cords.latitude, result.cords.longitude];
//             myMap  = new ymaps.Map("cdekMap", {
//                 center: cityCords,
//                 zoom: 10
//             });
//             myMap.setCenter([result.cords.latitude, result.cords.longitude]);
//             pvzs = result.message;
//             cost = result.cost;
//
//             if(result.answer === 'success'){
//                 let pvzsList = '';
//                 pvzs.forEach(function(pvz, index) {
//                     if(pvz.coordX !== undefined) {
//                         let placemarkName = 'placemark' + pvz.Code;
//
//                         window[placemarkName] = new ymaps.Placemark([pvz.coordY, pvz.coordX]);
//                         myMap.geoObjects.add(window[placemarkName]);
//                         let nearestStation = "";
//                         if (pvz.NearestStation) {
//                             nearestStation = '<li><b>Ближайщее метро:</b> ' + pvz.NearestStation + '</li>';
//                         }
//
//                         let addressComment = '';
//                         if (pvz.AddressComment) {
//                             addressComment = '<li><b>Как найти:</b> ' + pvz.AddressComment + '</li>';
//                         }
//                         window[placemarkName].events.add('click', function (e) {
//                             if (openedBaloon !== undefined) {
//                                 openedBaloon.close();
//                             }
//
//                             // Создание независимого экземпляра балуна и отображение его в центре карты.
//                             var  balloon = new ymaps.Balloon(myMap);
//                             // Здесь родительскими устанавливаются опции карты,
//                             // где содержатся значения по умолчанию для обязательных опций.
//
//                             balloon.options.setParent(myMap.options);
//
//                             // Открываем балун месте размещения метки.
//                             selectedPlacemark = pvz;
//                             let selectBtn = '';
//                             if (window.location.pathname !== '/contacts/') {
//                                 selectBtn = '<a class="btn btn-outline-red waves-effect waves-light cdekmap-placemark-btn"' +
//                                     'id="placemarkSelectBtn' + pvz.Code + '" href="#!">Выбрать</a>';
//                             }
//                             console.log(balloon);
//                             balloon.open(e.get('coords'), {
//                                 contentHeader: 'г. ' + pvz.City + ': ' + pvz.Name,
//                                 contentBody: '<ul>' +
//                                     '<li><b>Адрес:</b> ' + pvz.Address + '</li>' +
//                                     '<li><b>Телефон:</b> ' + pvz.Phone + '</li>' +
//                                     nearestStation +
//                                     '<li><b>Режим работы:</b> <ul>' +
//                                     '<li>Пн-Пт: ' + pvz.WorkTimeY[0].periods.replace('\/', ' -     ') + '</li>' +
//                                     // '<li>Вт: ' + pvz.WorkTimeY[1].periods + '</li>' +
//                                     // '<li>Ср: ' + pvz.WorkTimeY[2].periods + '</li>' +
//                                     // '<li>Чт: ' + pvz.WorkTimeY[3].periods + '</li>' +
//                                     // '<li>Пт: ' + pvz.WorkTimeY[4].periods + '</li>' +
//                                     // '<li><span class="red_text">Сб: ' + pvz.WorkTimeY[5].periods + '</span></li>' +
//                                     '<li><span class="red_text">Сб-Вс: ' + pvz.WorkTimeY[5].periods.replace('\/', ' - ') + '</span></li>' +
//
//                                     '</ul></li>' +
//                                     addressComment +
//                                     '</ul>' + selectBtn,
//                             });
//
//                             if (window.location.pathname !== '/contacts/') {
//
//                             }
//
//                             openedBaloon = balloon;
//
//                         });
//
//                         let ID = pvz.Code;
//                         console.log(pvz);
//                         let pvzListItem = '<div class="card z-depth-0 bordered"> ' +
//                         '<div class="card-header p-0" id="heading' + ID + '">' +
//
//                         '<h5 class="mb-0 p-0">' +
//                         '<button class="btn btn-link collapsed" style="width:90%" type="button" data-toggle="collapse"' +
//                         'data-target="#collapse' + ID + '" aria-expanded="false" aria-controls="collapse' + ID + '">' +
//                         'г. ' + pvz.City + ': ' + pvz.Address +
//                         '</button>' +
//                         '<span><label class="circle-radio-btn d-inline m-0" >\n' +
//                         '<input id="input' + pvz.Code + '" type="radio" name="cdek-pvz-celect" value="' + pvz.Code + '">\n' +
//                         '<span class="checkmark"></span>\n' +
//                         '</label></span>' +
//                         '</h5>' +
//                         '</div>' +
//                         '<div id="collapse' + ID + '" class="collapse" aria-labelledby="headingThree" data-parent="#accordionCdekList">' +
//                         '<div class="card-body">' +
//                         '<ul>' +
//                         '<li><b>Адрес:</b> ' + pvz.Address + '</li>' +
//                         '<li><b>Телефон:</b> ' + pvz.Phone + '</li>' +
//                         nearestStation +
//                         '<li><b>Режим работы:</b>' +
//                         ' <ul>' +
//                         '<li>Пн-Пт: ' +  (pvz.WorkTimeY[0] !== undefined)? pvz.WorkTimeY[0].periods.replace('\/', ' - ') + '' : '-' + '</li>' +
//                         '<li><span class="red_text">Сб-Вс: ' + (pvz.WorkTimeY[5] !== undefined)? pvz.WorkTimeY[5].periods.replace('\/', ' - ') + '': '-' + '</span></li>' +
//                             '</ul>' +
//                             '</li>' +
//                             addressComment +
//                             '</ul>' +
//                             '</div>' +
//                             '</div>' +
//                             '</div>';
//                         pvzsList += pvzListItem;
//                     }
//                 });
//                 console.log(pvzsList);
//                 $('#accordionCdekList').html(pvzsList);
//                 $('div.card-body>p.text-bold').html('Выберите ближайший пункт выдачи');
//
//             }
//             else{
//                 $('div.card-body>p.text-bold').html(
//                     '<span class="red_text">'+result.message+'</span>'
//                 );
//             }
//
//             $('#CCPrice').html(cost.cc.price);
//             $('#CDPrice').html(cost.cd.price);
//             let deliveryDate = new Date(cost.cd.deliveryDateMax);
//             let options = {
//                 month: 'long',
//                 day: 'numeric',
//                 weekday: 'long',
//             };
//
//             $('#deliveryDate').html(deliveryDate.toLocaleString('ru', options));
//             $('#selectedCity').attr('value', result.city.country + ', ' +  result.city.cityName);
//         });
//     }
// });

