<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.08.2019
 * Time: 10:47
 */

namespace App\DataFixtures;

use App\Utils\DataVariables;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory as Faker;
use Faker\Generator;
//implements DependentFixtureInterface
abstract class BaseFixtures extends Fixture
{
    /** @var ObjectManager */
    private $manager;
    /** @var Generator */
    protected $faker;

    public $variables;

    private $referencesIndex = [];

    abstract protected function loadData(ObjectManager $em);

    public function __construct(EntityManagerInterface $em)
    {
        $this->variables = new DataVariables($em);
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->faker = Faker::create('ru_RU');
        $this->loadData($manager);
    }

    protected function createMany(string $className, int $count, callable $factory)
    {
        for ($i = 0; $i < $count; $i++) {
          $this->createOne($className, $i, $factory);
            // store for usage later as App\Entity\ClassName_#COUNT#
        }
    }

    protected function createOne(string $className, int $i, callable $factory)
    {
        $entity = new $className();
        $factory($entity, $i);
        $this->addReference($className . '_' . $i, $entity);
        $this->manager->persist($entity);
    }

    /**
     * @param string $className
     * @return object
     * @throws \Exception
     */
    protected function getRandomReference(string $className)
    {
        $this->issetReferenceIndex($className);
        if (empty($this->referencesIndex[$className])) {
            throw new \Exception(sprintf('Cannot find any references for class "%s"', $className));
        }

        $randomReferenceKey = $this->faker->randomElement($this->referencesIndex[$className]);


        return $this->getReference($randomReferenceKey);
    }

    public function getCustomReference($className, $count)
    {
        $this->issetReferenceIndex($className);

        if (empty($this->referencesIndex[$className])) {
            throw new \Exception(sprintf('Cannot find any references for class "%s"', $className));
        }

        if(isset($this->referencesIndex[$className][$count])){
        return $this->getReference($this->referencesIndex[$className][$count]);
        }
        else {
            return;
        }
    }

    /**
     * @param $className
     * @return \Doctrine\Common\DataFixtures\ReferenceRepository
     */
    public function getReferenceRepository(): \Doctrine\Common\DataFixtures\ReferenceRepository
    {
        return $this->referenceRepository;
    }
    public function issetReferenceIndex($className)
    {
        if (!isset($this->referencesIndex[$className])) {
            $this->referencesIndex[$className] = [];
            foreach ($this->referenceRepository->getReferences() as $key => $ref) {
                if (strpos($key, $className.'_') === 0) {
                    $this->referencesIndex[$className][] = $key;
                }
            }
        }
    }

    public function doFlush(ObjectManager $manager)
    {
//        return null;
        $manager->flush();
    }



    public function fakeName()
    {
        $gender = rand(0, count(['male', 'female']) - 1);

        return $this->faker->unique()->name($gender);
    }

    /**
     * @param $amount
     * @return bool
     */
    public function getBoolean($amount)   :bool
    {

            return $this->faker->boolean($amount);

    }


    /**
     * @return ObjectManager
     */
    public function getManager(): ObjectManager
    {
        return $this->manager;
    }

}