<?php

namespace App\DataFixtures;

use App\Entity\TOrder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class OrderFixtures extends BaseFixtures
{
    public function loadData(ObjectManager $manager)
    {
        for($i = 6; $i < 135; $i++){
            $order = new TOrder();
            $order->setClientName($this->fakeName())
                ->setClientEmail($this->faker->email)
                ->setClientPhone(79370047776)
                ->setIsPayed(true)
                ->setCreatedAt($this->faker->dateTime)
                ->setCompanyName($this->faker->company)
            ;
            $manager->persist($order);
        }

        $manager->flush();
    }
}
