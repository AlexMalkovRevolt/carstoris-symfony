<?php

namespace App\Security;

use App\Entity\TUser;
use App\Repository\TUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;
    private $entityManager;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $userRepository;
    private $router;


    public function __construct(RouterInterface $router, EntityManagerInterface $entityManager, TUserRepository $userRepository,  CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder)
    {

        $this->entityManager = $entityManager;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
        $this->router = $router;

    }

    public function supports(Request $request)
    {
        return 'login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
//        dump($request->request->all(), $request->request->get('_email') );
        $loginBy = (isset($request->request->all()['login_form']['email']))? 'email' :  'phoneNumber';

        $credentials = [
            $loginBy => $request->request->all()['login_form'][$loginBy],
            'password' => $request->request->all()['login_form']['password'],
            'csrf_token' => $request->request->all()['login_form']['_token']
        ];

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials[$loginBy]
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {

//        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
////
////
////        if (!$this->csrfTokenManager->isTokenValid($token)) {
////            throw new InvalidCsrfTokenException();
////        }

        $user = $this->userRepository->findOneBy(['email' => $credentials['email']]);

        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Что то пошло не так. Попробуйте еще раз. ');
        }


        return $user;
//        return null;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {


        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
//        dd(new RedirectResponse($this->router->generate('home')));

        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {

            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->router->generate('home'));
    }

    protected function getLoginUrl()
    {
        return $this->router->generate('login');
    }
}
