<?php

namespace App\Form;

use App\Entity\Partner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddPartnerFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', TextType::class,[
                'label' => 'Название компании',
                'required' => false,
            ])
            ->add('contactName', TextType::class,[
                'label' => 'Контактное лицо',
                'required' => false,
                ])
            ->add('adress', TextType::class,[
                'label' => 'Адрес',
                'required' => false,
                ])
            ->add('password', PasswordType::class,[
                'label' => 'Пароль',
                ])
            ->add('promocode', TextType::class,[
                'label' => 'Промо код',
                'required' => false,
                ])
            ->add('accountDetails', TextareaType::class,[
                'label' => 'Реквизиты для перечисления бонусов',
                'required' => false,
                ])
            ->add('promocodeDiscount', TextType::class,[
                'label' => 'Скидка клиенту по промо коду',
                'required' => false,

                'attr' => ['placeholder' => 'Только цифры']
                ])
            ->add('bagsBonus', TextType::class,[
                'label' => 'Саквояжи',
                'required' => false,
                ])
            ->add('for_kidsBonus', TextType::class,[
                'label' => 'Для детей',
                'required' => false,
                ])
            ->add('mantleBonus', TextType::class,[
                'label' => 'Накидки',
                'required' => false,
                ])
            ->add('carpetsBonus', TextType::class,[
                'label' => 'Коврики',
                'required' => false,
                ])
            ->add('for_animalsBonus', TextType::class,[
                'label' => 'Для животных',
                'required' => false,
                ])
            ->add('foldersBonus', TextType::class,[
                'label' => 'Папки',
                'required' => false,
                ])
            ->add('phoneNumber', TextType::class,[
                'label' => 'Номер телефона',
                'required' => false,
                ])
            ->add('linkCode', TextType::class,[
                'label' => 'Партнерская ссылка',
                'required' => false,
            ])
            ->add('email', TextType::class, [
                'label' => 'E-mail',
            ])
            ->add('savePartner', SubmitType::class, [
                'label' => 'Сохранить'
            ])
            ->getForm()
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Partner::class,
        ]);
    }
}
