<?php


namespace App\Utils;


trait ArrayReducer
{
    /**
     * Метод удаляет знак с ключей предоставленного массива
     * В аргументах предоставляется знак для удаления и массив в ключах которого необходимо удалить предоставленный знак.
     * @param string $char
     * @param array $array
     * @return array
     */
    public function deleteCharFromKeys(string $char, array $array)
    {

        $reducedArray = [];
        foreach($array as $key => $item){
            $reducedArray[preg_replace('/['. $char .']/', '', $key)] = $item;
        }
        return $reducedArray;
    }

    public function wrapFirstLetterToTag(string $wrapTag, $word)
    {
        $array = str_split ( $word);
        $array[0] = '<'.$wrapTag.'>' . $array[0] . '</' . $wrapTag . '>';        
        return implode('', $array);
    }

}