<?php


namespace App\Utils;


use Symfony\Component\OptionsResolver\OptionsResolver;

trait OptionResolverTrait
{
    public $entityManager;
    public function configureOptions(OptionsResolver $resolver)
    {
        $this->entityManager = $resolver->setRequired('entity_manager');

    }
}