<?php


namespace App\Utils;

use Symfony\Component\Config\Definition\Exception\Exception;

class Dadata
{
    private $base_url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs";
    private $token;
    private $handle;

    function __construct()
    {
        $this->token = '711dc3fd59333801797a21bffa56c0f9a5a34769';
    }

    public function init()
    {
        $this->handle = curl_init();
        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->handle, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Token " . $this->token
        ));
        curl_setopt($this->handle, CURLOPT_POST, 1);
    }

    /**
     * See https://dadata.ru/api/outward/ for details.
     */
    public function findById($type, $fields)
    {
        $url = $this->base_url . "/findById/$type";
        return $this->executeRequest($url, $fields);
    }

    /**
     * See https://dadata.ru/api/geolocate/ for details.
     */
    public function geolocate($lat, $lon, $count = 10, $radius_meters = 100)
    {
        $url = $this->base_url . "/geolocate/address";
        $fields = array(
            "lat" => $lat,
            "lon" => $lon,
            "count" => $count,
            "radius_meters" => $radius_meters
        );
        return $this->executeRequest($url, $fields);
    }
    /**
     * See https://dadata.ru/api/geolocate/ for details.
     */
    public function postIndex($index)
    {
        $url = $this->base_url . "/suggest/address";
        $fields = array(
          'postal_code' => $index
        );
        return $this->executeRequest($url, $fields);
    }

    /**
     * See https://dadata.ru/api/iplocate/ for details.
     */
    public function iplocate($ip) {
        $url = $this->base_url . "/iplocate/address?ip=" . $ip;
        return $this->executeRequest($url, $fields = null);
    }

    /**
     * See https://dadata.ru/api/suggest/ for details.
     */
    public function suggest($type, $fields) {
        $url = $this->base_url . "/suggest/$type";
        return $this->executeRequest($url, $fields);
    }

    public function close() {
        curl_close($this->handle);
    }

    public function getPostalByGeo(array $coords, $radius)
    {
        $url = $this->base_url . '/geolocate/postal_unit/';
        $fields = $coords;
        $fields['radius_meters'] = $radius;
       return  $this->executeRequest($url, $fields);
    }

    private function executeRequest($url, $fields) {
//        dd(json_encode($fields));
        curl_setopt(
            $this->handle,
            CURLOPT_URL,
            $url);
        if ($fields != null) {
            curl_setopt($this->handle, CURLOPT_POST, 1);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, json_encode($fields));
        } else {
            curl_setopt($this->handle, CURLOPT_POST, 0);
        }
        $result = $this->exec();
        $result = json_decode($result, true);
        return $result;
    }

    private function exec() {
        $result = curl_exec($this->handle);
        $info = curl_getinfo($this->handle);
        if ($info['http_code'] == 429) {
            throw new TooManyRequests();
        } elseif ($info['http_code'] != 200) {
            throw new Exception('Request failed with http code ' . $info['http_code'] . ': ' . $result);
        }
        return $result;
    }
}

// Метод init() следует вызвать один раз в начале,
// затем можно сколько угодно раз вызывать suggest()
// и в конце следует один раз вызвать метод close().
//
// За счёт этого не создаются новые сетевые соединения на каждый запрос,
// а переиспользуется существующее.

//$dadata = new Dadata("ВАШ_API_КЛЮЧ");
//$dadata->init();
//
//$fields = array("query"=>"7707083893", "count"=>5);
//$result = $dadata->suggest("party", $fields);
//print_r($result);
//
//$fields = array("query"=>"77000000000283600", "count"=>1);
//$result = $dadata->findById("address", $fields);
//print_r($result);
//
//$result = $dadata->iplocate("46.226.227.20");
//print_r($result);
//
//$result = $dadata->geolocate(55.878, 37.653);
//print_r($result);
//
//$dadata->close();