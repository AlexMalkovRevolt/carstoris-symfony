<?php


namespace App\Utils;


trait Time
{
    private $month = [
        "января",
        "февраля",
        "марта",
        "апреля",
        "мая",
        "июня",
        "июля",
        "августа",
        "сентября",
        "октября",
        "ноября",
        "декабря"
    ];
    private $week = [
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Субботу',
        'Воскресенье',
    ];

    public function rdate(\DateTime $dateTime, string $format)
    {
        $formatArray = str_split($format);
        $formatResult = '';
        foreach ($formatArray as $formatItem){
            if($formatItem == 'F'){
                $formatResult .= $this->month[$dateTime->format('n')];
            }
            elseif ($formatItem == 'l'){
                $formatResult .= $this->week[$dateTime->format('w')];
            }
            elseif ($formatItem == ' '){
                $formatResult .= $formatItem;
            }
            else{
                $formatResult .= $dateTime->format($formatItem);
            }
        }
        return $formatResult;
    }
}