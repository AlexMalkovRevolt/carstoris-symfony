<?php


namespace App\Utils;


use App\ShoppingCart\ShoppingCart;

trait DiscountPromotion
{
    private $promotionProductCount;
    private $foundPromotionProduct;
    private $products = [];

    public function countCartItems(ShoppingCart $cart, array $promotionProducts )
    {

//dd($cart->getItems()[0]->getProductId());
        foreach($cart->getItems() as $item){
            $foundProductKey = array_search($item->getProductId(), $promotionProducts);
            if($foundProductKey !== false){
                $this->foundPromotionProduct[$promotionProducts[$foundProductKey]] = $item;
            }
        }

        dd($this->foundPromotionProduct);

    }
}