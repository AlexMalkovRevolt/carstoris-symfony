<?php


namespace App\Utils;


class BigCity
{
    use ArrayReducer;
    private $cities = [];

    public function __construct()
    {
       $this->cities = [
            ['index'=>'655001', 'nameRu'=>'Абакан', 'isMillion' => false],
            ['index'=>'163000', 'nameRu'=>'Архангельск', 'isMillion' => false],
            ['index'=>'414000', 'nameRu'=>'Астрахань', 'isMillion' => false],
            ['index'=>'656000', 'nameRu'=>'Барнаул', 'isMillion' => false],
            ['index'=>'308000', 'nameRu'=>'Белгород', 'isMillion' => false],
            ['index'=>'659300', 'nameRu'=>'Бийск', 'isMillion' => false],
            ['index'=>'675000', 'nameRu'=>'Благовещенск', 'isMillion' => false],
            ['index'=>'665700', 'nameRu'=>'Братск', 'isMillion' => false],
            ['index'=>'241001', 'nameRu'=>'Брянск', 'isMillion' => false],
            ['index'=>'173000', 'nameRu'=>'Великий Новгород', 'isMillion' => false],
            ['index'=>'690000', 'nameRu'=>'Владивосток', 'isMillion' => false],
            ['index'=>'600000', 'nameRu'=>'Владимир', 'isMillion' => false],
            ['index'=>'400000', 'nameRu'=>'Волгоград', 'isMillion' => true],
            ['index'=>'160000', 'nameRu'=>'Вологда', 'isMillion' => false],
            ['index'=>'394000', 'nameRu'=>'Воронеж', 'isMillion' => true],
            ['index'=>'153000', 'nameRu'=>'Иваново', 'isMillion' => false],
            ['index'=>'664001', 'nameRu'=>'Иркутск', 'isMillion' => false],
            ['index'=>'424000', 'nameRu'=>'Йошкар-Ола', 'isMillion' => false],
            ['index'=>'420000', 'nameRu'=>'Казань', 'isMillion' => true],
            ['index'=>'248000', 'nameRu'=>'Калуга', 'isMillion' => false],
            ['index'=>'650000', 'nameRu'=>'Кемерово', 'isMillion' => false],
            ['index'=>'610000', 'nameRu'=>'Киров', 'isMillion' => false],
            ['index'=>'681000', 'nameRu'=>'Комсомольск-на-Амуре', 'isMillion' => false],
            ['index'=>'350000', 'nameRu'=>'Краснодар', 'isMillion' => true],
            ['index'=>'600030', 'nameRu'=>'Красноярск', 'isMillion' => true],
            ['index'=>'640000', 'nameRu'=>'Курган', 'isMillion' => false],
            ['index'=>'305000', 'nameRu'=>'Курск', 'isMillion' => false],
            ['index'=>'398000', 'nameRu'=>'Липецк', 'isMillion' => false],
            ['index'=>'455000', 'nameRu'=>'Магнитогорск', 'isMillion' => false],
            ['index'=>'367000', 'nameRu'=>'Махачкала', 'isMillion' => false],
            ['index'=>'183001', 'nameRu'=>'Мурманск', 'isMillion' => false],
            ['index'=>'423800', 'nameRu'=>'Набережные Челны', 'isMillion' => false],
            ['index'=>'360002', 'nameRu'=>'Нальчик', 'isMillion' => false],
            ['index'=>'628301', 'nameRu'=>'Нефтеюганск', 'isMillion' => false],
            ['index'=>'628601', 'nameRu'=>'Нижневартовск', 'isMillion' => false],
            ['index'=>'603000', 'nameRu'=>'Нижний Новгород', 'isMillion' => true],
            ['index'=>'654000', 'nameRu'=>'Новокузнецк', 'isMillion' => false],
            ['index'=>'353900', 'nameRu'=>'Новороссийск', 'isMillion' => false],
            ['index'=>'630001', 'nameRu'=>'Новосибирск', 'isMillion' => true],
            ['index'=>'644001', 'nameRu'=>'Омск', 'isMillion' => true],
            ['index'=>'302001', 'nameRu'=>'Орел', 'isMillion' => false],
            ['index'=>'460000', 'nameRu'=>'Оренбург', 'isMillion' => false],
            ['index'=>'440000', 'nameRu'=>'Пенза', 'isMillion' => false],
            ['index'=>'614000', 'nameRu'=>'Пермь', 'isMillion' => true],
            ['index'=>'185001', 'nameRu'=>'Петрозаводск', 'isMillion' => false],
            ['index'=>'683000', 'nameRu'=>'Петропавловск', 'isMillion' => false],
            ['index'=>'180000', 'nameRu'=>'Псков', 'isMillion' => false],
            ['index'=>'357361', 'nameRu'=>'Пятигорск', 'isMillion' => false],
            ['index'=>'344000', 'nameRu'=> 'Ростов-на-Дону', 'isMillion' => true],
            ['index'=>'390000', 'nameRu'=>'Рязань', 'isMillion' => false],
            ['index'=>'443000', 'nameRu'=>'Самара', 'isMillion' => true],
            ['index'=>'430001', 'nameRu'=>'Саранск', 'isMillion' => false],
            ['index'=>'410000', 'nameRu'=>'Саратов', 'isMillion' => false],
            ['index'=>'214000', 'nameRu'=>'Смоленск', 'isMillion' => false],
            ['index'=>'354000', 'nameRu'=>'Сочи', 'isMillion' => false],
            ['index'=>'355000', 'nameRu'=>'Ставрополь', 'isMillion' => false],
            ['index'=>'309501', 'nameRu'=>'Старый Оскол', 'isMillion' => false],
            ['index'=>'628400', 'nameRu'=>'Сургут', 'isMillion' => false],
            ['index'=>'167000', 'nameRu'=>'Сыктывкар', 'isMillion' => false],
            ['index'=>'346842', 'nameRu'=>'Таганрог', 'isMillion' => false],
            ['index'=>'392000', 'nameRu'=>'Тамбов', 'isMillion' => false],
            ['index'=>'170001', 'nameRu'=>'Тверь', 'isMillion' => false],
            ['index'=>'445003', 'nameRu'=>'Тольятти', 'isMillion' => false],
            ['index'=>'634003', 'nameRu'=>'Томск', 'isMillion' => false],
            ['index'=>'300001', 'nameRu'=>'Тула', 'isMillion' => false],
            ['index'=>'625000', 'nameRu'=>'Тюмень', 'isMillion' => false],
            ['index'=>'670000', 'nameRu'=>'Улан-Удэ', 'isMillion' => false],
            ['index'=>'432000', 'nameRu'=>'Ульяновск', 'isMillion' => false],
            ['index'=>'090000', 'nameRu'=>'Уральск', 'isMillion' => false],
            ['index'=>'692500', 'nameRu'=>'Уссурийск', 'isMillion' => false],
            ['index'=>'070000', 'nameRu'=>'Усть-Каменогорск', 'isMillion' => true],
            ['index'=>'450000', 'nameRu'=>'Уфа', 'isMillion' => true],
            ['index'=>'680000', 'nameRu'=>'Хабаровск', 'isMillion' => false],
            ['index'=>'628001', 'nameRu'=>'Ханты-Мансийск', 'isMillion' => false],
            ['index'=>'428000', 'nameRu'=>'Чебоксары', 'isMillion' => false],
            ['index'=>'454000', 'nameRu'=>'Челябинск', 'isMillion' => true],
            ['index'=>'162601', 'nameRu'=>'Череповец', 'isMillion' => false],
            ['index'=>'672000', 'nameRu'=>'Чита', 'isMillion' => false],
            ['index'=>'228228', 'nameRu'=>'Якутск', 'isMillion' => false],
            ['index'=>'150000', 'nameRu'=>'Ярославль', 'isMillion' => false]
        ];
    }

    /**
     * @param int $blockAmount
     * @return array
     */
    public function getCities($blockAmount = 1)
    {

        if($blockAmount == 1){
          return [$this->cities];
        }
        else{
            return array_chunk($this->cities, $blockAmount);
        }
    }

    public function getCityByIndex(string $index)
    {
        $kye = array_search($index, $this->cities);
        return $this->cities[$kye];
    }

    public function getMillionaireCity()
    {
        $millionaire = [];
        foreach($this->cities as $city){
            if(isset($city['isMillion'])){
                     $millionaire[] = $city;
                }
        }

        return $millionaire;
    }
}