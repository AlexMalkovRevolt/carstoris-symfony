<?php


namespace App\Utils;


class SqlHelper
{
    private $array = [];
    public function __construct($dataArray)
    {
        $this->array = array_filter($dataArray);

    }

    /**
     * Метод создает строку для подстановки в sql запрос и
     * используется в миграциях.
     * @return string
     */
    public function makeColumnList()
    {
        $sqlKeys = array_keys($this->array);
        return implode(', ', $sqlKeys);
    }

    /**
     * @return string
     */
    public function makeValuesList()
    {
        $values = '';
        $array = array_values($this->array);
        foreach($array as $key => $value){

            if (is_string($value)  && $value != ''){
                $values .= "'$value'";
            }
            else{
                if(is_bool($value)){
                    $values .= 'true';
                }
                else{
                    $values .= $value;
                }
            }
            if($key+1 !== count($this->array)){
                $values .= ',';
            }
        }

        return $values;
    }

}