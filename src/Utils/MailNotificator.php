<?php


namespace App\Utils;


use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class MailNotificator
{
    private $mailer;
    private $to;
    private $from;

    public function __construct(MailerInterface $mailer)
    {
        $this->to = 'carstoris@ya.ru';
        $this->from = 'carstoris@ya.ru';
        $this->mailer = $mailer;
    }

    public function sendNotification(
        string $subject,
        string $template,
        array $context = [],
        string $to = null,
        string $from = null
)
    {
        if(!is_null($to)){
            $this->to = $to;
        }
        elseif (!is_null($from)){
            $this->from = $from;
        }
           $email = new TemplatedEmail();
                $email->from($this->from)
                ->to($this->to)
                ->cc('d2450170@yandex.ru')
                //->bcc('bcc@example.com')
                //->replyTo('fabien@example.com')
                //->priority(Email::PRIORITY_HIGH)
                ->subject($subject)
                ->htmlTemplate($template)
                ->context($context);

            $this->mailer->send($email);
       
    }

}