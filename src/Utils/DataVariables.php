<?php


namespace App\Utils;


use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;

class DataVariables
{

    /**
     * Категории для использования в меню и т.д.
     * Использование в twig - twig_variables.categories
     * @var array
     */
private $categories = [];

private   $gifts = [
    [
        'name' => 'Салфетка замша',
        'description' => '',
        'thumbnail' => '',
        'is_active' => true
    ],
    [
        'name' => 'Салфетка микрофибра',
        'description' => '',
        'thumbnail' => '',
        'is_active' => true
    ],
    [
        'name' => 'Блокнот с ручкой',
        'description' => '',
        'thumbnail' => '',
        'is_active' => true
    ],

];

private   $options = [
    [
        'name' => 'Цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 1,
        'slug' => 'material_color'
    ],
    [
        'name' => 'Цвет нити',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 1,
        'slug' => 'thread_color'
    ],
    [
        'name' => 'цвет оконтовки',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 1,
        'slug' => 'edging_color'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 2,
        'slug' => 'material_color'
    ],
    [
        'name' => 'комплект',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 2,
        'slug' => 'carpet_set'
    ],
    [
        'name' => 'подпятник',
        'description' => '',
        'price' => 20000,
        'thumbnail' => null,
        'product_id' => 2,
        'slug' => 'for_heels'
    ],
    [
        'name' => 'марка авто',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 2,
        'slug' => 'car_model'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 3,
        'slug' => 'material_color'
    ],
    [
        'name' => 'цвет нити',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 3,
        'slug' => 'thread_color'
    ],
    [
        'name' => 'цвет оконтовки',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 3,
        'slug' => 'edging_color'
    ],
    [
        'name' => 'комплект',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 3,
        'slug' => 'mantle_set'
    ],
    [
        'name' => 'model',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 3,
        'slug' => 'model'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 4,
        'slug' => 'material_color'
    ],
    [
        'name' => 'цвет нити',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 4,
        'slug' => 'thread_color'
    ],
    [
        'name' => 'цвет оконтовки',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 4,
        'slug' => 'edging_color'
    ],
    [
        'name' => 'защитные борта',
        'description' => '',
        'price' => 50000,
        'thumbnail' => null,
        'product_id' => 4,
        'slug' => 'protective_sides'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 5,
        'slug' => 'material_color'
    ],
    [
        'name' => 'цвет нити',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 5,
        'slug' => 'thread_color'
    ],
    [
        'name' => 'цвет оконтовки',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 5,
        'slug' => 'edging_color'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 5,
        'slug' => 'material_color'
    ],
    [
        'name' => 'цвет нити',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 5,
        'slug' => 'thread_color'
    ],
    [
        'name' => 'цвет оконтовки',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 5,
        'slug' => 'edging_color'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 6,
        'slug' => 'material_color'
    ],
    [
        'name' => 'цвет нити',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 6,
        'slug' => 'thread_color'
    ],
    [
        'name' => 'цвет оконтовки',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 6,
        'slug' => 'edging_color'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 7,
        'slug' => 'material_color'
    ],
    [
        'name' => 'цвет нити',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 7,
        'slug' => 'thread_color'
    ],
    [
        'name' => 'цвет оконтовки',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 7,
        'slug' => 'edging_color'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 8,
        'slug' => 'material_color'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 9,
        'slug' => 'material_color'
    ],
    [
        'name' => 'цвет материала',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 10,
        'slug' => 'material_color'
    ],
    [
        'name' => 'цвет нити',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 10,
        'slug' => 'thread_color'
    ],
    [
        'name' => 'цвет оконтовки',
        'description' => '',
        'price' => null,
        'thumbnail' => null,
        'product_id' => 10,
        'slug' => 'edging_color'
    ],


];

public function __construct(EntityManagerInterface $em)
{

//    if($_SERVER['SERVER_NAME'] == 'carstoris.ru'){
    if($_ENV['APP_ENV'] == 'prod'){

    //на домене carstoris.ru должен быть этот код, чтобы показывать только оработающие категории.
    $this->categories = $em->getRepository(Category::class)
        ->findBy([
            'isActive' => true
        ] );
    }
    else{
         $this->categories = $em->getRepository(Category::class)
              ->findAll()
          ;
    }
}

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }


    /**
     * @return array
     */
    public function getGifts(): array
    {
        return $this->gifts;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

}
