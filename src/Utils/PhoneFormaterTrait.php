<?php


namespace App\Utils;


trait PhoneFormaterTrait
{
    public function formatPhone($phone)
    {
        return '+'.substr($phone, 0, 1).' ('.substr($phone, 1, 3).') '.substr($phone, 4, 3).'-'.substr($phone, 7, 2).'-'.substr($phone, 9, 2);
    }
}