<?php


namespace App\Utils;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Location
{
    private $session;
    private $location;
    private static $instance = null;
    
    private function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public static function getInstance(SessionInterface $session)
    {
        if(is_null(self::$instance)){
            self::$instance = new self($session);
        }

        return self::$instance;
    }

    public function setLocation(Array $location)
    {
        $array = $location;
        while(key($array) == 'location'){
                $array = $array['location'];
        }
        $this->location['location'] = $array;

        $this->session->set('location', $this->location);
    }

    public function getLocation()
    {
        return $this->location['location'];
    }

    public function getCity()
    {
        return $this->location['location']['data']['city'];
    }

    public function getCityWithType()
    {
        return $this->location['location']['data']['city_with_type'];

    }

    public function getPostCode()
    {
        return $this->location['location']['data']['postal_code'];
    }

    public function getCityData()
    {
        return $this->location['location']['data'];
    }


    public function getCladrId()
    {
        return $this->location['location']['data']['city_kladr_id'];

    }

    public function getGeoLocation()
    {
        return [
            'lat' => $this->location['location']['data']['geo_lat'],
            'lon' => $this->location['location']['data']['geo_lon'],

        ];
    }
}