<?php


namespace App\Utils;


class AppVariables
{
    private $logos = [
        'amg',
        'audi',
        'bmw',
        'bmw-m',
        'cadillac',
        'chevrolet',
        'citroen',
        'ford',
        'honda',
        'hyundai',
        'infiniti',
        'jaguar',
        'jeep',
        'kia',
        'lada',
        'land-rover',
        'lexus',
        'mazda',
        'mercedes-benz',
        'mini',
        'mitsubishi',
        'nissan',
        'opel',
        'peugeot',
        'porsche',
        'renault',
        'skoda',
        'subaru',
        'suzuki',
        'toyota',
        'volkswagen',
        'volvo',
    ];
    private $paramsTrans = [
        'material-color' => 'цвет материала',
        'thread-color' => 'цвет нити',
        'border-color' => 'цвет окантовки',
        'auto-brand' => 'бренд авто',
        'without_logo' => 'без логотипа',
        'black' => 'черный',
        'red' => 'красный',
        'blue' => 'синий',
        'light-blue' => 'голубой',
        'gray' => 'серый',
        'dark-gray' => 'темно серый',
        'beg' => 'бежевый',
        'brown' => 'коричневый',
        'orange' => 'оранжевый',
        'light-gray' => 'светло серый',
        'dark-brown' => 'темно коричневый',
        'yellow' => 'желтый',
        'zipper' => 'с замком',
        'without_zipper' => 'без замка',
        'with_zipper' => 'с замком',
        'biruza' => 'бирюзовый',
        'white' => 'белый',
        'pink' => 'розовый',
        'green' => 'зеленый',
        'emerald' => 'изумрудный',
        'light-green' => 'салатовый',
        'sapphire' => 'синий',
        'light-sapphire' => 'светло-синий',
        'mantle-types' => 'виды накидок',
        'romb' => 'Ромб',
        'uzkiy-romb' => 'Узкий ромб',
        'polosy' => 'Полосы',
        'complect-type' => 'вид комплектацию',
        'model-type' => 'вид модели',
        'nakidki-iz-alkantary' => 'Накидки на сиденья авто из Алькантары',
        'nakidki-na-odno-sidenye' => 'на одно сиденье',
        'nakidki-na-polovinu-sidenye' => 'на половину сиденья',
        'nakidki-na-2/3-sidenye' => 'на 2/3 сиденья',
        'nakidki-na-vse-sidenye' => 'На все сиденье',
        'uzkiy-verh-niz' => 'Узкий верх, узкий низ',
        'uzkiy-verh-shirokiy-niz' => 'Узкий верх, широкий низ',
        'shirokiy-verh-shirokiy-niz' => 'Широкий верх, широкий низ',
        'polnyy-komplekt' => 'Полный комплект',
        'na-zadniye-sidenya' => 'На задние сиденья',
        'na-peredniye-sidenya' => 'На передние сиденья',
        'kovriki-v-bagazhnik' => 'Коврики в багажник',
        'kovriki-v-salon' => 'Коврики в салон',
        'kovriki-zaniye' => 'Коврики задние',
        'kovriki-peredniye' => 'Коврики передние',
    ];
    private $prices = [
        'logo_shield' => 300,
        'carpets-shildik' => 190,
        'carpets-podpyatnik' => 590
    ];

    /**
     * @return array
     */
    public function getLogos(): array
    {
        return $this->logos;
    }

    /**
     * @return array
     */
    public function getParamsTrans(): array
    {
        return $this->paramsTrans;
    }

    /**
     * @return array
     */
    public function getPrices(): array
    {
        return $this->prices;
    }

}