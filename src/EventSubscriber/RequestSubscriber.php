<?php


namespace App\EventSubscriber;


use App\Entity\Partner;
use App\ShoppingCart\ShoppingCart;
use App\Utils\Dadata;
use App\Utils\Location;
use App\Utils\OptionResolverTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestSubscriber implements EventSubscriberInterface
{
    private $em;
    private $cart;

    public function __construct(EntityManagerInterface $em, ShoppingCart $cart)
    {
        $this->em = $em;
        $this->cart =$cart;

    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['processRequest', 10],
            ],
        ];
    }

    public function processRequest(RequestEvent $event)
    {
        $request = $request = Request::createFromGlobals();
        $dadata = new Dadata();
        $dadata->init();
        $userAgent = $request->headers->getIterator()['user-agent'][0];
        if(!(bool)preg_match('/^.*yandex.com\/bots\)$/',$userAgent)){
            $session = new Session();
//            $session->set('location', '');die();
            $location = Location::getInstance($session);
//            dd($session->get('location'));
//            $ip = ($request->getClientIp() && $request->getClientIp() != '127.0.0.1')? $request->getClientIp() : '178.207.202.19';
//            dd($session->get('location'));

            if( !$session->get('location') && !isset($session->get('location')->location['data'])){

                /**
                 * Получаем ip. Если ip не определен или ip == localhost, возвращаем Казанский ip чтобы локация был г. Казань.
                 */
                $ip = ($request->getClientIp() && $request->getClientIp() != '127.0.0.1')? $request->getClientIp() : '178.207.202.19';

                $location->setLocation(
                    $dadata->iplocate(
                        $ip
//                        '91.210.170.138'
                    ));

            }
            else{

//                $postCode = $session->get('location')['data']['postal_code'];
//                $fields = [];
//                dd($dadata->suggest('address', $postCode));
//                $location->setLocation($dadata->suggest('address', $poostCode));
//                dd($session->get('location'));
                $location->setLocation($session->get('location'));
//                dd($location->getLocation());
            }

    //            dd($location->getCityWithType());

    //            dd($location->getCityData());
    //            dd($session->get('location')->getLocation()['data']);
    //            dd($dadata->iplocate(
    //    //            $request->getClientIp()
    //                '91.210.170.138'
    //            ));
        }
        if(!is_null($request->get('promocode'))){

            $repository = $this->em->getRepository(Partner::class);
            $partner = $repository->findOneBy(['promocode'=> $request->get('promocode')]);

            if($partner){
                $this->cart->setPartner($partner);
                $this->cart->setLinkPartner();
            }
        }
        if($_ENV['APP_ENV'] == 'dev' && !is_null($request->get('cleancart')) && $request->get('cleancart') == 'yes'){
        $this->cart->cleanCart();

         }
        if($_ENV['APP_ENV'] == 'dev' && !is_null($request->get('cleanpartner')) && $request->get('cleanpartner') == 'yes'){
            $this->cart->removePartner();
        }
    }
}