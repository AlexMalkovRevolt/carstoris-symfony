<?php


namespace App\ShoppingCart;


use App\Utils\AppVariables;
use App\Entity\Product;
use App\Repository\ProductRepository;

class ShoppingCartItem
{
    private $id;
    private $productId;
    private $product;
    private $quantity = 1;
    private $appVariables;
    private $price;
    private $parameters = [];
    private $productName;
    private $paramsString;
    private $paramsRus;
    private $isWithLogo = false;
    private $shieldPrice = 0;
    private $discount = 0;
    private $isPromoted = false;
    private $priceWithoutDiscount;
    private $images = [];



    public function __construct(Product $product, int $quantity, array $images = [], $params = [])
    {
        $this->appVariables = new AppVariables();
        $this->productId = $product->getId();
        $this->quantity = $quantity;
        $this->productName = $product->getName();

        if ($params) {
            $this->addParams($params);
            $this->paramsString = $this->paramsToString($params);
            $this->paramsRus = $this->translateParams();
        }

        $this->product = $product;

        $this->shieldPrice = $this->appVariables->getPrices()['logo_shield'];

        if($product->getCategory()->getId() == 4){
            $this->discount = 200;
        }

        $this->price = $product->getPrice();
        if( isset($params['auto-brand'])){
            $this->isWithLogo = !($params['auto-brand'] == 'without_logo');
        }
        if(isset($params['carpets-shildik']) && !is_null($params['carpets-shildik'])) {
            $this->setPrice($this->price + $this->appVariables->getPrices()['carpets-shildik']);
        }
        if(isset($params['carpets-podpyatnik']) && !is_null($params['carpets-podpyatnik'])){
            $this->setPrice($this->price + $this->appVariables->getPrices()['carpets-podpyatnik']);
        }
        foreach($images as $key => $path){
            $this->images[$key] = $path;
        }

    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }


    public function addQuantity(int $quantity)
    {
        $this->quantity += $quantity;

    }

    public function minusQuantity(int $quantity)
    {

        if($this->quantity > 1){
             $this->quantity -= $quantity;
        }
        elseif($this->quantity <= 1){
             $this->quantity = 1;
        }
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return float|int
     */
    public function getItemAmount()
    {
        return $this->price * $this->quantity;
    }



    private function addParams($params)
    {
        $this->parameters = $params;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {

//        var_dump($this->parameters);die;
        return $this->parameters;
    }

    /**
     * Получение объекта Product единицы корзнины.
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Mетод производит из параметров товара строку, для дальнейшей проверки наличия похожего товаа в корзине.
     * @param array $params
     * @return string
     */
    public function paramsToString(array $params)
    {
        $result = '';
        foreach ($params as $key => $value) {
            if($key != 'quantity' && $key != 'name'){
                $result .= $value;
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getParamsString(): string
    {
        return $this->paramsString;
    }

    /**
     * Метод делает перевод каждого параметра на русский язык
     * @return array
     */
    private function translateParams()
    {
        $translatedArray = [];
        $translator = new AppVariables();

        foreach ($this->parameters as $key => $param) {
            if($key !== 'auto-brand' && preg_match('/(related)/', $key) == 0 ){
                if(isset($translator->getParamsTrans()[$key])){
                    $translatedKey = $translator->getParamsTrans()[$key];
                    $translatedValue = (isset($translator->getParamsTrans()[$param]))?$translator->getParamsTrans()[$param] : '';
                    $translatedArray[] = [
                        $translatedKey,
                        $translatedValue
                    ];
                }
                else{
                    continue;
                }
            }

        }


        return $translatedArray;

    }

    /**
     * @return bool
     */
    public function isWithLogo(): bool
    {
        return $this->isWithLogo;
    }

    /**
     * @return int
     */
    public function getShieldPrice(): int
    {
        return $this->shieldPrice;
    }

    public function removeShieldPrice()
    {

        $this->shieldPrice = 0;
    }

    public function setShieldPrice()
    {
        $this->shieldPrice =  $this->appVariables->getPrices()['logo_shield'];
    }

    /**
     * @return $this
     */
    public function setBoxShieldDiscount()
    {
        if($this->isWithLogo){
            $this->price = $this->product->getPrice();
        }
        return $this;
    }

    public function addShieldPrice()
    {

        return $this->price = $this->product->getPrice() + $this->appVariables->getPrices()['logo_shield'];

    }

    /**
     * @return bool
     */
    public function getIsPromoted()
    {
        return $this->isPromoted;
    }

    /**
     *
     */
    public function setIsPromoted(): void
    {
        $this->isPromoted = true;
    }

    public function deductPrice(int $deductAmount)
    {
        $this->price -= $deductAmount;
    }
    /**
     * @param string|null $price
     */
    public function setPrice(?string $price): void
    {
        $this->price = $price;
    }


    /**
     * @return string|null
     */
    public function getPriceWithoutDiscount(): ?string
    {
        return $this->priceWithoutDiscount;
    }

    /**
     * @return int
     */
    public function getDiscount(): int
    {
        return $this->discount;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages(array $images): void
    {
        $this->images = $images;
    }



}