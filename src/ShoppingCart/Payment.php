<?php


namespace App\ShoppingCart;

use App\Entity\TOrder;
use Symfony\Component\HttpClient\HttpClient;

class Payment
{
    private $client;

    private $order;

    private $request;

    private $connection;

    private $expirationDate;

    private $merchant = 'carstoris';

    private $bankServer = 'https://securepayments.sberbank.ru';



    public function __construct(
        TOrder $order,
        string $url = ':443/payment/rest/register.do',
        array $connection = [
                'userName' => 'carstoris-api',
                'password' => '0nlinepr0m0__116'
             ])
    {
        $this->client = HttpClient::create();
        $this->order = $order;
        if(!isset($_ENV['PAYMENT_LOGIN'])){//Если платежи реальные то в .ENV файле не установлены настройки тестовых платежей
             $this->connection = $connection;
             $this->connection['url'] = $this->bankServer.$url;
        }
        else{//Если платежи тестовые то в .ENV файле установлены настройки тестовых платежей
            $this->connection = [
                'userName' => $_ENV['PAYMENT_LOGIN'],
                'password' => $_ENV['PAYMENT_PASSWORD']
            ];
            $this->connection['url'] = $_ENV['BANK_SERVER'].$url;
        }

        $date = new \DateTime('now + 3hours');
//        dd($date);
        $this->expirationDate = $date->format('y-m-d\TH:m:s');

        $this->request = $this->prepareRequest();

    }

    private function prepareRequest()
    {
//        dd($this->order->getAmountToPay());
//        dd($this->order->getId());
       return $this->connection['url'] .
           '?userName='.$this->connection['userName'].
           '&password='.$this->connection['password'].
           '&amount='.$this->order->getAmountToPay() * 100 .'&currency=643&language=ru&orderNumber='.$this->order->getId().
           '&returnUrl=https://'.$_SERVER['HTTP_HOST'].'/order/pay/status&jsonParams={"orderNumber":'.$this->order->getId().
           '}&pageView=DESKTOP&expirationDate='.$this->expirationDate.'&merchantLogin='.$this->merchant;
    }

    public function doOrderRequest()
    {
//        dd($this->expirationDate );
//        dd($this->request);2019-11-08T14:14:14 2019-18-10T00:30:00
        return $this
            ->client
            ->request(
            'GET',
            $this->request
        );
    }


    public function checkOrder()
    {
        $request =  $this->bankServer.'/payment/rest/getOrderStatusExtended.do' .
            '?userName='. $this->connection['userName'].
            '&password='. $this->connection['password'].
            '&orderId='. $this->order->getBankOrderId().
            '&orderNumber=' . $this->order->getId();

        return $this->client->request('GET', $request);

    }

}