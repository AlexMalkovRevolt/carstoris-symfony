<?php


namespace App\ShoppingCart;


use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Session
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function getSession()
    {
        return $this->session;
    }

}