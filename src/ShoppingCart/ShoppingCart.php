<?php


namespace App\ShoppingCart;


use App\Entity\Gift;
use App\Entity\Partner;
use App\Entity\Product;
use App\Entity\Promotion;
use App\ShoppingCart\ShoppingCartItem;

use App\Utils\AppVariables;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ShoppingCart
{
    private $items = [];
    private $cartAmount;
    private $session;
    private $cartAmountOldPrice;
    private $lastEditedItem=0;
    private $partner;
    private $linkPartner;
    private $gift;
    private $boxesWithLogoQuantity;
    private $promotedItems = [];

    public function __construct(SessionInterface $session)
    {

        $this->session = $session;
        if(isset( $session->get('shopping_cart')->items)){
            $this->items = $session->get('shopping_cart')->items;
        }
        if(isset( $session->get('shopping_cart')->lastEditedItem)){
            $this->lastEditedItem = $session->get('shopping_cart')->lastEditedItem;
        }

        if(isset($session->get('shopping_cart')->linkPartner)){
            $this->linkPartner = $session->get('shopping_cart')->linkPartner;
        }
        else{
            $this->linkPartner = false;
        }
        if(isset($session->get('shopping_cart')->partner)){
            $this->partner = $session->get('shopping_cart')->partner;
        }
        if(isset($session->get('shopping_cart')->gift)){
            $this->gift = $session->get('shopping_cart')->gift;
        }
        else{
            $this->gift = false;
        }
        $this->cartAmount = $this->getCartAmount();
        $this->cartAmountOldPrice = $this->getCartAmountOldPrice();

        $this->setBoxLogoDiscount($this->getBoxesWithLogoQuantity());

    }
    /**
     * Добавление единицы заказа в корзину.
     * @param \App\ShoppingCart\ShoppingCartItem $item
     */
    public function addItem(ShoppingCartItem $item)
    {

        $promoted = $this->findPromotedCategory(4, $item->getPrice());
        if($promoted['found']){
            if(!$promoted['reduced']){
                $this->items[$promoted['item']]->setPrice($this->items[$promoted['item']]->getPrice()-$this->items[$promoted['item']]->getDiscount());
            }

            $item->setPrice($item->getPrice()-$item->getDiscount());
        }

        $this->setBoxLogoDiscount($this->getBoxesWithLogoQuantity());

        /**
         * Проверка на наличие похожого товара
         */
        $foundId = $this->checkIdentity(
            $item->getParamsString()
        );

        /**
         * Если похожий товар не найден в корзине, добавляем его.
         * Если похожий товар найден, то просто увеличиваем колличество.
         **/
        if(is_null($foundId)){
            $id = (is_countable($this->items))? count($this->items) : 0;
            $item->setId($id);
            $this->items[$id] = $item;
            $this->setItems();
            $this->setLastEditedItem(count($this->items)-1);
        }else{
            $this->items[$foundId]->addQuantity($item->getQuantity());
            $this->setLastEditedItem($foundId);
        }

        $this->setCartToSession();
    }

    public function reduceQuantity(ShoppingCartItem $item)
    {
        $foundId = $this->checkIdentity($item->getParamsString());

        $this->items[$foundId]->minusQuantity($item->getQuantity());
        if($this->items[$foundId]->getQuantity()== 0){
            unset($this->items[$foundId]);
        }

        $this->setItems();
    }

    /**
     * Получение всех единиц заказа из корзины
     * @return mixed
     */
    public function getItems()
    {
        return((bool) $this->items)? $this->items : false;
    }

    /**
     * Получение единицы корзины по id
     * @param int $id
     * @return \App\ShoppingCart\ShoppingCartItem
     */
    public function getItem(int $id) :ShoppingCartItem
    {

        return $this->items[$id];
    }

    /**
     * Очистка корзины от единиц заказа. Опустошение.
     * Необходмо, к примеру, после проведения успешной оплаты.
     */
    public function cleanCart()
    {
        $this->items = [];
        $this->partner = null;
        $this->removeGift();
        $this->setItems();
        $this->setLastEditedItem(0);
    }

    /**
     * Вставка всех единиц заказа в карзину
     */
    private function setItems()
    {
        $this->setCartToSession();
    }


    /**
     * Удаление единицы корзины
     * @param $itemId
     */
    public function removeItem($itemId)
    {
        unset($this->items[$itemId]);
//        $this->setBoxLogoDiscount();
        $this->setItems();
    }


    /**
     * Получение суммы всех цен, всех единиц в корзине.
     * Общаяя стоимость заказа
     * @return int
     */
    public function getCartAmount()
    {
        //Ищем единицы с категорией 4, в которой есть скидка на покупку двух и более товаров.
        $found = $this->findPromotedCategoryToReduce(4);

        //Если единицы с такой же категорией найдены и такая единица только одна, и ее категория равна 4, то ставим цену без скидки
        if (count($found) > 1) {
           foreach($found as $itemKey){
//               dump($itemKey);
               if($this->items[$itemKey]->getQuantity() > 1){
                   if($this->items[$itemKey]->getPrice() == $this->items[$itemKey]->getProduct()->getPrice()){
                       $this->items[$itemKey]->setPrice(
                           $this->items[$itemKey]->getProduct()->getPrice() - $this->items[$itemKey]->getDiscount()
                       );
                   }
               }
           }
        }elseif(count($found) == 1){
            if($this->items[$found[0]]->getQuantity() > 1){
                $this->items[$found[0]]->setPrice(
                    $this->items[$found[0]]->getProduct()->getPrice() - $this->items[$found[0]]->getDiscount()
                );
            }
            elseif ($this->items[$found[0]]->getQuantity() == 1){
                $this->items[$found[0]]->setPrice(
                    $this->items[$found[0]]->getProduct()->getPrice()
                );
            }
        }

        $amount = 0;
        if($this->getItems()){
            foreach ($this->items as $item) {
                $amount += ($item->getItemAmount()) ;
            }
        }

        if(!$this->partner){
            return $amount;
        }
        else{
            return $amount - $this->getDiscount();
        }
    }

    /**
     * @return float|int
     */
    public function getCartAmountOldPrice()
    {
        $amountOld = 0;

        if((bool) $this->getItems()) {
            foreach ($this->items as $item) {
                $amountOld += $item->getProduct()->getPriceWithoutDiscount() * $item->getQuantity();
            }
        }

        return $this->cartAmountOldPrice = $amountOld;
    }

    /**
     * Получение количества единиц. Например, для использования в верхнем навбаре.
     * @return int
     */
    public function getItemsQuantity()
    {
        $amount = 0;

        if($this->getItems()){
            foreach ($this->items as $item){
                $amount += $item->getQuantity();
            }
        }
//        if($this->gift){
//            $amount ++;
//        }
        return $amount;
    }

    /**
     * Разбивает строку из запроса в массив параметров единицы карзины.
     * @param string $data
     * @return array
     */
    public function prepareParameters(string $data)
    {

            $data = explode('&', $data);

            $params = [];
//        dump($data);
            foreach($data as $el){

                $el = explode('=', $el);

                if($el[0] !== 'product-id'){

                    $params[$el[0]] = $el[1];
                }

            }

            return $params;

    }


    /**
     * Метод проверяет наличие в корзине одинаковых товаров.
     * Если карзина пустая или если товар с такими же параметрами не найден, возвращает null,
     * если не пустая проходит по каждому елементу карзины.
     * Если элемент содержит запрашиваемые параметры, метод возвращает ключ елемента(единицу товара),
     * если нет, продолжает проверку.
     * @param string $paramString
     * @return bool
     */
    private function checkIdentity(string $paramString)
    {
        if($this->items){
            foreach($this->items as $key => $item){
               if ($item->getParamsString() == $paramString){
                   return $key;
               }
            }
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getLastEditedItem()
    {
        return $this->lastEditedItem;
    }

    /**
     * @param int $itemId
     */
    public function setLastEditedItem(int $itemId = 0)
    {

        $this->setBoxLogoDiscount();
        $this->lastEditedItem = $itemId;
        $this->setCartToSession();
    }

    /**
     * @return Partner | null
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param Partner $partner
     */
    public function setPartner(Partner $partner): void
    {
        $this->partner = $partner;
        $this->setCartToSession();
    }

    /**
     * @param Partner $partner
     */
    public function removePartner(): void
    {
        $this->partner = null;
        $this->setCartToSession();
    }

    private function getPromoDiscount()
    {
        if($this->partner){
            return $this->getPartner()->getPromocodeDiscount();
        }
        return 0;
    }

    public function getDiscount()
    {
        return (isset($this->partner))? (int) $this->partner->getPromocodeDiscount() : 0 ;
    }

    public function setLinkPartner()
    {
        $this->linkPartner = true;
        $this->setCartToSession();

    }

    private function setCartToSession()
    {
        $this->session->set('shopping_cart', $this);
    }

    public function getLinkPartner()
    {
        return $this->linkPartner;
    }

    /**
     * @return mixed
     */
    public function getGift()
    {
        return $this->gift;
    }

    /**
     * @param Gift $gift
     */
    public function setGift(Gift $gift): void
    {
        $this->gift = $gift;
        $this->setCartToSession();
    }

    /**
     *
     */
    public function removeGift(): void
    {
        $this->gift = false;
        $this->setCartToSession();
    }

    public function reducePromotionAmount()
    {
        $promotedItem = $this->getPromotedItems();
        foreach($this->items as $item){
            $item->deductPrice(210);
        }
//        return $this->items;
        return $promotedItem;
    }

    private function getPromotedItems()
    {
        $promoted = [
            401 => [],
            402 => []
        ];
        foreach($this->items as $key => $item){

            if($item->getProductId() == 401 || $item->getProductId() == 402){
                $promoted[$item->getProductId()][] = $key;
            }
        }
        if(count($promoted[401])>$promoted[402]){
            $promoted[401] = array_slice($promoted[401], count($promoted[402]));
        }
        elseif(count($promoted[401])<$promoted[402]){
            $promoted[402] = array_slice($promoted[402], count($promoted[401]));
        }
        return $promoted;
    }

    private function findPromotedCategory(int $categoryId)
    {
        $return = [
            'found' => false,
            'reduced' => false
            ];
        foreach ($this->items as $key => $item){
            if($item->getProduct()->getCategory()->getId() == $categoryId){
                $return['found'] = true;
                if($item->getPrice() != $item->getProduct()->getPrice()){
                    $return['reduced'] = true;
                }
                else{
                    $return['item'] = $key;
                }
            }
        }
        return $return;
    }


    private function findPromotedCategoryToReduce(int $categoryId)
    {
        $return = [];
        foreach ($this->items as $key => $item){

            if($item->getProduct()->getCategory()->getId() == $categoryId){
                $return[] = $key;
            }
        }
        $return = (count($return) > 0)? $return: [];
        return $return;
    }

    /**
     * Метод прверяет, является ли единица корзины саквояжем
     * @param ShoppingCartItem $item
     * @return bool
     */
    private function isBox(ShoppingCartItem $item)
    {
        return ($item->getProduct()->getId() == 101);
    }

    /**
     * Метод устанавливает скидку на шильдики если саквояжей больше двух
     * @param array $indexes
     * @return $this
     */

    public function setBoxLogoDiscount()
    {
        $indexes = $this->getBoxesWithLogoQuantity();

        if(count($indexes) > 1){
           foreach ($indexes as $key){
              $this->items[$key]->setBoxShieldDiscount();
           }
        }
        elseif(count($indexes) == 1){
            if($this->items[$indexes[0]]->isWithLogo()){
                $this->items[$indexes[0]]->setPrice($this->items[$indexes[0]]->addShieldPrice());
            }
            else{
                $this->items[$indexes[0]]->setPrice($this->items[$indexes[0]]->getProduct()->getPrice());
            }

        }
        $this->setCartToSession();
        return $this;
    }

    /**
     * @return array
     */
    private function getBoxesWithLogoQuantity()
    {
        $itemsIndexes = [];
        foreach ($this->items as $key => $item){
            if($this->isBox($item)){
                for($count = 0; $count < $item->getQuantity(); $count++){
                    $itemsIndexes[] = $key;
                    $this->boxesWithLogoQuantity += $item->getQuantity();
                }
            }
        }
        return $itemsIndexes;
    }

}