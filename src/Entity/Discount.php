<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DiscountRepository")
 */
class Discount
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="integer")
     */
    private $discountAmount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $discountConditionAmount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maximumItemsToDiscount;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="discount")
     */
    private $product;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getDiscountAmount(): ?int
    {
        return $this->discountAmount;
    }

    public function setDiscountAmount(int $discountAmount): self
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    public function getDiscountConditionAmount(): ?int
    {
        return $this->discountConditionAmount;
    }

    public function setDiscountConditionAmount(?int $discountConditionAmount): self
    {
        $this->discountConditionAmount = $discountConditionAmount;

        return $this;
    }

    public function getMaximumItemsToDiscount(): ?int
    {
        return $this->maximumItemsToDiscount;
    }

    public function setMaximumItemsToDiscount(?int $maximumItemsToDiscount): self
    {
        $this->maximumItemsToDiscount = $maximumItemsToDiscount;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
            $product->setDiscount($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getDiscount() === $this) {
                $product->setDiscount(null);
            }
        }

        return $this;
    }
}
