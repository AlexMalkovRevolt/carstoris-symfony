<?php

namespace App\Entity;

use App\ShoppingCart\ShoppingCart;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Utils\PhoneFormaterTrait;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 */
class TOrder
{
    use PhoneFormaterTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $clientName;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private $clientPhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $clientEmail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $company_name;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    private $company_inn;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $amountToPay;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $amountPayed;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderItem", mappedBy="orderId", orphanRemoval=true)
     */
    private $orderItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Gift")
     */
    private $gift;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $bankOrderId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPayed;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Partner", inversedBy="Orders")
     */
    private $partner;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Delivery", inversedBy="tOrder", cascade={"persist", "remove"})
     */
    private $delivery;

    public function __construct()
    {

        $this->orderItems = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClientName(): ?string
    {
        return $this->clientName;
    }

    public function setClientName(?string $clientName): self
    {
        $this->clientName = $clientName;

        return $this;
    }

    public function getClientPhone(): ?string
    {
        return $this->formatPhone($this->clientPhone);
    }

    public function setClientPhone(string $clientPhone): self
    {
        $this->clientPhone = $clientPhone;

        return $this;
    }

    public function getClientEmail(): ?string
    {
        return $this->clientEmail;
    }

    public function setClientEmail(?string $clientEmail): self
    {
        $this->clientEmail = $clientEmail;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->company_name;
    }

    public function setCompanyName(?string $company_name): self
    {
        $this->company_name = $company_name;

        return $this;
    }

    public function getCompanyInn(): ?string
    {
        return $this->company_inn;
    }

    public function setCompanyInn(?string $company_inn): self
    {
        $this->company_inn = $company_inn;

        return $this;
    }

    public function getAmountToPay(): ?string
    {
        return $this->amountToPay;
    }

    public function setAmountToPay(?string $amountToPay): self
    {

        $this->amountToPay = (string) $amountToPay;

        return $this;
    }

    public function getAmountPayed(): ?string
    {
        return $this->amountPayed;
    }

    public function setAmountPayed(?string $amountPayed): self
    {
        $this->amountPayed = $amountPayed;

        return $this;
    }

    /**
     * @return Collection|OrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(OrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setOrderId($this);
        }

        return $this;
    }

    public function removeOrderItem(OrderItem $orderItem): self
    {
        if ($this->orderItems->contains($orderItem)) {
            $this->orderItems->removeElement($orderItem);
            // set the owning side to null (unless already changed)
            if ($orderItem->getOrderId() === $this) {
                $orderItem->setOrderId(null);
            }
        }

        return $this;
    }

    public function getGift(): ?Gift
    {
        return $this->gift;
    }

    public function setGift(?Gift $gift): self
    {
        $this->gift = $gift;

        return $this;
    }

    public function getBankOrderId(): ?string
    {
        return $this->bankOrderId;
    }

    public function setBankOrderId(?string $bankOrderId): self
    {
        $this->bankOrderId = $bankOrderId;

        return $this;
    }

    public function getIsPayed(): ?bool
    {
        return $this->isPayed;
    }

    public function setIsPayed(?bool $isPayed): self
    {
        $this->isPayed = $isPayed;

        return $this;
    }

    public function getPartner(): ?Partner
    {
        return $this->partner;
    }

    public function setPartner(?Partner $partner): self
    {
        $this->partner = $partner;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getDelivery(): ?Delivery
    {
        return $this->delivery;
    }

    public function setDelivery(?Delivery $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }
}
