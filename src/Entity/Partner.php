<?php

namespace App\Entity;

use App\Validator\Constraints\NumberOnly;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Utils\PhoneFormaterTrait;
use Symfony\Component\Validator\Constraints as Assert;



use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\PartnerRepository")
 */
class Partner
{
    use PhoneFormaterTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contactName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adress;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true, nullable=true)
     */
    private $promocode;

    /**
     * @ORM\Column(type="text", nullable=true, nullable=true)
     */
    private $accountDetails;

    /**
     * @ORM\Column(type="bigint", nullable=true, nullable=true)
     *
     */
    private $promocodeDiscount;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductPartnerBonus", mappedBy="partner", orphanRemoval=true)
     */
    private $productPartnerBonuses;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $linkCode;

    /**
     * @var
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TOrder", mappedBy="partner", fetch="EXTRA_LAZY", orphanRemoval=true)
     */
    private $orders;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bagsBonus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $for_kidsBonus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $mantleBonus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $carpetsBonus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $for_animalsBonus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $foldersBonus;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     */
    private $phoneNumber;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    private $encoder;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TUser", inversedBy="partners", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $tuser;

    public function __construct()
    {

        $this->productPartnerBonuses = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;
        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    public function setContactName(string $contactName): self
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }


    public function getPassword(): ?string
    {

        return $this->password;
    }

    public function setPassword(string $password = null): self
    {

        $this->password = $password;

        return $this;
    }

    public function getPromocode(): ?string
    {
        return $this->promocode;
    }

    public function setPromocode(string $promocode): self
    {
        $this->promocode = $promocode;

        return $this;
    }

    public function getAccountDetails(): ?string
    {
        return $this->accountDetails;
    }

    public function setAccountDetails(?string $accountDetails): self
    {
        $this->accountDetails = $accountDetails;

        return $this;
    }

    public function getPromocodeDiscount(): ?string
    {
        return $this->promocodeDiscount;
    }

    public function setPromocodeDiscount(?string $promocodeDiscount): self
    {
        $this->promocodeDiscount = $promocodeDiscount;

        return $this;
    }

    /**
     * @return Collection|ProductPartnerBonus[]
     */
    public function getProductPartnerBonuses(): Collection
    {
        return $this->productPartnerBonuses;
    }

    public function addProductPartnerBonus(ProductPartnerBonus $productPartnerBonus): self
    {
        if (!$this->productPartnerBonuses->contains($productPartnerBonus)) {
            $this->productPartnerBonuses[] = $productPartnerBonus;
            $productPartnerBonus->setPartner($this);
        }

        return $this;
    }

    public function removeProductPartnerBonus(ProductPartnerBonus $productPartnerBonus): self
    {
        if ($this->productPartnerBonuses->contains($productPartnerBonus)) {
            $this->productPartnerBonuses->removeElement($productPartnerBonus);
            // set the owning side to null (unless already changed)
            if ($productPartnerBonus->getPartner() === $this) {
                $productPartnerBonus->setPartner(null);
            }
        }

        return $this;
    }

    public function getLinkCode(): ?string
    {
        return 'partner=' . $this->linkCode;
    }

    public function setLinkCode($linkCode): self
    {
        $this->linkCode = $linkCode;

        return $this;
    }

    /**
     * @return Collection|TOrder[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(TOrder $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setPartner($this);
        }

        return $this;
    }

    public function removeOrder(TOrder $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getPartner() === $this) {
                $order->setPartner(null);
            }
        }

        return $this;
    }

    public function getBagsBonus(): ?int
    {
        return $this->bagsBonus;
    }

    public function setBagsBonus(?int $bagsBonus): self
    {
        $this->bagsBonus = $bagsBonus;

        return $this;
    }

    public function getForkidsBonus(): ?int
    {
        return $this->for_kidsBonus;
    }

    public function setForkidsBonus(?int $for_kidsBonus): self
    {
        $this->for_kidsBonus = $for_kidsBonus;

        return $this;
    }

    public function getMantleBonus(): ?int
    {
        return $this->mantleBonus;
    }

    public function setMantleBonus(?int $mantleBonus): self
    {
        $this->mantleBonus = $mantleBonus;

        return $this;
    }

    public function getCarpetsBonus(): ?int
    {
        return $this->carpetsBonus;
    }

    public function setCarpetsBonus(?int $carpetsBonus): self
    {
        $this->carpetsBonus = $carpetsBonus;

        return $this;
    }

    public function getForanimalsBonus(): ?int
    {
        return $this->for_animalsBonus;
    }

    public function setForanimalsBonus(?int $for_animalsBonus): self
    {
        $this->for_animalsBonus = $for_animalsBonus;

        return $this;
    }

    public function getFoldersBonus(): ?int
    {
        return $this->foldersBonus;
    }

    public function setFoldersBonus(?int $foldersBonus): self
    {
        $this->foldersBonus = $foldersBonus;

        return $this;
    }

    public function getBonus(OrderItem $item)
    {
        $slug = $item->getProduct()->getCategory()->getSlug();

        $functionName = 'get' .preg_replace('/_*/', '', ucfirst($slug)) . 'Bonus';

       return (method_exists($this, $functionName))? $this->$functionName() : null;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->formatPhone($this->phoneNumber);
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function setEmail(string $email)
    {
        $this->tuser->setEmail($email);
    }

    public function getEmail()
    {
        if($this->tuser){
        return $this->tuser->getEmail();
        }
        return $this->email;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Функция считает начисленные бонусы.
     * @return float|int
     */
    public function getBonusesAmount()
    {
        $bonusAmount = 0;
        foreach($this->getOrders() as $order){
            foreach ($order->getOrderItems() as $orderItem){
                //                    dump($orderItem->getBonuse());
                if(!is_null($orderItem->getBonuse())){
                    $bonusAmount +=  $orderItem->getBonuse()->getAmount()*($orderItem->getBonuse()->getPercent()/100);
                }
            }
        }

        return $bonusAmount;
    }

    /**
     * @return int|string|null
     */
    public function getOrdersAmount()
    {
        $orderAmount = 0;
        foreach($this->getOrders() as $order){
            $orderAmount += $order->getAmountPayed();
        }
        return $orderAmount;
    }

    /**
     * @return float|int
     */
    public function getPayedBonusAmount()
    {
        $payedBonusAmount = 0;
        foreach($this->getOrders() as $order){

                foreach ($order->getOrderItems() as $orderItem){
                    if(!is_null($orderItem->getBonuse())){
                        if($orderItem->getBonuse()->getIsPayed()){
                            $payedBonusAmount +=  $orderItem->getBonuse()->getAmount()*($orderItem->getBonuse()->getPercent()/100);
                        }
                    }
                }

        }

        return $payedBonusAmount;
    }

    public function getSelectedProperties(array $propertieNames)
    {
        $properties = [];
        foreach($propertieNames as $propName){

            $properties[$propName] = $this->$propName;
        }

        return $properties;
    }

    public function getTuser(): ?TUser
    {
        return ($this->tuser)? $this->tuser: null;

    }

    public function setTuser(?TUser $tuser): self
    {
        $this->tuser = $tuser;

        return $this;
    }

    /**
     * @Assert\IsTrue(message="Тип значения должен быть числом")
     */
    public function isDiscountNumber()
    {
        return preg_match('/\d*/', $this->promocodeDiscount);
    }
}
