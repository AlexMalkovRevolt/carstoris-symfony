<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"cart"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"cart"})
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    private $description;

    /**
     * @ORM\Column(type="bigint")
     * @Groups({"cart"})
     */
    private $price;

    /**
     * @ORM\Column(type="bigint")
     * @Groups({"cart"})
     */
    private $priceWithoutDiscount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $singleName;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $logoPrice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $width;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $length;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $netto;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $brutto;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products", fetch="EXTRA_LAZY")
     */
    private $category;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Promotion", inversedBy="product", fetch="EXTRA_LAZY")
     */
    private $promotion;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Discount", inversedBy="product", fetch="EXTRA_LAZY")
     */
    private $discount;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $qualityDescription;

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }



    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPriceWithoutDiscount(): ?string
    {
        if($this->priceWithoutDiscount){
             return $this->priceWithoutDiscount;
        }
        else{
            return $this->getPrice();
        }
    }

    public function setPriceWithoutDiscount(string $priceWithoutDiscount): self
    {
        $this->priceWithoutDiscount = $priceWithoutDiscount;

        return $this;
    }

    public function getSingleName(): ?string
    {
        return $this->singleName;
    }

    public function setSingleName(?string $singleName): self
    {
        $this->singleName = $singleName;

        return $this;
    }

    public function getLogoPrice(): ?string
    {
        return $this->logoPrice;
    }

    public function setLogoPrice(?string $logoPrice): self
    {
        $this->logoPrice = $logoPrice;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(?int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(?int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getLength(): ?int
    {
        return $this->length;
    }

    public function setLength(?int $length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setWeight(?int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getNetto(): ?int
    {
        return $this->netto;
    }

    public function setNetto(?int $netto): self
    {
        $this->netto = $netto;

        return $this;
    }

    public function getBrutto(): ?int
    {
        return $this->brutto;
    }

    public function setBrutto(?int $brutto): self
    {
        $this->brutto = $brutto;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPromotion(): ?Promotion
    {
        return $this->promotion;
    }

    public function setPromotion(?Promotion $promotion): self
    {
        $this->promotion = $promotion;

        return $this;
    }

    public function getDiscount(): ?Discount
    {
        return $this->discount;
    }

    public function setDiscount(?Discount $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getQualityDescription(): ?string
    {
        return $this->qualityDescription;
    }

    public function setQualityDescription(?string $qualityDescription): self
    {
        $this->qualityDescription = $qualityDescription;

        return $this;
    }
}
