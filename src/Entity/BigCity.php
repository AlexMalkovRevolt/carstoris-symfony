<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BigCityRepository")
 */
class BigCity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameRu;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $index;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isMillionaireCity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameRu(): ?string
    {
        return $this->nameRu;
    }

    public function setNameRu(string $nameRu): self
    {
        $this->nameRu = $nameRu;

        return $this;
    }

    public function getIndex(): ?string
    {
        return $this->index;
    }

    public function setIndex(string $index): self
    {
        $this->index = $index;

        return $this;
    }

    public function getIsMillionaireCity(): ?bool
    {
        return $this->isMillionaireCity;
    }

    public function setIsMillionaireCity(?bool $isMillionaireCity): self
    {
        $this->isMillionaireCity = $isMillionaireCity;

        return $this;
    }
}
