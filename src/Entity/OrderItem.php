<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use App\ShoppingCart\ShoppingCartItem;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderItemRepository")
 */
class OrderItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=true)
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="bigint")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="TOrder", inversedBy="orderItems", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $orderId;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Bonus", mappedBy="orderItem", cascade={"persist", "remove"})
     */
    private $bonuse;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $orderedParameter = [];


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getOrderId(): ?TOrder
    {
        return $this->orderId;
    }

    public function setOrderId(?TOrder $orderId): self
    {
        $this->orderId = $orderId;

        return $this;
    }

    public function getBonuse(): ?Bonus
    {
        return $this->bonuse;
    }

    public function setBonuse(Bonus $bonuse): self
    {
        $this->bonuse = $bonuse;

        // set the owning side of the relation if necessary
        if ($this !== $bonuse->getOrderItem()) {
            $bonuse->setOrderItem($this);
        }

        return $this;
    }

    public function getOrderedParameter(): ?array
    {
        return $this->orderedParameter;
    }

    public function setOrderedParameter(?array $orderedParameter): self
    {
        $this->orderedParameter = $orderedParameter;

        return $this;
    }

}
