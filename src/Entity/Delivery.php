<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeliveryRepository")
 */
class Delivery
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TOrder", mappedBy="delivery", cascade={"persist", "remove"})
     */
    private $tOrder;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $locality;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelivery;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apartmentType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $apartmentNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deliveryCompany;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $building;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pvz;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deliveryType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tariff;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cityPostCode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getTOrder(): ?TOrder
    {
        return $this->tOrder;
    }

    public function setTOrder(?TOrder $tOrder): self
    {
        $this->tOrder = $tOrder;

        // set (or unset) the owning side of the relation if necessary
        $newDelivery = null === $tOrder ? null : $this;
        if ($tOrder->getDelivery() !== $newDelivery) {
            $tOrder->setDelivery($newDelivery);
        }
        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }

    public function setLocality(?string $locality): self
    {
        $this->locality = $locality;

        return $this;
    }

    public function getIsDelivery(): ?bool
    {
        return $this->isDelivery;
    }

    public function setIsDelivery(?bool $isDelivery): self
    {
        $this->isDelivery = $isDelivery;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getApartmentType(): ?string
    {
        return $this->apartmentType;
    }

    public function setApartmentType(?string $apartmentType): self
    {
        $this->apartmentType = $apartmentType;

        return $this;
    }

    public function getApartmentNumber(): ?int
    {
        return $this->apartmentNumber;
    }

    public function setApartmentNumber(?int $apartmentNumber): self
    {
        $this->apartmentNumber = $apartmentNumber;

        return $this;
    }

    public function getDeliveryCompany(): ?string
    {
        return $this->deliveryCompany;
    }

    public function setDeliveryCompany(string $deliveryCompany): self
    {
        $this->deliveryCompany = $deliveryCompany;

        return $this;
    }

    public function getBuilding(): ?string
    {
        return $this->building;
    }

    public function setBuilding(string $building): self
    {
        $this->building = $building;

        return $this;
    }

    public function getPvz(): ?string
    {
        return $this->pvz;
    }

    public function setPvz(?string $pvz): self
    {
        $this->pvz = $pvz;

        return $this;
    }

    public function getDeliveryType(): ?string
    {
        return $this->deliveryType;
    }

    public function setDeliveryType(?string $deliveryType): self
    {
        $this->deliveryType = $deliveryType;

        return $this;
    }

    public function getTariff(): ?int
    {
        return $this->tariff;
    }

    public function setTariff(?int $tariff): self
    {
        $this->tariff = $tariff;

        return $this;
    }

    public function getCityPostCode(): ?string
    {
        return $this->cityPostCode;
    }

    public function setCityPostCode(?string $cityPostCode): self
    {
        $this->cityPostCode = $cityPostCode;

        return $this;
    }
}
