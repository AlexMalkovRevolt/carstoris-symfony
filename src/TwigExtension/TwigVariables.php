<?php


namespace App\TwigExtension;


use App\Entity\TUser;
use App\Repository\CategoryRepository;
use App\ShoppingCart\ShoppingCart;
use App\Utils\BigCity;
use App\Utils\DataVariables;
use App\Utils\Location;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class TwigVariables extends \Twig\Extension\AbstractExtension implements \Twig\Extension\GlobalsInterface
{
    private $topMenu;
    private $shoppingCart;
    private $categories = [];
    private $user;
    private $tovar = [
        'товар',
        'товара',
        'товаров'
    ];
    private $workDays = [
        1 => 'Понедельник',
        2 => 'Вторник',
        3 => 'Среда',
        4 => 'Четверг',
        5 => 'Пятница',
        6 => 'Суббота',
        7 => 'Воскресенье'
    ];
    private $location;


    /**
     * TwigVariables constructor.
     * @param CategoryRepository $categoryRepository
     * @param EntityManagerInterface $em
     * @param Security $security
     * @param ShoppingCart $cart
     * @param Location $location
     * @param DataVariables $data
     */

    public function __construct(CategoryRepository $categoryRepository, EntityManagerInterface $em, Security $security, ShoppingCart $cart, SessionInterface $session, DataVariables $data)
    {
        $this->location =  Location::getInstance($session);

        $this->shoppingCart = $cart;
        if($security->getUser()){
            /**
             * В сессии хранятся данные о пользователе с момента последней аутентификации. И если, напрмер, изменится email
             * в базе данных, то в сесси все еще будет храниться старый email. Эта переменная создается, чтобы получить
             * обновленные данные текущего пользоваться.
             */
            $id = $security->getUser()->getId();
            $user = $em->getRepository(TUser::class)->findOneBy(
                ['id'=>$id]
            );
            $this->user =  $user;
        }
        $this->topMenu = $categoryRepository->findAll();
        $this->categories = $data->getCategories();
    }

    public function getGlobals()
    {
        return [
            'bigCities' => new BigCity(),
        ];
    }
    public function getTovar(int $quantity)
    {
        $end = substr($quantity, -1);
        if($end == 1){
            $end = 0;
        }
        elseif ($end > 1 && $end < 5){
            $end = 1;
        }
        else{
            $end = 2;
        }
        return $this->tovar[$end];
    }

    public function getTopMenu(): array
    {
        return $this->topMenu;
    }

    /**
     * @return ShoppingCart
     */
    public function getShoppingCart(): ShoppingCart
    {

//        dd($this->shoppingCart);
        return $this->shoppingCart;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    public function getShoppingCartItemParams()
    {
        $items = $this->shoppingCart->getItems();
//        dd($items);
    }

    /**
     *
     * @return mixed
     */
    public function getUser() :Tuser
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }


    /**
     * @return array
     */
    public function getWorkDays(): array
    {
        return $this->workDays;
    }

    public function getCityWithType()
    {
//        dd($this->location->getCityWithType());
        return $this->location->getCityWithType();
    }

    public function getBigCity()
    {
        return new BigCity();

    }

    public function wrapToTag(string $word, string $tag, $index = 0)
    {

    }

}