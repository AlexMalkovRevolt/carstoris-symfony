<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 */

class NumberOnly extends Constraint
{

    public $message = 'Данные могут быть только числом.';

}