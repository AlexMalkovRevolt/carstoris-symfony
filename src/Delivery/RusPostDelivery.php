<?php


namespace App\Delivery;


use Symfony\Component\HttpClient\HttpClient;

class RusPostDelivery
{
    private $url = 'https://otpravka-api.pochta.ru';
    private $token = '64cqTrFjQUI5HqLmoKN8U4HL1_acFLnY';
    private $client;
    private $authorizationKey = 'ZDI0NTAxNzBAeWFuZGV4LnJ1OtCkNdC10YTQtNGE0LzRiDXQtdGEX18=';

    public function __construct()
    {
        $this->client = HttpClient::create();
    }

    public function execute(string $url, array $headers)
    {

        return $this->client->request('GET', $url, ['headers' => $headers]);
    }

    public function getOPSesOfCity(string $city)
    {
        $url  = $this->url .'/postoffice/1.0/settlement.offices.codes?settlement=' .$city;

        $headers = [
            'Authorization' => 'AccessToken ' . $this->token,
            'Content-Type' => 'application/json;charset=UTF-8',
            'X-User-Authorization' => 'Basic ' . $this->authorizationKey
        ];
        $indexes = $this->execute($url, $headers)->toArray();
//        dd($indexes);
        $OPSes = [];
        foreach ($indexes as $key => $index){
            $url = $this->url  . '/postoffice/1.0/' . $index;
//            $statusCode = 400;
//            while($statusCode != 200){
//
//                $statusCode = $ops->getStatusCode();
//            }
            $ops = $this->execute($url, $headers);
            if($ops->getStatusCode() !== 200){
                continue;
            }
            $OPSes[] = $ops->toArray();

        }

        return $OPSes;

    }
}