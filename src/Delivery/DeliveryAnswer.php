<?php


namespace App\Delivery;


class DeliveryAnswer
{
    private $pvz=[];


    public function getPvz()
    {
        return $this->pvz;
    }

    public function setPvz(array $pvz)
    {
        $this->pvz = $pvz;
    }
}