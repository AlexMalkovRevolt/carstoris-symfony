<?php


namespace App\Delivery;


use App\ShoppingCart\ShoppingCart;
use App\Utils\ArrayReducer;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\ResponseInterface;


class CdekIntegrator
{
    use ArrayReducer;
    private $client;
    private $tariffsList = [
            'cc' => 136, //Посылка склад склад
            'cd' => 137, //Посылка склад дверь
            'dc' => 138, //Посылка дверь склад
            'dd' => 139, //Посылка дверь дверь
            ];
    private $tariffFlips = [];
    private $cdekMethod;
    private $url;
    private $requestMethod = 'GET';
    private $serializer;
    private $sdekMethods = [
        'getPvz' => 'pvzlist/v1/xml'
    ];


    public function __construct()
    {
        $this->tariffFlips = array_flip($this->tariffsList);
        $this->client = HttpClient::create();
//        $this->client->;
        $this->url = $_ENV['CDEK_SERVER'];
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * @param string $cdekMethod
     */
    public function setCdekMethod(string $cdekMethod): void
    {
        $this->cdekMethod = $cdekMethod;
    }


    /**
     * @return ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function getResponse(): ResponseInterface
    {
        return $this->client->request(
            $this->requestMethod,
            $this->setUrlForRequest(),
            [ 'headers' =>[
                    'account' => $_ENV['CDEK_ACCOUNT'],
                    'secure_password' => $_ENV['CDEK_SECUREPASSWORD']
                ]
            ]);
    }

    /**
     * @return string
     */
    private function setUrlForRequest(): string
    {
        return $this->url . $this->cdekMethod;
    }

    /**
     * @param mixed $requestMethod
     */
    public function setRequestMethod($requestMethod): void
    {
        $this->requestMethod = $requestMethod;
    }

    public function getPvzs(string $cityPostcode, $settings = [])
    {
        $this->cdekMethod = '/pvzlist/v1/xml';
        $parameters = '?citypostcode=' .$cityPostcode . '&';

        foreach($settings as $key => $setting){
            $parameters .= $key .'=' . $setting . '&';
        }
        $parameters = substr_replace($parameters, '', -1);

        $this->cdekMethod .= $parameters;
//        dd($this->getResponse());
        $result = $this->serializer->deserialize(
            $this->getResponse()->getContent(),
            DeliveryAnswer::class,
            'xml' )->getPvz();

        if(count($result) == 0){
            return false;
        }
//        dd($result->getPvz());
        $reduced = [];

        if(!isset($result[0])){
            $result = [
              $result
            ];
        }
        foreach($result as $item){
            if(is_array($item)){
                $reducedItem = [];
                foreach ($item as $key1 => $itemParams){

                    if(is_array($itemParams)){
                        $reducedItemParam = [];
                        foreach($itemParams as $key2 => $i){
                            if(is_array($i)){
                                $reducedItemParam[$key2] = $this->deleteCharFromKeys('@', $i);
                            }else{
                                $reducedItemParam[$key2] = $i;
                            }
                        }
                        $itemParams = $reducedItemParam;
                        $reducedItem[$key1] = $this->deleteCharFromKeys('@', $itemParams);
                    }
                    else{
                        $reducedItem[$key1] = $itemParams;
                    }

                }
                $item = $reducedItem;
            }
            $reduced[] =  $this->deleteCharFromKeys('@', $item);
        }


        return $reduced;
    }

    public function getCost(ShoppingCart $cart, int $cityPostCode)
    {
//        $tarifsList = [137,136,10,11,5,7,8,9];

        $results = [];
        foreach($this->tariffsList as  $tariffKey => $tariff){
            $params = [
                'version'  => '1.0',
                'dateExecute' => date('Y-m-d', strtotime("+48 hours")), //"2020-08-02",
                'senderCityPostCode' => 420000,
                'authLogin'=>  $_ENV['CDEK_ACCOUNT'],
                'secure' =>  $_ENV['CDEK_SECUREPASSWORD'],
                'senderCountryCode' => 'ru',
                'receiverCityPostCode' => $cityPostCode,
                'tariffId' => $this->tariffsList[$tariffKey],
                'currency' => 'RUB',

                'goods' => []
            ];

            if($cart->getItems()){
                $items = $cart->getItems();
                foreach($items as $item){
                    $product = $item->getProduct();
                    $itemQuantity = $item->getQuantity();
                    for($i = 0; $i<$itemQuantity; $i++ ){
                        $params['goods'][] = [
                            'weight'=> ($product->getWeight())?$product->getWeight() / 1000 : .5,//значение передается в киллограммах, в базе храниться значение в граммах.
                            'length'=> ($product->getLength())? $product->getLength() : 50,//значение передается в сантиметрах
                            'width'=> ($product->getWidth())? $product->getWidth() : 30,//значение передается в сантиметрах
                            'height'=> ($product->getHeight())? $product->getHeight() :2//значение передается в сантиметрах
                        ];
                    }

                    $answer = $this->client->request(
                        $this->requestMethod,
                        'http://api.cdek.ru/calculator/calculate_tarifflist.php',
                            [ 'headers' =>[
                                'account' => $_ENV['CDEK_ACCOUNT'],
                                'secure_password' => $_ENV['CDEK_SECUREPASSWORD'],
                                'Content-Type' => 'application/json'
                            ],

                                'json' => $params

                        ]);
//                    dd($answer->toArray());
                    if(isset($answer->toArray()['error']) != 'error'){
                        $results[] = $answer->toArray();
//                        dd($results);
                    }

                }
            }
        }

        foreach($results as $key => $item){

            $results[$key]['result']['price'] =  round($item['result']['price'], 0, PHP_ROUND_HALF_UP);

            $results[$key]['result']['priceByCurrency'] =  round($item['result']['priceByCurrency'], 0, PHP_ROUND_HALF_UP);
        }
        return $results;
    }

    public function prepareCostAnswer(array $cost)
    {
        $costAnswer = [];
        foreach ($cost as $price){
            $price = $price['result'];
            $price['tariffCode'] = $this->getTariffFlips()[(int)$price['tariffId']];
            $costAnswer[$price['tariffCode']] = $price;
        }
        return $costAnswer;
    }



    public function getOrderVolumetricWeight(array $goods)
    {
        $amount = 0;
        foreach($goods as $item){
            if(isset($item['length']) && isset($item['width']) && isset($item['height']) ){
                $amount += $item['length'] * $item['width'] * $item['height'] / 5000;
            }
        }
        return $amount;
    }

    public function getCitiesList($size = 1, $postcode)
    {
        $params = "?size=$size&page=0&postcode=$postcode";
       $url =  'http://integration.cdek.ru/v1/location/cities/json' . $params;
        return json_decode($this->client->request(
            'GET',
            $url,
            [ 'headers' =>[
                'account' => $_ENV['CDEK_ACCOUNT'],
                'secure_password' => $_ENV['CDEK_SECUREPASSWORD']
            ]
            ])->getContent())[0];
    }

    public function getOrderWeight(array $goods)
    {

        $amount = 0;
        foreach($goods as $item){

                $amount += $item['weight'];

        }
        return $amount;

    }

    /**
     * @return array
     */
    public function getTariffsList(): array
    {
        return $this->tariffsList;
    }

    /**
     * @param array $tariffsList
     */
    public function setTariffsList(array $tariffsList): void
    {
        $this->tariffsList = $tariffsList;
    }

    /**
     * @return array
     */
    public function getTariffFlips(): array
    {
        return $this->tariffFlips;
    }

    /**
     * @param array $tariffFlips
     */
    public function setTariffFlips(array $tariffFlips): void
    {
        $this->tariffFlips = $tariffFlips;
    }

    /**
     * Метод получает все ПВЗ данного города($postcode), и возвращает требуемый($pvzCode)
     * @param string $postcode
     * @param string $pvzCode
     * @return array
     */
    public function getPvz(string $postcode, string $pvzCode): array
    {
        $pvzs = $this->getPvzs($postcode);
          if($pvzs){
            foreach($pvzs as $pvz){
                if(array_search($pvzCode, $pvz, true)){
                    return $pvz;
                }
            }
        }
    }

}