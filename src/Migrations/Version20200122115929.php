<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200122115929 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX uniq_312b3e16773a4ce8');
        $this->addSql('ALTER TABLE partner ALTER tuser_id SET NOT NULL');
        $this->addSql('CREATE INDEX IDX_312B3E16773A4CE8 ON partner (tuser_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX IDX_312B3E16773A4CE8');
        $this->addSql('ALTER TABLE partner ALTER tuser_id DROP NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX uniq_312b3e16773a4ce8 ON partner (tuser_id)');
    }
}
