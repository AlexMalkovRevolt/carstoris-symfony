<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version21190904153945 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {

        $products = [
            [
                'id' => 101,
                'category_id' => 1,
                'name' => 'Саквояжи',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 5900
            ],
            [
                'id' => 301,
                'category_id' => 3,
                'name' => 'Коврики передние',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 0
            ],
            [
                'id' => 302,
                'category_id' => 3,
                'name' => 'Коврики задние',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 0
            ],
            [
                'id' => 303,
                'category_id' => 3,
                'name' => 'Коврики в салон',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 0
            ],
            [
                'id' => 304,
                'category_id' => 3,
                'name' => 'Коврик в багажник',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 0
            ],

            [
                'id' => 201,
                'category_id' => 2,
                'name' => 'На передние сиденья',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 5900
            ],
            [
                'id' => 202,
                'category_id' => 2,
                'name' => 'На задние сиденья',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 5900
            ],
            [
                'id' => 203,
                'category_id' => 2,
                'name' => 'Полный комплект',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 5900
            ],
            [
                'id' => 401,
                'category_id' => 4,
                'name' => 'на спинку',
                'description' => '',
                'price' => 1790,
                'price_without_discount' => 2190
            ],
            [
                'id' => 402,
                'category_id' => 4,
                'name' => 'на половину сиденья',
                'description' => '',
                'price' => 2190,
                'price_without_discount' => 2590
            ],
            [
                'id' => 501,
                'category_id' => 5,
                'name' => 'папки',
                'description' => '',
                'price' => 900,
                'price_without_discount' => 1350
            ],
            [
                'id' => 502,
                'category_id' => 5,
                'name' => 'папки на молнии',
                'description' => '',
                'price' => 1100,
                'price_without_discount' => 1600
            ],
            [
                'id' => 601,
                'category_id' => 6,
                'name' => 'в салон',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 5900
            ],
            [
                'id' => 602,
                'category_id' => 6,
                'name' => 'в салон',
                'description' => '',
                'price' => 3750,
                'price_without_discount' => 5900
            ],
            [
                'id' => 603,
                'category_id' => 6,
                'name' => 'в салон',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 5900
            ],
            [
                'id' => 604,
                'category_id' => 6,
                'name' => 'в салон',
                'description' => '',
                'price' => 3150,
                'price_without_discount' => 5900
            ]
        ];
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $sql ='';

        foreach ($products as $product){
            $name = $product['name'];
            $description = $product['description'];
            $category = $product['category_id'];
            $price = $product['price'];
            $price2 = $product['price_without_discount'];
            $id = $product['id'];

            $sql = "INSERT INTO product (id, category_id, name, description, price, price_without_discount) VALUES (";

            $sql .= "$id, $category, '$name', '$description', $price, $price2";

            $sql .= ');';

            $this->addSql($sql);

        }

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
    }
}
