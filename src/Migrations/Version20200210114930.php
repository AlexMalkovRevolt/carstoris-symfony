<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200210114930 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE promition_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE promition (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE promition_product (promition_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(promition_id, product_id))');
        $this->addSql('CREATE INDEX IDX_2BD22D4AD5F2F813 ON promition_product (promition_id)');
        $this->addSql('CREATE INDEX IDX_2BD22D4A4584665A ON promition_product (product_id)');
        $this->addSql('ALTER TABLE promition_product ADD CONSTRAINT FK_2BD22D4AD5F2F813 FOREIGN KEY (promition_id) REFERENCES promition (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE promition_product ADD CONSTRAINT FK_2BD22D4A4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT fk_d34a04ad139df194');
        $this->addSql('DROP INDEX idx_d34a04ad139df194');
        $this->addSql('ALTER TABLE product DROP promotion_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE promition_product DROP CONSTRAINT FK_2BD22D4AD5F2F813');
        $this->addSql('DROP SEQUENCE promition_id_seq CASCADE');
        $this->addSql('DROP TABLE promition');
        $this->addSql('DROP TABLE promition_product');
        $this->addSql('ALTER TABLE product ADD promotion_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT fk_d34a04ad139df194 FOREIGN KEY (promotion_id) REFERENCES promotion (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d34a04ad139df194 ON product (promotion_id)');
    }
}
