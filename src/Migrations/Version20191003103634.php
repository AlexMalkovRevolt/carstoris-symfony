<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191003103634 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE option_value DROP CONSTRAINT fk_249ce55ca7c41d6f');
        $this->addSql('DROP SEQUENCE option_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE option_set_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE option_value_id_seq CASCADE');
        $this->addSql('DROP TABLE option');
        $this->addSql('DROP TABLE option_value');
        $this->addSql('DROP TABLE option_set');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE option_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE option_set_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE option_value_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE option (id INT NOT NULL, name VARCHAR(100) NOT NULL, description TEXT DEFAULT NULL, price BIGINT DEFAULT NULL, thumbnail VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE option_value (id INT NOT NULL, order_item_id INT NOT NULL, option_id INT NOT NULL, int_value INT DEFAULT NULL, bool_value BOOLEAN DEFAULT NULL, string_value VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_249ce55ca7c41d6f ON option_value (option_id)');
        $this->addSql('CREATE INDEX idx_249ce55ce415fb15 ON option_value (order_item_id)');
        $this->addSql('CREATE TABLE option_set (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE option_value ADD CONSTRAINT fk_249ce55ca7c41d6f FOREIGN KEY (option_id) REFERENCES option (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE option_value ADD CONSTRAINT fk_249ce55ce415fb15 FOREIGN KEY (order_item_id) REFERENCES order_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
