<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200128143234 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE product ADD height INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD width INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD length INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD weight INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD netto INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD brutto INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product DROP height');
        $this->addSql('ALTER TABLE product DROP width');
        $this->addSql('ALTER TABLE product DROP length');
        $this->addSql('ALTER TABLE product DROP weight');
        $this->addSql('ALTER TABLE product DROP netto');
        $this->addSql('ALTER TABLE product DROP brutto');
    }
}
