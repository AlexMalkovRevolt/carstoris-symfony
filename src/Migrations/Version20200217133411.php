<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200217133411 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE sessions');
        $this->addSql('ALTER TABLE partner ALTER company_name DROP NOT NULL');
        $this->addSql('ALTER TABLE partner ALTER contact_name DROP NOT NULL');
        $this->addSql('ALTER TABLE partner ALTER link_code DROP NOT NULL');
        $this->addSql('ALTER TABLE tuser ALTER email DROP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE sessions (sess_id VARCHAR(128) NOT NULL, sess_data BYTEA NOT NULL, sess_time INT NOT NULL, sess_lifetime INT NOT NULL, PRIMARY KEY(sess_id))');
        $this->addSql('ALTER TABLE partner ALTER company_name SET NOT NULL');
        $this->addSql('ALTER TABLE partner ALTER contact_name SET NOT NULL');
        $this->addSql('ALTER TABLE partner ALTER link_code SET NOT NULL');
        $this->addSql('ALTER TABLE tuser ALTER email SET NOT NULL');
    }
}
