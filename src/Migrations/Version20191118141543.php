<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191118141543 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE partner ADD bags_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD carpets_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD for_animals_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD folders_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner DROP box_bonus');
        $this->addSql('ALTER TABLE partner DROP carpet_bonus');
        $this->addSql('ALTER TABLE partner DROP foranimals_bonus');
        $this->addSql('ALTER TABLE partner DROP folder_bonus');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE partner ADD box_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD carpet_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD foranimals_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD folder_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner DROP bags_bonus');
        $this->addSql('ALTER TABLE partner DROP carpets_bonus');
        $this->addSql('ALTER TABLE partner DROP for_animals_bonus');
        $this->addSql('ALTER TABLE partner DROP folders_bonus');
    }
}
