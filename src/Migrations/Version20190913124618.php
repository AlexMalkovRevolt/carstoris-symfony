<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190913124618 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE auto_brand_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE bonuse_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE category_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE delivery_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE gift_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE option_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE option_set_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE option_value_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE order_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE partner_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_partner_bonus_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shopping_cart_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shopping_cart_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE torder_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE transition_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE auto_brand (id INT NOT NULL, name VARCHAR(255) NOT NULL, logo VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE bonuse (id INT NOT NULL, partner_id INT NOT NULL, order_item_id INT NOT NULL, transition_id INT NOT NULL, is_payed BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5F957A079393F8FE ON bonuse (partner_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5F957A07E415FB15 ON bonuse (order_item_id)');
        $this->addSql('CREATE INDEX IDX_5F957A078BF1A064 ON bonuse (transition_id)');
        $this->addSql('CREATE TABLE category (id INT NOT NULL, name VARCHAR(100) NOT NULL, description TEXT DEFAULT NULL, menu_order SMALLINT DEFAULT NULL, slug VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE delivery (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE gift (id INT NOT NULL, name VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, thumbnail TEXT DEFAULT NULL, is_active BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN gift.thumbnail IS \'(DC2Type:array)\'');
        $this->addSql('CREATE TABLE option (id INT NOT NULL, product_id INT NOT NULL, name VARCHAR(100) NOT NULL, description TEXT DEFAULT NULL, price BIGINT DEFAULT NULL, thumbnail VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5A8600B04584665A ON option (product_id)');
        $this->addSql('CREATE TABLE option_set (id INT NOT NULL, product_id INT NOT NULL, option_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_506AF59B4584665A ON option_set (product_id)');
        $this->addSql('CREATE INDEX IDX_506AF59BA7C41D6F ON option_set (option_id)');
        $this->addSql('CREATE TABLE option_value (id INT NOT NULL, order_item_id INT NOT NULL, option_id INT NOT NULL, int_value INT DEFAULT NULL, bool_value BOOLEAN DEFAULT NULL, string_value VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_249CE55CE415FB15 ON option_value (order_item_id)');
        $this->addSql('CREATE INDEX IDX_249CE55CA7C41D6F ON option_value (option_id)');
        $this->addSql('CREATE TABLE order_item (id INT NOT NULL, shopping_cart_item_id INT NOT NULL, product_id INT NOT NULL, order_id_id INT NOT NULL, quantity INT NOT NULL, price BIGINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_52EA1F093B3A089F ON order_item (shopping_cart_item_id)');
        $this->addSql('CREATE INDEX IDX_52EA1F094584665A ON order_item (product_id)');
        $this->addSql('CREATE INDEX IDX_52EA1F09FCDAEAAA ON order_item (order_id_id)');
        $this->addSql('CREATE TABLE partner (id INT NOT NULL, company_name VARCHAR(255) NOT NULL, contact_name VARCHAR(255) NOT NULL, adress VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, promocode VARCHAR(255) NOT NULL, account_details TEXT DEFAULT NULL, promocode_discount BIGINT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product (id INT NOT NULL, category_id INT NOT NULL, name VARCHAR(100) NOT NULL, description TEXT DEFAULT NULL, price BIGINT NOT NULL, price_without_discount BIGINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D34A04AD12469DE2 ON product (category_id)');
        $this->addSql('CREATE TABLE product_partner_bonus (id INT NOT NULL, partner_id INT NOT NULL, product_id INT NOT NULL, amount INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5649CFED9393F8FE ON product_partner_bonus (partner_id)');
        $this->addSql('CREATE INDEX IDX_5649CFED4584665A ON product_partner_bonus (product_id)');
        $this->addSql('CREATE TABLE shopping_cart (id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE shopping_cart_item (id INT NOT NULL, shopping_cart_id INT NOT NULL, product_id INT NOT NULL, quantity INT NOT NULL, price BIGINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E59A1DF445F80CD ON shopping_cart_item (shopping_cart_id)');
        $this->addSql('CREATE INDEX IDX_E59A1DF44584665A ON shopping_cart_item (product_id)');
        $this->addSql('CREATE TABLE torder (id INT NOT NULL, gift_id INT DEFAULT NULL, client_name VARCHAR(255) DEFAULT NULL, client_phone VARCHAR(10) NOT NULL, client_email VARCHAR(255) DEFAULT NULL, company_name VARCHAR(255) DEFAULT NULL, company_inn VARCHAR(12) DEFAULT NULL, amount_to_pay BIGINT DEFAULT NULL, amount_payed BIGINT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_127A8AF197A95A83 ON torder (gift_id)');
        $this->addSql('CREATE TABLE transition (id INT NOT NULL, order_id_id INT NOT NULL, amount BIGINT NOT NULL, source VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F715A75AFCDAEAAA ON transition (order_id_id)');
        $this->addSql('ALTER TABLE bonuse ADD CONSTRAINT FK_5F957A079393F8FE FOREIGN KEY (partner_id) REFERENCES partner (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonuse ADD CONSTRAINT FK_5F957A07E415FB15 FOREIGN KEY (order_item_id) REFERENCES order_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonuse ADD CONSTRAINT FK_5F957A078BF1A064 FOREIGN KEY (transition_id) REFERENCES transition (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE option ADD CONSTRAINT FK_5A8600B04584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE option_set ADD CONSTRAINT FK_506AF59B4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE option_set ADD CONSTRAINT FK_506AF59BA7C41D6F FOREIGN KEY (option_id) REFERENCES option (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE option_value ADD CONSTRAINT FK_249CE55CE415FB15 FOREIGN KEY (order_item_id) REFERENCES order_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE option_value ADD CONSTRAINT FK_249CE55CA7C41D6F FOREIGN KEY (option_id) REFERENCES option (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F093B3A089F FOREIGN KEY (shopping_cart_item_id) REFERENCES shopping_cart_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F094584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT FK_52EA1F09FCDAEAAA FOREIGN KEY (order_id_id) REFERENCES torder (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_partner_bonus ADD CONSTRAINT FK_5649CFED9393F8FE FOREIGN KEY (partner_id) REFERENCES partner (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_partner_bonus ADD CONSTRAINT FK_5649CFED4584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shopping_cart_item ADD CONSTRAINT FK_E59A1DF445F80CD FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shopping_cart_item ADD CONSTRAINT FK_E59A1DF44584665A FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE torder ADD CONSTRAINT FK_127A8AF197A95A83 FOREIGN KEY (gift_id) REFERENCES gift (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transition ADD CONSTRAINT FK_F715A75AFCDAEAAA FOREIGN KEY (order_id_id) REFERENCES torder (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD12469DE2');
        $this->addSql('ALTER TABLE torder DROP CONSTRAINT FK_127A8AF197A95A83');
        $this->addSql('ALTER TABLE option_set DROP CONSTRAINT FK_506AF59BA7C41D6F');
        $this->addSql('ALTER TABLE option_value DROP CONSTRAINT FK_249CE55CA7C41D6F');
        $this->addSql('ALTER TABLE bonuse DROP CONSTRAINT FK_5F957A07E415FB15');
        $this->addSql('ALTER TABLE option_value DROP CONSTRAINT FK_249CE55CE415FB15');
        $this->addSql('ALTER TABLE bonuse DROP CONSTRAINT FK_5F957A079393F8FE');
        $this->addSql('ALTER TABLE product_partner_bonus DROP CONSTRAINT FK_5649CFED9393F8FE');
        $this->addSql('ALTER TABLE option DROP CONSTRAINT FK_5A8600B04584665A');
        $this->addSql('ALTER TABLE option_set DROP CONSTRAINT FK_506AF59B4584665A');
        $this->addSql('ALTER TABLE order_item DROP CONSTRAINT FK_52EA1F094584665A');
        $this->addSql('ALTER TABLE product_partner_bonus DROP CONSTRAINT FK_5649CFED4584665A');
        $this->addSql('ALTER TABLE shopping_cart_item DROP CONSTRAINT FK_E59A1DF44584665A');
        $this->addSql('ALTER TABLE shopping_cart_item DROP CONSTRAINT FK_E59A1DF445F80CD');
        $this->addSql('ALTER TABLE order_item DROP CONSTRAINT FK_52EA1F093B3A089F');
        $this->addSql('ALTER TABLE order_item DROP CONSTRAINT FK_52EA1F09FCDAEAAA');
        $this->addSql('ALTER TABLE transition DROP CONSTRAINT FK_F715A75AFCDAEAAA');
        $this->addSql('ALTER TABLE bonuse DROP CONSTRAINT FK_5F957A078BF1A064');
        $this->addSql('DROP SEQUENCE auto_brand_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE bonuse_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE category_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE delivery_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE gift_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE option_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE option_set_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE option_value_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE order_item_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE partner_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_partner_bonus_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shopping_cart_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shopping_cart_item_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE torder_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE transition_id_seq CASCADE');
        $this->addSql('DROP TABLE auto_brand');
        $this->addSql('DROP TABLE bonuse');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE delivery');
        $this->addSql('DROP TABLE gift');
        $this->addSql('DROP TABLE option');
        $this->addSql('DROP TABLE option_set');
        $this->addSql('DROP TABLE option_value');
        $this->addSql('DROP TABLE order_item');
        $this->addSql('DROP TABLE partner');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_partner_bonus');
        $this->addSql('DROP TABLE shopping_cart');
        $this->addSql('DROP TABLE shopping_cart_item');
        $this->addSql('DROP TABLE torder');
        $this->addSql('DROP TABLE transition');
    }
}
