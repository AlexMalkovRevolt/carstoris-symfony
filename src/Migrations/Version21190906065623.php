<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Utils\SqlHelper;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version21190906065623 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $brands = [
            ['id'=>1, 'name' => 'Alfa Romeo'],
            ['id'=>2, 'name' => 'BMW'],
            ['id'=>3, 'name' => 'Audi'],
            ['id'=>4, 'name' => 'Cadillac'],
            ['id'=>5, 'name' => 'Ford'],
            ['id'=>6, 'name' => 'Chevrolet'],
            ['id'=>7, 'name' => 'Daewoo'],
            ['id'=>8, 'name' => 'Kia'],
            ['id'=>9, 'name' => 'Fiat'],
            ['id'=>10, 'name' => 'Hummer'],
            ['id'=>12, 'name' => 'Lancia'],
            ['id'=>13, 'name' => 'Lexus'],
            ['id'=>14, 'name' => 'Mazda'],
            ['id'=>15, 'name' => 'Mini'],
            ['id'=>16, 'name' => 'Saab'],
            ['id'=>17, 'name' => 'Seat'],
            ['id'=>18, 'name' => 'Mercedes'],
            ['id'=>19, 'name' => 'Volvo'],
            ['id'=>21, 'name' => 'Acura'],
            ['id'=>23, 'name' => 'Brilliance'],
            ['id'=>25, 'name' => 'BYD'],
            ['id'=>26, 'name' => 'Chery'],
            ['id'=>27, 'name' => 'Chrysler'],
            ['id'=>28, 'name' => 'Citroen'],
            ['id'=>30, 'name' => 'Dodge'],
            ['id'=>31, 'name' => 'FAW'],
            ['id'=>32, 'name' => 'Geely'],
            ['id'=>34, 'name' => 'Great Wall'],
            ['id'=>35, 'name' => 'Honda'],
            ['id'=>36, 'name' => 'Hyundai'],
            ['id'=>37, 'name' => 'Infiniti'],
            ['id'=>38, 'name' => 'Isuzu'],
            ['id'=>39, 'name' => 'Jaguar'],
            ['id'=>40, 'name' => 'Jeep'],
            ['id'=>42, 'name' => 'Land Rover'],
            ['id'=>43, 'name' => 'Lifan'],
            ['id'=>47, 'name' => 'MG'],
            ['id'=>48, 'name' => 'Mitsubishi'],
            ['id'=>49, 'name' => 'Nissan'],
            ['id'=>50, 'name' => 'Opel'],
            ['id'=>51, 'name' => 'Peugeot'],
            ['id'=>52, 'name' => 'Pontiac'],
            ['id'=>53, 'name' => 'Porsche'],
            ['id'=>54, 'name' => 'Renault'],
            ['id'=>56, 'name' => 'Rover'],
            ['id'=>59, 'name' => 'Skoda'],
            ['id'=>60, 'name' => 'Smart'],
            ['id'=>61, 'name' => 'Subaru'],
            ['id'=>62, 'name' => 'Suzuki'],
            ['id'=>63, 'name' => 'Toyota'],
            ['id'=>64, 'name' => 'Volkswagen'],
            ['id'=>65, 'name' => 'ГАЗ'],
            ['id'=>66, 'name' => 'УАЗ'],
            ['id'=>69, 'name' => 'Ssang Yong'],
            ['id'=>70, 'name' => 'ВАЗ'],
            ['id'=>77, 'name' => 'Ravon'],
            ['id'=>79, 'name' => 'Genesis'],
        ];


        foreach ($brands as $brand){

            $sqlHelper = new SqlHelper($brand);
            $sql = "INSERT INTO auto_brand (id, name) VALUES (";
            $values = $sqlHelper->makeValuesList();
            $sql .= "$values";
            $sql .= ');';

            //dd($sql);
            $this->addSql($sql);
        }

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
    }
}
