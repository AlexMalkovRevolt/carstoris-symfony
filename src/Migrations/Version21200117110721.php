<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version21200117110721 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $sql = "";
        for($i = 6; $i<=233; $i++){
            $sql = "INSERT INTO torder (id, client_name, client_phone,  client_email, amount_payed, created_at)" .
                "VALUES ($i, 'Дамир', '79370047776', 'sdf@fdsdf.ru', 66150,  '2020-01-17 11:18:42' )";
            $this->addSql($sql);
        }

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
    }
}
