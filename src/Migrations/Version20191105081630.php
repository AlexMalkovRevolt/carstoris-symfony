<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191105081630 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE bonuse DROP CONSTRAINT fk_5f957a078bf1a064');
        $this->addSql('DROP SEQUENCE bonuse_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE transition_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE bonus_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE transaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bonus (id INT NOT NULL, partner_id INT NOT NULL, order_item_id INT NOT NULL, transition_id INT NOT NULL, is_payed BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9F987F7A9393F8FE ON bonus (partner_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9F987F7AE415FB15 ON bonus (order_item_id)');
        $this->addSql('CREATE INDEX IDX_9F987F7A8BF1A064 ON bonus (transition_id)');
        $this->addSql('CREATE TABLE transaction (id INT NOT NULL, order_id_id INT NOT NULL, amount BIGINT NOT NULL, source VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_723705D1FCDAEAAA ON transaction (order_id_id)');
        $this->addSql('ALTER TABLE bonus ADD CONSTRAINT FK_9F987F7A9393F8FE FOREIGN KEY (partner_id) REFERENCES partner (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus ADD CONSTRAINT FK_9F987F7AE415FB15 FOREIGN KEY (order_item_id) REFERENCES order_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonus ADD CONSTRAINT FK_9F987F7A8BF1A064 FOREIGN KEY (transition_id) REFERENCES transaction (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1FCDAEAAA FOREIGN KEY (order_id_id) REFERENCES torder (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE bonuse');
        $this->addSql('DROP TABLE transition');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE bonus DROP CONSTRAINT FK_9F987F7A8BF1A064');
        $this->addSql('DROP SEQUENCE bonus_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE transaction_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE bonuse_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE transition_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bonuse (id INT NOT NULL, partner_id INT NOT NULL, order_item_id INT NOT NULL, transition_id INT NOT NULL, is_payed BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_5f957a079393f8fe ON bonuse (partner_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_5f957a07e415fb15 ON bonuse (order_item_id)');
        $this->addSql('CREATE INDEX idx_5f957a078bf1a064 ON bonuse (transition_id)');
        $this->addSql('CREATE TABLE transition (id INT NOT NULL, order_id_id INT NOT NULL, amount BIGINT NOT NULL, source VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_f715a75afcdaeaaa ON transition (order_id_id)');
        $this->addSql('ALTER TABLE bonuse ADD CONSTRAINT fk_5f957a079393f8fe FOREIGN KEY (partner_id) REFERENCES partner (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonuse ADD CONSTRAINT fk_5f957a07e415fb15 FOREIGN KEY (order_item_id) REFERENCES order_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bonuse ADD CONSTRAINT fk_5f957a078bf1a064 FOREIGN KEY (transition_id) REFERENCES transition (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE transition ADD CONSTRAINT fk_f715a75afcdaeaaa FOREIGN KEY (order_id_id) REFERENCES torder (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE bonus');
        $this->addSql('DROP TABLE transaction');
    }
}
