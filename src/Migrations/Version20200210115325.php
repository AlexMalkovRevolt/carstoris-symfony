<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200210115325 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE promition_product DROP CONSTRAINT fk_2bd22d4ad5f2f813');
        $this->addSql('DROP SEQUENCE promition_id_seq CASCADE');
        $this->addSql('CREATE TABLE promotion_product (promotion_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(promotion_id, product_id))');
        $this->addSql('CREATE INDEX IDX_8B37F297139DF194 ON promotion_product (promotion_id)');
        $this->addSql('CREATE INDEX IDX_8B37F2974584665A ON promotion_product (product_id)');
        $this->addSql('ALTER TABLE promotion_product ADD CONSTRAINT FK_8B37F297139DF194 FOREIGN KEY (promotion_id) REFERENCES promotion (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE promotion_product ADD CONSTRAINT FK_8B37F2974584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE promition');
        $this->addSql('DROP TABLE promition_product');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE promition_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE promition (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE promition_product (promition_id INT NOT NULL, product_id INT NOT NULL, PRIMARY KEY(promition_id, product_id))');
        $this->addSql('CREATE INDEX idx_2bd22d4ad5f2f813 ON promition_product (promition_id)');
        $this->addSql('CREATE INDEX idx_2bd22d4a4584665a ON promition_product (product_id)');
        $this->addSql('ALTER TABLE promition_product ADD CONSTRAINT fk_2bd22d4ad5f2f813 FOREIGN KEY (promition_id) REFERENCES promition (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE promition_product ADD CONSTRAINT fk_2bd22d4a4584665a FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE promotion_product');
    }
}
