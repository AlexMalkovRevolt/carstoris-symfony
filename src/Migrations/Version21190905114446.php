<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Utils\SqlHelper;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version21190905114446 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $gifts = [
                      [
                          'name' => 'Салфетка замша',
                            'description' => '',
                            'thumbnail' => '',
                            'is_active' => true
                      ],
                      [
                            'name' => 'Салфетка микрофибра',
                            'description' => '',
                            'thumbnail' => '',
                            'is_active' => true
                      ],
                    [
                        'name' => 'Блокнот с ручкой',
                        'description' => '',
                        'thumbnail' => '',
                        'is_active' => true
                    ],

        ];

        $sql ='';
        $id = 1;
        /** Автоматическое создание запроса для добавления данных */
        foreach ($gifts as $gift){
            $sqlHelper = new SqlHelper($gift);
            $columns = $sqlHelper->makeColumnList();
            $sql = "INSERT INTO gift (id, $columns) VALUES (";
            $values = $sqlHelper->makeValuesList();
            $sql .= "$id, $values";
            $sql .= ');';
            //dd($sql);
            $this->addSql($sql);
            $id++;
        }

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
    }
}
