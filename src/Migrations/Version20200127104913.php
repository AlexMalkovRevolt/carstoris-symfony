<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200127104913 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE delivery ADD country VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery ADD region VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery ADD locality VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery ADD is_delivery BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery ADD street VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery ADD apartment_type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery ADD apartment_number INT DEFAULT NULL');
        $this->addSql('ALTER TABLE delivery ADD delivery_company VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE delivery DROP country');
        $this->addSql('ALTER TABLE delivery DROP region');
        $this->addSql('ALTER TABLE delivery DROP locality');
        $this->addSql('ALTER TABLE delivery DROP is_delivery');
        $this->addSql('ALTER TABLE delivery DROP street');
        $this->addSql('ALTER TABLE delivery DROP apartment_type');
        $this->addSql('ALTER TABLE delivery DROP apartment_number');
        $this->addSql('ALTER TABLE delivery DROP delivery_company');
    }
}
