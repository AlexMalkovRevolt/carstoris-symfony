<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191118134720 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE partner ADD box_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD for_kids_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD mantle_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD carpet_bonus INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD for_animals INT DEFAULT NULL');
        $this->addSql('ALTER TABLE partner ADD folder_bonus INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE partner DROP box_bonus');
        $this->addSql('ALTER TABLE partner DROP for_kids_bonus');
        $this->addSql('ALTER TABLE partner DROP mantle_bonus');
        $this->addSql('ALTER TABLE partner DROP carpet_bonus');
        $this->addSql('ALTER TABLE partner DROP for_animals');
        $this->addSql('ALTER TABLE partner DROP folder_bonus');
    }
}
