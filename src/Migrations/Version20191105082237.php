<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191105082237 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE bonus DROP CONSTRAINT fk_9f987f7a8bf1a064');
        $this->addSql('DROP INDEX idx_9f987f7a8bf1a064');
        $this->addSql('ALTER TABLE bonus RENAME COLUMN transition_id TO transaction_id');
        $this->addSql('ALTER TABLE bonus ADD CONSTRAINT FK_9F987F7A2FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9F987F7A2FC0CB0F ON bonus (transaction_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE bonus DROP CONSTRAINT FK_9F987F7A2FC0CB0F');
        $this->addSql('DROP INDEX IDX_9F987F7A2FC0CB0F');
        $this->addSql('ALTER TABLE bonus RENAME COLUMN transaction_id TO transition_id');
        $this->addSql('ALTER TABLE bonus ADD CONSTRAINT fk_9f987f7a8bf1a064 FOREIGN KEY (transition_id) REFERENCES transaction (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_9f987f7a8bf1a064 ON bonus (transition_id)');
    }
}
