<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Faker\Factory;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191204092214 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $factory = new Factory();
        $faker =  $factory::create('ru_RU');

        $password = '$argon2i$v=19$m=65536,t=4,p=1$N1JHamdrdjlkeUIwTzJILg$s9OrEHH1FGyjsj/3+IeIlTj5Bmg/WjuuwpNKZhJeL7o';

//
//
//        for($i = 20; $i<50; $i++){
//            $date = new \DateTime('NOW');
//            $sql1 = " INSERT INTO tuser (id, email, roles, password, created_at) VALUES (";
//
//            $sql1 .= "$i, '$faker->email', '[\"ROLE_PARTNER\"]', '$password', '2019-12-02 16:03:46') ";
//            $this->addSql($sql1);
//        }

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
    }
}
