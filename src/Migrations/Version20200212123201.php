<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200212123201 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE production_id_seq CASCADE');
        $this->addSql('DROP TABLE production');
        $this->addSql('ALTER TABLE discount ADD discount_amount INT NOT NULL');
        $this->addSql('ALTER TABLE discount ADD discount_condition_amount INT DEFAULT NULL');
        $this->addSql('ALTER TABLE discount ADD maximum_items_to_discount INT DEFAULT NULL');
        $this->addSql('ALTER TABLE promotion RENAME COLUMN discount_persent TO discount_percent');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE production_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE production (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE promotion RENAME COLUMN discount_percent TO discount_persent');
        $this->addSql('ALTER TABLE discount DROP discount_amount');
        $this->addSql('ALTER TABLE discount DROP discount_condition_amount');
        $this->addSql('ALTER TABLE discount DROP maximum_items_to_discount');
    }
}
