<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version21190904153408 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {

        $this->abortIf($this->connection
                ->getDatabasePlatform()
                ->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $id = 1;
        $categories = [
            [
                'name' => 'Саквояжи',
                'description' => 'Саквояжи',
                'menu_order' => 1,
                'slug' => 'bags'
            ],
            [
                'name' => 'Накидки',
                'description' => 'Накидки',
                'menu_order' => 2,
                'slug' => 'mantles'
            ],
            [
                'name' => 'Коврики',
                'description' => 'Коврики',
                'menu_order' => 3,
                'slug' => 'carpets'
            ],
            [
                'name' => 'Для детей',
                'description' => 'Для детей',
                'menu_order' => 4,
                'slug' => 'for_kids'
            ],
            [
                'name' => 'Папки',
                'description' => 'Папки',
                'menu_order' => 5,
                'slug' => 'folders'
            ],
            [
                'name' => 'Для собак',
                'description' => 'Для собак',
                'menu_order' => 6,
                'slug' => 'for_animals'
            ],

        ];
        $query = '';
        foreach ($categories as $category){
            $name = $category['name'];
            $description = $category['description'];
            $order = $category['menu_order'];
            $slug = $category['slug'];
            $sql = "INSERT INTO category (id, name, description, menu_order, slug) VALUES (";

                $sql .= "$id, '$name', '$description', $order, '$slug'";

            $sql .= ")";
            $this->addSql($sql);
            $id++;

        }



    }

    public function down(Schema $schema) : void
    {

//        // this down() migration is auto-generated, please modify it to your needs
//        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
//
//        $this->addSql('CREATE SCHEMA public');
    }
}
