<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191003092836 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE shopping_cart_item DROP CONSTRAINT fk_e59a1df445f80cd');
        $this->addSql('ALTER TABLE order_item DROP CONSTRAINT fk_52ea1f093b3a089f');
        $this->addSql('DROP SEQUENCE shopping_cart_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shopping_cart_item_id_seq CASCADE');
        $this->addSql('DROP TABLE shopping_cart');
        $this->addSql('DROP TABLE shopping_cart_item');
        $this->addSql('ALTER TABLE option DROP CONSTRAINT fk_5a8600b04584665a');
        $this->addSql('DROP INDEX idx_5a8600b04584665a');
        $this->addSql('ALTER TABLE option DROP product_id');
        $this->addSql('ALTER TABLE option_set DROP CONSTRAINT fk_506af59b4584665a');
        $this->addSql('ALTER TABLE option_set DROP CONSTRAINT fk_506af59ba7c41d6f');
        $this->addSql('DROP INDEX idx_506af59b4584665a');
        $this->addSql('DROP INDEX idx_506af59ba7c41d6f');
        $this->addSql('ALTER TABLE option_set DROP product_id');
        $this->addSql('ALTER TABLE option_set DROP option_id');
        $this->addSql('DROP INDEX uniq_52ea1f093b3a089f');
        $this->addSql('ALTER TABLE order_item DROP shopping_cart_item_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE shopping_cart_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shopping_cart_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE shopping_cart (id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE shopping_cart_item (id INT NOT NULL, shopping_cart_id INT NOT NULL, product_id INT NOT NULL, quantity INT NOT NULL, price BIGINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_e59a1df44584665a ON shopping_cart_item (product_id)');
        $this->addSql('CREATE INDEX idx_e59a1df445f80cd ON shopping_cart_item (shopping_cart_id)');
        $this->addSql('ALTER TABLE shopping_cart_item ADD CONSTRAINT fk_e59a1df44584665a FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shopping_cart_item ADD CONSTRAINT fk_e59a1df445f80cd FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_item ADD shopping_cart_item_id INT NOT NULL');
        $this->addSql('ALTER TABLE order_item ADD CONSTRAINT fk_52ea1f093b3a089f FOREIGN KEY (shopping_cart_item_id) REFERENCES shopping_cart_item (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_52ea1f093b3a089f ON order_item (shopping_cart_item_id)');
        $this->addSql('ALTER TABLE option ADD product_id INT NOT NULL');
        $this->addSql('ALTER TABLE option ADD CONSTRAINT fk_5a8600b04584665a FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_5a8600b04584665a ON option (product_id)');
        $this->addSql('ALTER TABLE option_set ADD product_id INT NOT NULL');
        $this->addSql('ALTER TABLE option_set ADD option_id INT NOT NULL');
        $this->addSql('ALTER TABLE option_set ADD CONSTRAINT fk_506af59b4584665a FOREIGN KEY (product_id) REFERENCES product (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE option_set ADD CONSTRAINT fk_506af59ba7c41d6f FOREIGN KEY (option_id) REFERENCES option (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_506af59b4584665a ON option_set (product_id)');
        $this->addSql('CREATE INDEX idx_506af59ba7c41d6f ON option_set (option_id)');
    }
}
