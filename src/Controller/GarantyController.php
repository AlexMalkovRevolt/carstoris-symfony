<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GarantyController extends AbstractController
{
    /**
     * @Route("/garantiya", name="garanty")
     */
    public function index()
    {
        return $this->render('garanty/index.html.twig', [
            'breadcrumb' => 'Гарантийные условия',
            'title' => 'Гарантийные условия',
            'desc' => 'Гарантия на продукцию сайта carstoris.ru'
        ]);
    }
}
