<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CarpetsController extends BaseController
{
    /**
     * categoryId = 3
     * @Route("/kovriki/", name="carpets")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function index(Request $request)
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findBy([
                'category' => 3
            ],
            [
                'id'=>'ASC'
            ]);
//        dd($products);
        $related = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy([
                'id' => 101
            ]);

//        dd($products);
        $slug = 'carpets';
        $title = 'Коврики EVA, Купить Автоковрики eva по Доступным Ценам | carstoris.ru';
        $desc = 'Заказать автомобильные коврики eva от компании CarStoris. Мы производим автоковрики eva согласно параметрам вашего автомобиля и вашим цветовым предпочтениям';
        $breadcrumb = 'Коврики';
        return $this->render('product/index.html.twig', [
            'products' =>$products,
            'brands' => $this->getBrands(),
            'logos' => $this->getLogos(),
            'slug' => $slug,
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
            'related' => $related,
            'related_slag' => 'bags'
        ]);
    }
}
