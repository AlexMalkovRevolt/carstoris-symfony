<?php


namespace App\Controller;




use App\Utils\MailNotificator;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;

class ErrorController extends BaseController
{

    public function showException(Request $request, FlattenException $exception, MailNotificator $notificator)
    {
//        dd($request,$exception);
        $statusCode = $exception->getStatusCode();
        $rout = 'page' . $statusCode;
        if($rout == 'page404'){
            return $this->redirectToRoute($rout, [], 301);
        }
//        dd($exception->getTrace());
        $message = '<p>' . $exception->getMessage() . '</p><p>Файл:' . $exception->getTrace()[0]['file'] . '</p><p>Строка: ' . $exception->getTrace()[0]['line'] . '</p>' ;
        dd($message);
        $notificator->sendNotification('Ошибка на сайте carstoris.ru', 'email/errors-notification.html.twig', ['message' => $message], 'd2450170@yandex.ru',  'd2450170@yandex.ru');

        if(!$_ENV['APP_ENV'] == 'dev' ){
            die('Упс, что то пошло не так, мы работаем над этим');
        }
//
    }
}