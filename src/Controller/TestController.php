<?php

namespace App\Controller;


use App\Delivery\CdekIntegrator;
use App\Delivery\RusPostDelivery;
use App\ShoppingCart\ShoppingCart;
use App\Utils\BigCity;
use App\Utils\Dadata;
use App\Utils\DiscountPromotion;
use App\Utils\MailNotificator;
use DadataApiBundle\DadataApiBundle;
use phpDocumentor\Reflection\Location;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mime\Email;

class TestController extends BaseController
{
    use DiscountPromotion;

    /**
     * @Route("/test", name="test_test")
     */


    public function index(ShoppingCart $cart, SessionInterface $session, Request $request)
    {
        $location = \App\Utils\Location::getInstance($session);
//        dd($location);
        $post = new RusPostDelivery();
        dump($post->getOPSesOfCity('Казань')[4]);
        dd($post->getOPSesOfCity('Казань')[0]['phones']);

    }

    /**
     * @Route("/mail", name="test")
     */
    public function mail(MailerInterface $mailer)
    {


        $email = (new Email())
            ->from('d2450170@yandex.ru')
            ->to('d2450170@yandex.ru')
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again!')
            ->html('<p>See Twig integration for better HTML integration!</p>');

        $mailer->send($email);die;

    }

    public function eventDispatcher()
    {
        $dispatcher = new EventDispatcher();

    }
    private function getRange($max = 10)
    {
        $array = [];
        for($i=0; $i < $max; $i++){
            $array[] = $i;
        }
        return $array;
    }
}
