<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class InfopagesController extends BaseController
{
    /**
     * @Route("/o-kompanii/", name="aboutus")
     */
    public function index()
    {
        $title = 'О компании CarStoris';
        $desc = 'На данной странице Вы можете ознакомиться Вы можете узнать подробнее о компании CarStoris';
        $breadcrumb = 'О компании';
        return $this->render('infopages/aboutus.html.twig', [
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }

    /**
     * @Route("/foto-i-video/", name="media")
     */
    public function media()
    {
        $title = 'Фото и видео продукция компании CarStoris';
        $desc = 'На данной странице Вы можете посмотреть каталог фотографий и видео компании CarStoris';
        $breadcrumb = 'Фото и видео';
        return $this->render('infopages/media.html.twig', [
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }

    /**
     * @Route("/akcii/", name="promotions")
     */
    public function discounts()
    {
        $title = 'Акции и спецпредложения на автомобильные аксессуары';
        $desc = 'На данной странице Вы можете посмотреть акции и спецпредложения. Подберите подходящее для себя предложение';
        $breadcrumb = 'Акции';
        return $this->render('infopages/promotions.html.twig', [
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }

    /**
     * @Route("/comments/", name="comments")
     */
    public function comments()
    {
        $title = '';
        $desc = '';
        $breadcrumb = '';
        return $this->render('infopages/comments.html.twig', [
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }

    /**
     * @Route("/dlya-partnyorov/", name="forpartners")
     */
    public function for_partners()
    {
        $title = 'Партнерская программа компании CarStoris';
        $desc = 'На данной странице Вы можете ознакомиться с условиями и преимуществами партнерской программы';
        $breadcrumb = 'Нашим партнерам';
        return $this->render('infopages/forpartners.html.twig', [
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }

    /**
     * @Route("/oplata-i-dostavka/", name="payment")
     */
    public function payment()
    {
        $title = 'Информация о доставке и оплате компании CarStoris';
        $desc = 'На данной странице Вы можете ознакомиться с полезной информацией о способах оплаты и условиях доставки';
        $breadcrumb = 'Оплата и доставка';
        return $this->render('infopages/payment.html.twig', [
            'controller_name' => 'InfopagesController',
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }

    /**
     * @Route("/contacts/", name="contacts")
     */
    public function contacts()
    {
        $title = 'Контактная информация компании CarStoris';
        $desc = 'На данной странице Вы можете ознакомиться с контактными данными компании CarStoris';
        $breadcrumb = 'Контакты';
        return $this->render('infopages/contacts.html.twig', [
            'controller_name' => 'InfopagesController',
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }

//    /**
//     * @Route("/politika-konfidencialnosti", name="privacyPolicy")
//     */
//    public function privacyPolicy()
//    {
//        $title = 'Политика конфиденциальности компании CarStoris';
//        $desc = '...';
//        $breadcrumb = 'Политика конфиденциальности';
//        return $this->render('infopages/privacy-policy.html.twig', [
//            'controller_name' => 'InfopagesController',
//            'title' => $title,
//            'desc' => $desc,
//            'breadcrumb' => $breadcrumb,
//        ]);
//    }

    /**
     * @Route("/sitemap/", name="sitemap")
     */
    public function sitemap()
    {
        $title = 'Карта сайта компании CarStoris';
        $desc = 'На данной странице расположена карта сайта компании CarStoris';
        $breadcrumb = 'Карта сайта';
        return $this->render('infopages/sitemap.html.twig', [
            'controller_name' => 'InfopagesController',
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }
}
