<?php

namespace App\Controller;

use App\Delivery\CdekIntegrator;
use App\Entity\Bonus;
use App\Entity\Delivery;
use App\Entity\Gift;
use App\Entity\OrderItem;
use App\Entity\Partner;
use App\Entity\Product;
use App\Entity\TOrder;

use App\ShoppingCart\Payment;
use App\Utils\BigCity;
use App\Utils\Dadata;
use App\Utils\DataVariables;
use App\Utils\Location;
use App\Utils\MailNotificator;
use App\Utils\PaymentMessage;
use App\Utils\Time;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends BaseController
{
    use Time;
    /**
     * @Route("/order/", name="order")
     */
    public function index(SessionInterface $session)
    {

//        $this->getCart()->cleanCart();
        $title = 'Оформление заказа';
        $desc = 'На данной странице расположена форма оформления заказа компании CarStoris';
        $breadcrumb = $title;

        $location = Location::getInstance($session);

        $pvzs = [];
        $costAnswer = [];
        $deliveryDate = '';
        if($location->getCity() != "Казань"){
            $cdek = new CdekIntegrator();
            $cost = $cdek->getCost($this->getCart(), $location->getPostCode());
            $costAnswer = $cdek->prepareCostAnswer($cost);
            $pvzs = $cdek->getPvzs($location->getPostCode());
            $date = new \DateTime('+ ' . $costAnswer['cd']['deliveryPeriodMax'] .'days',  new \DateTimeZone('Europe/Moscow'));
            $deliveryDate = $this->rdate($date,'l, d F');
        }

        return $this->render('order/checkout.html.twig', [
            'title' => $title,
            'pvzs' => $pvzs,
            'cost' => $costAnswer,
            'location' => $location,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
            'deliveryDate' => $deliveryDate,
            'shoppingCart' => $this->getCart(),
        ]);
    }

    /**
     * @Route("/order/pay/", name="order-pay")
     */
    public function pay(Request $request, MailNotificator $notificator)
    {

//        dd($request->query);
        /**
         * ) Создать заказ
         * ) Перевести единицы из корзины в заказ
         * ) Добавить единицы корзины в базу
         * ) Сохранить заказ.
         * ) Подготовить ссылку для запроса в Сбер.
         * ) Сделать запрос в Сбер.
         * ) Сделать редирект на страницу оплаты.
         **/

//        dd($request->query);
        $order = new Torder();


        $date = new \DateTime('NOW');//Дата сооздания заказа
        $cdek = new CdekIntegrator();
        //Если клиент заказал доставку до ПВЗ, то получаем данные ПВЗ
        $pvzs  = [];
        if($request->query->get('cdek-pvz-celect')){
            $pvzs = $cdek->getPvzs(
                $request->query->get('city-postcode'));
        }
        $pvz = [];

        $deliveryParams = [
            'isDelivery' => $request->query->get('delivery-method') == 'delivery',
            'country' => ($request->query->get('country'))? $request->query->get('country'): null,
            'region' => ($request->query->get('region'))? $request->query->get('region'): null,
            'cityPostCode' => $request->query->get('city-postcode'),
            'locality' => (
                $request->query->get('locality'))?
                $request->query->get('locality'): null,
            'street' => ($request->query->get('street'))? $request->query->get('street'): null,
            'building' => ($request->query->get('building'))? $request->query->get('building'): '',
            'apartmentType' => ($request->query->get('apartment-type'))? $request->query->get('apartment-type'): null,
            'apartmentNumber' => ($request->query->get('apartment-no'))? $request->query->get('apartment-no'): null,

        ];
        $deliveryParams['pvz'] = $request->query->get('cdek-pvz-celect');
        if($request->query->get('transport-companies-select')){
            $deliveryParams['deliveryType'] = $request->query->get('transport-companies-select');
            $deliveryTariffs = [
                'intercity-bus' => 'cc',
                'to-door' => 'cd'
            ];

            $deliveryParams['tariff'] = $cdek->getTariffsList()[
                $deliveryTariffs[
                    $request->query->get('transport-companies-select')
                ]
            ];
        }

        $delivery = new Delivery();
        foreach($deliveryParams as $key =>$param){
            $methodName = 'set'. ucfirst($key);
            $delivery->$methodName($param);
        }
//        dd($delivery);
        $this->em->persist($delivery);

        $deliveryCost = 0;
        //Данные заказа
        if($delivery->getIsDelivery()){
            $deliveryCost = 100;
        }
        $order->setClientName($request->query->get('full-name'))
            ->setClientPhone(
                preg_replace('/\D/', '', $request->query->get('phone'))
            )
            ->setClientEmail($request->query->get('email'))
            ->setAmountToPay($this->getCart()->getCartAmount()+$deliveryCost)
            ->setCreatedAt($date)
            ->setDelivery($delivery);
        ;

        //Если клиент ввел промокод, то добавляем партнера в заказ
        if( (bool) $this->getCart()->getPartner()){
            $partner = $this->getDoctrine()
                ->getRepository(Partner::class )
                ->findOneBy([
                    'id' => $this
                        ->getCart()
                        ->getPartner()
                        ->getId()
                ]);
            $order->setPartner($partner);
        }
        if($this->getCart()->getGift()){
            $gift = $this->getDoctrine()->getRepository(Gift::class)->findOneBy([
               'id' =>  $this->getCart()->getGift()->getId()
            ]);

            $order->setGift($gift);
        }

        //Добавляем единицы заказа в заказ
//    dd($this->getCart()->getItems()->getProduct());
        if( $this->getCart()->getItems()){
            foreach ($this->getCart()->getItems() as $item){
//                dd($this->getProduct($item->getProduct()->getId()));
                $orderItem = new OrderItem();
                $orderItem
                    ->setOrderId($order)
                    ->setQuantity($item->getQuantity())
                    ->setProduct($this->getProduct($item->getProduct()->getId()))
                    ->setPrice($item->getPrice())
                    ->setOrderedParameter($item->getParams())
                    ;
                $this->em->persist($orderItem);
            }
        }
        else{
            //TODO: доработать ответ
            die('Неверные параметры заказа');
        }

        $this->em->persist($order);
        $this->em->flush();
        //Переопределяем переменную заказа заказом из базы, чтобы оперировать объектом
        $order = $this->getDoctrine()->getRepository(TOrder::class)
            ->findOneBy(
                ['id' => $order->getId()]
            );
//        dd($this->getCart()->getPartner());
        //Создание объекта платежа
        $payment = new Payment($order);
        //Выполнение платежа и получение ответа от Банка
        $bankResponse = $payment->doOrderRequest();
//        dd($payment);
        //Если статус ответа 200, то сохраняем данные заказа в базу
        if( 200 == $bankResponse->getStatusCode()){
            $bankResponse = json_decode(
                $bankResponse->getContent()
            );

            if(isset($bankResponse->orderId)){
                  $order->setBankOrderId($bankResponse->orderId);
            }

            if((bool) $this->getCart()->getPartner()){
                $order->setPartner($partner);
            }

            $this->em->persist($order);
            $this->em->flush();


//            $this->getCart()->cleanCart();

            // Редирект на страницу  банка для  провдения платежа.
//            dd($bankResponse);
//            return $this->redirect($bankResponse->formUrl);
            try{
                return $this->redirect($bankResponse->formUrl);die;
            }catch(\Throwable $e){

//                if(isset($_ENV['DEV_HOST']) && $_ENV['DEV_HOST'] == '178.205.56.109'  ){
//                    dd($payment->doOrderRequest());
//                }

                if(isset($bankResponse->orderId)){
                    $notificator->sendNotification(
                    'Ошибка на сайте carstoris.ru', 'email/errors-notification.html.twig',
                    ['message' => $bankResponse->orderId . ' / ' . $bankResponse->formUrl],
                    'd2450170@yandex.ru',  'd2450170@yandex.ru');die;
                }
                else{
                    $notificator->sendNotification(
                        'Ошибка на сайте carstoris.ru', 'email/errors-notification.html.twig',
                        ['message' => $bankResponse->errorCode . ' / ' . $bankResponse->errorMessage],
                        'd2450170@yandex.ru',  'd2450170@yandex.ru');die;
                }

            }
        }
        else{
            echo $bankResponse->getStatusCode();
            return $this->redirect($bankResponse->formUrl);

        }
    }

    /**
     * @Route("/order/pay/status/", name="order-pay-status")
     * @param MailNotificator $notificator
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function paymentStatus(MailNotificator $notificator, Request $request, EntityManagerInterface $em)
    {

        //Номер заказа в банковской системе

        $bankOrder = $request->query->get('orderId');
        $partner = $this->getCart()->getPartner();

        $order = $this->getDoctrine()
            ->getRepository(TOrder::class)
            ->findOneBy(
                ['bankOrderId' => $bankOrder]
            );

        $payment = new Payment($order);
        $paymentMessage = new PaymentMessage;
        $answer = json_decode($payment->checkOrder()->getContent());

        /**
         * Если возвращается код 200, и платеж прошел, очищаем карзину,
         * отправляем ответ в json, отмечаем закакз как оплаченный и отправляем на почту администратору уведомление.
         */
        if($payment
                ->checkOrder()
                ->getStatusCode() == 200 && isset($answer->actionCode) && $answer->actionCode == 0 && !($order->getIsPayed())){
                        $this->getCart()->cleanCart();

            $order->setAmountPayed($answer->amount/100);
            $order->setIsPayed(true);
            $cdek = new CdekIntegrator();
             $pvz = [];
             $delivery = $order->getDelivery();
             if(!is_null($delivery->getPvz())){
                 //Если доставка до склада, получаем данные склада (ПВЗ-пункт выдачи заказа)
                 $pvz = $cdek->getPvz($order->getDelivery()->getCityPostCode(), $order->getDelivery()->getPvz());
             }

            $notificator->sendNotification(
                'Оплата заказа',
                'email/order-payed.html.twig',
                [
                    'order' => $order,
                    'items' => $order->getOrderItems(),
                    'partner' => $partner,
                    'delivery' => $delivery,
                    'pvz' => $pvz
                ]
            );
            foreach($order->getOrderItems() as $item){
                if($order->getPartner()) {
                    if($order->getPartner()->getBonus($item)){
                        $bonus = new Bonus($order->getPartner(), $item);
                        $bonus->setPercent(
                            $order->getPartner()->getBonus($item)
                        )
                            ->setAmount($item->getPrice())
                        ;
                        $em->persist($bonus);
                        $em->flush();
                    }
                }
            }
            $em->persist($order);
            return $this->redirect('/order/pay/confirm');
        }
        else{

            $title = 'Отмена оплаты';
            $desc = 'На данной странице расположена информация об отказе платежа и его причинах';

            return $this->render('payment/decline.html.twig', [
                'title' =>  $title,
                'desc' =>  $desc,
                'breadcrumb' => $title,
                'thanks' => 'К сожалению, оплата отклонена банком!',
                'message' => (isset($answer->actionCode))?$paymentMessage->getMessage($answer->actionCode)[1]: $paymentMessage->getMessage($answer->errorCode)[1]
            ]);
        }

    }

    /**
     * @Route("/order/pay/confirm", name="pay-confirm")
     */
    public function payConfirm()
    {

        $title = 'Корзина';
        $desc = 'На данной странице расположена форма оформления заказа компании CarStoris';
                return $this->render('shoppingcart/shoppingcart.html.twig', [
                    'shoppingCart' => $this->getCart(),
                    'title' =>  $title,
                    'desc' =>  $desc,
                    'breadcrumb' => $title,
                    'thanks' => 'Спасибо за заказ!',
                    'message' => 'Ваш заказ оформлен, оплата проведена. Наш менеджер свяжется с Вами в ближайщее время.'
            ]);
    }



    /**
     * @Route("/order/code-activation/", name="promocode-activation")
     */
    public function activation(SessionInterface $session){
//        $this->getCart()->cleanCart();die;
            $request = new Request($_POST, $_GET);
            $promocode =  $request->get('promocode');
            if($promocode){
                try{
                $partner = $this->getDoctrine()
                    ->getRepository(Partner::class)
                    ->findOneBy([
                        'promocode' => $promocode
                    ]);

                 if($partner){
                    //

                     $this->getCart()->setPartner($partner, $session);
                        $answer = [
                            'cartAmount' => $this->getCart()->getCartAmount(),
                            'cartAmountOldPrice' => $this->getCart()->getCartAmountOldPrice(),
                            'promoCodeDiscount' => $this->getCart()->getPartner()->getPromocodeDiscount(),
                            'promocode' => $this->getCart()->getPartner()->getPromocode()
                        ];
//                        var_dump($answer);die;
                 }
                 else{
                     $answer = [
                         'message' => 'Partner Not Found'
                     ];
                 }
                echo $this->serialiser->serialize($answer, 'json');die;
                $session->set('promocode', $promocode);
                }catch (\Throwable $e){
                    echo $e;
                }
            }
            else{
               echo 'нет промокода';die;
            }

    }

    /**
     * @Route("/order/cdek/pvz/", name="promocode_activation", methods={"POST", "GET"})
     * @param Request $request
     */
    public function getPvz(Request $request)
    {

        if($request->get('citypostcode')){

            $citypostcode = $request->get('citypostcode');
            $cdek = new CdekIntegrator($_ENV['CDEK_SERVER']);

            $response = $cdek->getPvzs($citypostcode, [
//                'weightmax'=> 50,
//                "allowedcod" => 1
            ]);

            $cost = $cdek->getCost($this->getCart(), $citypostcode);

            if($response){
                $answer = [
                    'answer' => 'success',
                    'message' => $response,
                    'cost' => $cdek->prepareCostAnswer($cost),
                    'city' => $cdek->getCitiesList(1, $response[0]['PostalCode']),

                ];

                    $dadata = new Dadata();
                    $dadata->init();
                    $fields = array("query" => $response[0]['City'], "count" => 1);
                    $dadataCity = $dadata->suggest("address", $fields);
//                    var_dump($dadataCity["suggestions"][0]['data']['geo_lon']);die();
                    $answer['cords']['longitude'] = $dadataCity["suggestions"][0]['data']['geo_lon'];
                    $answer['cords']['latitude'] = $dadataCity['suggestions'][0]['data']['geo_lat'];


            }else{
                $answer = [
                  'answer' => 'error',
                  'message' => 'В выбранном городе нет пункта приема заказов CDEK'
                ];
            }

            echo json_encode($answer);die;

//            dd($response->getPvz()[0]['PhoneDetail']);
        }else{
            //TODO:
            die;
        }
    }
}
