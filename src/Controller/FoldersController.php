<?php

namespace App\Controller;

use App\Entity\Product;
use App\Utils\AppVariables;

use Symfony\Component\Routing\Annotation\Route;


class FoldersController extends BaseController
{
    /**
     * @Route("/papki/", name="folders")
     */
    public function index()
    {

        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findBy([
                'category' => 5
            ]);
        $related = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy([
                'id' => 101
            ]);
//        dd($products);
        $slug = 'folders';

        $title = 'Папка для Автодокументов, Купить в Машину по Доступной Цене | carstoris.ru';
        $desc = 'Купить недорого папку для документов от интернет-магазина Сarstoris! Высокое качество. Доставка по 
        всей России. Огромный выбор цветов материала, цветов нити и окантовки. Собственное производство. Отзывы на сайте. Звоните: +7 (927) 241-05-74';
        $breadcrumb = 'Папки';
        return $this->render('product/index.html.twig', [
            'products' => $products[1],
            'products2' => $products[0],
            'brands' => $this->getBrands(),
            'logos' => $this->getLogos(),
            'slug' => $slug,
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
            // 'title' => $products->getName(),
            'related' => $related
        ]);
    }
}
