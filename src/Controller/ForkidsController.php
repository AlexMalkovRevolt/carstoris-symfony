<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Promotion;
use Symfony\Component\Routing\Annotation\Route;

class ForkidsController extends BaseController
{
    /**
     * categoryId = 4
     * @Route("/dlya-detej-pod-kreslo/", name="for_kids")
     */
    public function index()
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findBy([
                'category' => 4
            ],
                ['id' => 'ASC']);

        $promotion = $this->em->getRepository(Promotion::class)
            ->findOneBy([
                'id'=> $products[0]->getPromotion()->getId()
            ]);

        $related = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy([
                'id' => 101
            ]);

        $promotionPrice = $products[0]->getPrice() + $products[1]->getPrice() - $promotion->getDiscountAmount();
        $promotionOldPrice = $products[0]->getPriceWithoutDiscount() + $products[1]->getPriceWithoutDiscount();
        $slug = 'forkids';
        $title = 'Защитные Накидки на Спинку Сиденья Автомобиля от Детей, Купить по Низким Ценам Накидку под Детское Кресло | carstoris.ru';
        $desc = 'Закажите детскую накидку на сиденье автомобиля и сохраните чистоту в салоне. Защитная накидка на сиденье автомобиля поможет вам уберечь интерьер';
        $breadcrumb = 'Для детей';

        return $this->render('product/index.html.twig', [
                        'products' => array_reverse($this->getProducts(4)),
                        'brands' => $this->getBrands(),
                        'logos' => $this->getLogos(),
                        'slug' => $slug,
                        'title' => $title,
                        'desc' => $desc,
                        'promotionPrice' => $promotionPrice,
                        'promotionOldPrice' => $promotionOldPrice,
                        'promotion' => $promotion,
                        'breadcrumb' => $breadcrumb,
                        'related' => $related,
        ]);
    }

    /**
     * categoryId = 4
     * @Route("/dlya-detej-na-spinku/", name="for_kids_na_spinku")
     */
    public function naSpinku()
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findBy([
            'category' => 4
            ],
            ['id' => 'ASC']);

        $promotion = $this->em->getRepository(Promotion::class)
            ->findOneBy([
                'id'=> $products[0]->getPromotion()->getId()
            ]);

        $related = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy([
                'id' => 101
            ]);

        $promotionPrice = $products[0]->getPrice() + $products[1]->getPrice() - $promotion->getDiscountAmount();
        $promotionOldPrice = $products[0]->getPriceWithoutDiscount() + $products[1]->getPriceWithoutDiscount();
        $slug = 'forkids';
        $title = 'Защитные Накидки на Спинку Сиденья Автомобиля от Детей, Купить по Низким Ценам Накидку под Детское Кресло | carstoris.ru';
        $desc = 'Закажите детскую накидку на сиденье автомобиля и сохраните чистоту в салоне. Защитная накидка на сиденье автомобиля поможет вам уберечь интерьер';
        $breadcrumb = 'Для детей';
        return $this->render('product/index.html.twig', [
            'products' => array_reverse($this->getProducts(4)),
            'brands' => $this->getBrands(),
            'logos' => $this->getLogos(),
            'slug' => $slug,
            'title' => $title,
            'desc' => $desc,
            'promotionPrice' => $promotionPrice,
            'promotionOldPrice' => $promotionOldPrice,
            'promotion' => $promotion,
            'breadcrumb' => $breadcrumb,
            'related' => $related,
        ]);
    }
}
