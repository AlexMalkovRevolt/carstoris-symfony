<?php

namespace App\Controller;


use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class BagsController extends BaseController
{
    /**
     * @Route("/sakvoyazhi/", name="bags")
     */
    public function index(Request $request)
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy([
                'id' => 101
            ]);

        $related = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy([
                'id' => 501
            ]);

//        dd($products);
        $slug = 'bags';

        $title = 'Саквояж в Багажник Автомобиля, Купить Автомобильный Саквояж с Доставкой | carstoris.ru';
        $desc = 'Купить саквояж в багажник по доступным ценам от интернет-магазина Сarstoris! Изготовим, упакуем и 
        отправим заказ в течение 48 часов. Эксклюзивный дизайн, ручная работа. Собственное производство. Отзывы на сайте. Гарантия. Звоните:+7 (927) 241-05-74';
        $breadcrumb = 'Саквояжи';
        return $this->render('product/index.html.twig', [
            'products' => $products,
            'brands' => $this->getBrands(),
            'logos' => $this->getLogos(),
            'slug' => $slug,
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
            // 'title' => $products->getName(),
            'related' => $related,
            'related_slag' => 'folders'
        ]);
    }

}
