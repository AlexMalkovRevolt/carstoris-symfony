<?php

namespace App\Controller;

use App\Entity\Bonus;
use App\Entity\OrderRequest;
use App\Entity\Partner;
use App\Entity\TOrder;
use App\Entity\TUser;
use App\Form\AddPartnerFormType;
use App\Form\EditPartnerFormType;
use App\Form\OrderWithDiscountForm;
use App\Repository\BonusRepository;
use App\Repository\PartnerRepository;
use App\ShoppingCart\Session;
use App\Utils\MailNotificator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use http\Message;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\UserProvider\UserProviderFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class AccountsController extends BaseController
{
    /**
     * @Route("/partner/", name="accounts_partner")
     * @param UserProviderInterface $userProvider
     * @param BonusRepository $bonusRepository
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(UserProviderInterface $userProvider, Security $security, BonusRepository $bonusRepository, Request $request, SessionInterface $session, EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {
//        dd($security->getUser()->getRoles());
        if($session->get('_security.last_username')){
            if(in_array('ROLE_ADMIN', $security->getUser()->getRoles()) && !in_array('ROLE_PARTNER', $security->getUser()->getRoles())){//["ROLE_ADMIN","ROLE_PARTNER"]
                return $this->redirect('/admin');
            }

            $partner = $this->getDoctrine()->getRepository(Partner::class)
                ->findOneBy(
                    ['tuser' => $security->getUser()]
                );



        }
        else{
            return $this->redirect('/login');
        }

        $query = $entityManager->createQuery(
            'SELECT sum(o.amountPayed) as ordersAmount 
            FROM App\Entity\TOrder o
            WHERE o.partner = :partner
            AND  o.amountPayed IS NOT NULL
           '
        )->setParameter('partner', $partner->getId());

        $ordersAmount = $query->getResult()[0]['ordersAmount'];


        $queryBuilder = $bonusRepository->findByPartner($partner);
        $pagination = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            20
        );
        $query = $entityManager->createQuery(
            'SELECT sum(b.amount) allBonusAmount 
            FROM App\Entity\Bonus b
            WHERE b.partner = :partner     
           '
        )->setParameter('partner', $partner->getId());

        $allBonusesAmount = $query->getResult()[0]['allBonusAmount'];



        $query = $entityManager->createQuery(
            'SELECT sum(b.amount) allBonusAmount 
            FROM App\Entity\Bonus b
            WHERE b.partner = :partner
            AND  b.isPayed IS NULL     
           '
        )->setParameter('partner', $partner->getId());
        $notPayedBonusesAmount =  ($query->getResult()[0]['allBonusAmount'])? $query->getResult()[0]['allBonusAmount'] : 0;

      //   returns an array of Product objects
        $orders = $this->getDoctrine()->getRepository(TOrder::class)
                ->findBy([
                    'partner' => $partner->getId()
                ])

        ;


        if($partner){

        }
        else{
            return $this->redirectToRoute('login');
        }

        $title = 'Партнер';
        $desc = 'Страница партнера';
        $breadcrumb = $title;

        return $this->render('accounts/partner.html.twig', [
            'pagination' => $pagination,
            'orders' => $orders,
            'ordersAmount' => ($ordersAmount)? $ordersAmount : 0,
            'partner' => $partner,
            'bonusAmount' => ($allBonusesAmount)? $allBonusesAmount : 0,
            'notPayedBonusesAmount' => $notPayedBonusesAmount,
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }

    /**
     *  @Route("/admin", name="accounts-admin")
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Security $security
     * @param SessionInterface $session
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function admin( UserPasswordEncoderInterface $passwordEncoder, Security $security, SessionInterface $session, Request $request, EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {

        if($request->getMethod() == 'POST'){
            if((int) $request->get('id')){
                $repository = $this->getDoctrine()
                    ->getRepository(Partner::class);

                $partner = $repository
                    ->findBy(['id'=>$request->get('id')])[0];
                try{
                    $entityManager->remove($partner);
                    $entityManager->flush();
                    echo 'ok';die;
                }catch(\Throwable $e){
                    echo $e;die;
                }
            }
            elseif(strip_tags($request->get('bonus-ids'))){
                $partnersIds = explode(',', strip_tags($request->get('bonus-ids')));
                $repository = $this->getDoctrine()
                    ->getRepository(Bonus::class);

                $bonuses = $repository->findBy([
                    'partner' => $partnersIds,
                    'isPayed' => false
                ]);

                foreach ($bonuses as $bonus){
                    $bonus->setIsPayed();
                }
                $entityManager->flush();
                echo json_encode($partnersIds);die;
            }

        }
        $partner = new Partner();

        $user = new TUser();
        $partner->setTuser($user);
        $form = $this->createForm(AddPartnerFormType::class, $partner);
        $editForm = $this->createForm(EditPartnerFormType::class, $partner);

        $form->handleRequest($request);
        $editForm->handleRequest($request);
        if($editForm->isSubmitted()){
            echo 'ok'; die;
        }
        if (($form->isSubmitted() && $form->isValid())) {

            $partner = $form->getData();
            $user = $this->getDoctrine()->getRepository(TUser::class)->findOneBy([
                'email' => $partner->getEmail()
        ]);
            if(!$user){
                $user = new TUser();
                $user->setPassword($passwordEncoder->encodePassword($user, $partner->getPassword()));
                $user->setEmail($partner->getEmail());
            }

            $user->setRoles([
                "ROLE_PARTNER"
            ])
            ;
            if(!$user){

                $entityManager->persist($user);
            }
           // $partner->setPassword($user->getPassword());
            $partner->setLinkCode($partner->getPromocode());
            $partner->setTuser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($partner);
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('accounts-admin');
        }

        $partners = $this->getDoctrine()->getRepository(Partner::class)->findAll();

        $data = [];
        $count = 0;
        foreach($partners as $partner){
//            dd($partner);
            $data[$count] =[];
            $data[$count]['num'] = (int)$count + 1;
            $data[$count]['id'] = $partner->getId();
            $data[$count]['partner'] = $partner->getCompanyName();
            $data[$count]['createdAt'] = $partner->getTuser()->getCreatedAt();
            $data[$count]['bonusesAmount'] = $partner->getBonusesAmount();
            $data[$count]['ordersAmount'] = $partner->getOrdersAmount();
            $data[$count]['payedBonusesAmount'] = $partner->getPayedBonusAmount();
            $data[$count]['bonusesToPay'] = $data[$count]['bonusesAmount'] - $data[$count]['payedBonusesAmount'] ;
            $count  ++;
        }
        $paginator = new Paginator();
        $elements = $paginator->paginate($data, 3, 20, array('preserveKeys' => true));
//        dd( $data);
        $breadcrumb = 'Админ';
        return $this->render('accounts/admin.html.twig', [
            'addPartnerForm' => $form->createView(),
            'editPartnerForm' => $editForm->createView(),
            'breadcrumb' => $breadcrumb,
            'partners' => $data,
            'title' => 'Личный кабинет администратора',
            'user' => $security->getUser(),
            'desc' =>  'Личный кабинет администратора',
        ]);
    }

    /**
     * @Route("/admin/edit", name="admin_edit", methods={"POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param Security $security
     * @param MailNotificator $mailer
     * @param EntityManagerInterface $entityManager
     */
    public function adminEdit(Request $request, UserPasswordEncoderInterface $encoder, Security $security,SessionInterface $session, MailNotificator $mailer, EntityManagerInterface $entityManager)
    {
        $submittedToken = $request->request->get('token');
        if ($this->isCsrfTokenValid('edit-admin', $submittedToken)) {
            $admin = $security->getUser();
            $user = $this->em->getRepository(TUser::class)->findOneBy([
                'id' => $admin->getId()
            ]);
            try{
                $email = $request->get('email');
                $password = $request->get('password');
                $password = $encoder->encodePassword($user, $password);
                if($request->get('email')){
                    $user->setEmail($email);
                }
                if($request->get('password')){
                    $user->setPassword($password);
                }

               $entityManager->flush();
                echo 'ok';die;
            }catch (\Throwable $e){
//                $mailer ->sendNotification(
//                    'Ошибка при изменении данных администратора' . $admin->getUsername(),
//                            'email/errors-notification.html.twig',
//                    [
//                        'message'=>$e
//                    ],
//                    'd24501770@yandex.ru'
//                );

                echo 'error';die();
            }
        }
        else{
            echo 'no token'; die;
        }

        return;
    }

    /**
     * @Route("/admin/partner-get/{id}", name="partner_get", methods={"GET"})
     * @param int $id
     * @param Request $request
     * @param MailNotificator $notificator
     * @param EntityManagerInterface $entityManager
     */
    public function partnerGet(int $id, Request $request, MailNotificator $mailer, EntityManagerInterface $entityManager)
    {
        if((int) $request->get('id')){
            try{
                $repository = $this->getDoctrine()->getRepository(Partner::class);
                $partner = $repository
                    ->findOneBy(['id' => $id]);
                $partnerData['status'] = 'success';
                $partnerData['data'] = $partner->getSelectedProperties([
                    'id',
                    'companyName',
                    'contactName',
                    'phoneNumber',
                    'adress',
                    'promocodeDiscount',
                    'accountDetails',
                    'bagsBonus',
                    'for_kidsBonus',
                    'mantleBonus',
                    'carpetsBonus',
                    'for_animalsBonus',
                    'foldersBonus',
                    'promocode',
                ]);

                $partnerData['data']['email'] = $partner->getTUser()->getEmail();
                $partnerData['data']['linkCode'] = $partner->getLinkCode();
                $encoders = [new JsonEncoder()];
                $normalizers = [new ObjectNormalizer()];
                $serializer = new Serializer($normalizers, $encoders);
                $jsonContent = $serializer->serialize($partnerData, 'json');
                echo $jsonContent;die;
            } catch(\Throwable $e){

                $mailer->sendNotification(
                    'Ошибка на сайте carstoris.ru в контроллере AccountsController, метод partnerGet()',
                    'email/simple-notification.html.twig',
                    [
                        'message' => $e
                    ]
                );
                echo '{"status": "error" ,"message": "Что то пошло не так, попробуйте позже!"}';die;
            }
        }

    }

    /**
     * @Route("/admin/partner-edit/{id}", name="partner_edit", methods={"POST"}, requirements={"id"="\d+"})
     * @param $id
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param PartnerRepository $partnerRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function partnerEdit($id, UserPasswordEncoderInterface $passwordEncoder, PartnerRepository $partnerRepository,  Request $request)
    {
        $partner = $partnerRepository->findOneBy([
            'id' => $id
        ]);

        $editForm = $this->createForm(EditPartnerFormType::class, $partner);
        /**
         * Если пароль не задан, удаляем его из формы, чтобы он не флэшился.
         */
        if($request->get('edit_partner_form')['password'] == ""){
            $editForm->remove('password');
        }
        $editForm->submit($request->request->get($editForm->getName()));

        if ($editForm->isSubmitted()) {
            $partner = $editForm->getData();
            $user = $partner->getTuser();
            $user->setRoles([
                "ROLE_PARTNER"
            ])
                ->setEmail($partner->getEmail());

            if($partner->getPassword()){
                $user->setPassword($passwordEncoder->encodePassword($user, $partner->getPassword()));
                $partner->setPassword($user->getPassword());
            }
            $partner->setLinkCode($partner->getPromocode());
            $partner->setTuser($user);

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->flush();
            return $this->redirectToRoute('accounts-admin');
        }
    }

    /**
     * @Route("/admin/partner-add", name="partner_add", methods={"POST"})
     * @param Request $request
     * @param TUser $user
     * @param EntityManagerInterface $entityManager
     */
    public function partnerAdd(Request $request, Security $security, EntityManagerInterface $entityManager)
    {

        if(!$request->get('email') == $security->getUser()->getEmail()){
            $user = new TUser();
        }
        else{
            $user = $security->getUser();
        }
        $partner = new Partner();
        $partner->setTuser($user);
//        dd($request->getContent());
        $form = $this->createForm(AddPartnerFormType::class, $partner);
        $form->handleRequest($request);
        if($form->isSubmitted()){

        }

    }
    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout()
    {
        // controller can be blank: it will never be executed!
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }

    /**
     *  @Route("/partner/change-info-request", name="partner_edit_request", methods={"POST"})
     */
    public function changePartnerInfoRequest(Request $request, Security $security, MailNotificator $mailer)
    {
        $message = $request->get('message');

        $token = explode('----', $request->get('token'));


        if($message){
            if($this->isCsrfTokenValid('edit-partner-request', $token[0])){
                $partner = $this->em->getRepository(Partner::class)
                    ->findOneBy(
                        ['id' => $token[1]]
                    );
                try{
                    $mailer ->sendNotification(
                        'Партнер "' . $partner->getCompanyName() . '" отправил запрос на изменение информации.',
                        'email/simple-notification.html.twig',
                        [
                            'message'=>$message
                        ]
                    );
                    echo 'ok';
                    die;
                }catch(\Throwable $e){
                    echo $e; die;
                }

            }
            else{
                echo 'no authority';
            }
        }
        else{
            echo 'no message'; die;
        }

    }

    /**
     * @Route("/admin/get-promocode/{promo}", name="accounts-partner")
     * @param $promo
     * @return bool
     */
    public function getIsPromoExist($promo)
    {
        $partnerCode = $this->getDoctrine()->getRepository(Partner::class)->findOneBy([
            'promocode' => strtoupper($promo)
        ]);
//        var_dump($partnerCode);die;
        if (is_null($partnerCode)) {
            echo 'true';
        } else {
            echo 'false';
        }
        die;
    }


}
