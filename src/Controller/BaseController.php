<?php

namespace App\Controller;

use App\Entity\AutoBrand;
use App\Entity\Partner;
use App\Entity\Product;
use App\ShoppingCart\ShoppingCart;
use App\Utils\AppVariables;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


Abstract class BaseController extends AbstractController
{
    private $cart;
    private $vars;
    private $promocode;
    public $serialiser;
    public $request;
    public $em;


    public function __construct(ShoppingCart $cart, EntityManagerInterface $entityManager, SessionInterface $session)
    {

        $this->request = new Request($_GET, $_POST);

        /**
         * Промокод вводится либо по ссылке партнера либо при оформлении заказа.
         * Если в ссылке нет промокода, то в сессии параметер промокод либо null либо введенный ранее.
         * После оформления заказа, промокод удаляется из сессии.
         */
        if($this->request->get('promocode')){
            $this->promocode =  $this->request->get('promocode');
            $session->set('promocode', $this->promocode);
        }
        $this->em = $entityManager;
        $this->cart = $cart;
        $this->vars = new AppVariables();
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
//        $normalizer->setCircularReferenceLimit(2);
        $this->serialiser = new Serializer($normalizers, $encoders);
    }

    /**
     * @return SessionInterface
     */
    public function getCart(): ShoppingCart
    {
        return $this->cart;
    }

    /**
     * Возвращает товары принадлежащие указанной категории.
     * Если товар один, то возвращает один товар.
     * @param $categoryId
     * @return Product|object[]
     */
    public function getProducts($categoryId)
    {
       $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findBy(['category'=> $categoryId]);

        if(count($products) == 1){
            return  $products[0];
        }
        return  $products;
    }

    /**
     * Возвращает товары принадлежащие по номеру id.
     * Если товар один, то возвращает один товар.
     * @param $Id
     * @return Product|object[]
     */
    public function getProduct($Id)
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy(['id'=> $Id]);
        return  $products;
    }

    /**
     * Возвращает все автомобильные бренды
     * @return object[]
     */
    public function getBrands()
    {
        return $this->getDoctrine()
            ->getRepository(AutoBrand::class)
            ->findAllOrdered();
    }

    /**
     * Возвращает логотипы
     * @return array
     */
    public function getLogos()
    {
        return  $this->vars->getLogos();
    }

}
