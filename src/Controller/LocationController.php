<?php

namespace App\Controller;

use App\Utils\Dadata;
use App\Utils\Location;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class LocationController extends BaseController
{
    /**
     * @Route("/location/change", name="location", methods={"POST", "GET"})
     */
    public function index(Request $request)
    {
        $city = $request->request->get('city');
        $token = $request->request->get('token');
        if($request->getMethod() == "GET"){
            $city = $request->query->get('city');
            $token = $request->query->get('token');
        }

        if ($this->isCsrfTokenValid('change-city', $token)) {
            $session = new Session();

            $dadata = new Dadata();
            $dadata->init();
            $fields = array("query" => $city, "count" => 1);
            $location = Location::getInstance($session);
            $dadataCity['location'] = $dadata->suggest("address", $fields)['suggestions'][0];

            $location->setLocation($dadataCity);

            echo  json_encode($location->getLocation());
        }
        die;
    }

    /**
     *@Route("/location/search-location", name="search_location", methods={"POST", "GET"})
     * @param Request $request
     */
    public function getCity(Request $request)
    {
        $dadata = new Dadata();
        $dadata->init();
        $searchKey = $request->request->get('search-key');

        $fields = array(
            "query"=> $searchKey,
            "count" => 8,
            "from_bound" => [ "value"=> "city" ],
            "to_bound"=> [ "value" => "city" ] );

        $result = $dadata->suggest("address", $fields);
        echo json_encode($result);die;

    }
}
