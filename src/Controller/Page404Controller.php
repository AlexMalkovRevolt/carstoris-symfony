<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Page404Controller extends AbstractController
{
    /**
     * @Route("/page404/", name="page404")
     */
    public function index()
    {
        return $this->render('page404/index.html.twig', [
            'controller_name' => 'Page404Controller',
            'title' => 'Страница 404',
            'desc' => 'Страница ошибки 404'
        ]);
    }
}
