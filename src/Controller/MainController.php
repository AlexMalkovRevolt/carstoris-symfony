<?php
namespace App\Controller;

use App\Entity\Category;
use App\Entity\OrderRequest;
use App\Entity\Partner;
use App\Entity\Product;
use App\Entity\TUser;
use App\Form\OrderWithDiscountForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MainController extends BaseController
{
    /**
     * @Route("/", name="home")
     * @param OrderWithDiscountForm $discountForm
     *  @param  Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(OrderWithDiscountForm $discountForm, Request $request)
    {


        $orderRequest = new OrderRequest();

        $form = $this->createForm(OrderWithDiscountForm::class, $orderRequest);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $orderRequest = $form->getData();

             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($orderRequest);
             $entityManager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('main/home.html.twig', [
            'title' => 'Аксессуары для Автомобиля, Купить Автоаксессуары по Доступным Ценам, Саквояжи, Коврики, Папки, Накидки | carstoris.ru',
            'products' =>false,
            'orderRequestForm' => $form->createView(),
            'desc' => 'Заказать автоаксессуары для всех моделей автомобилей в интернет-магазина Сarstoris! Саквояжи, 
            коврики, папки, накидки для сидений. В наличии и на заказ. Собственное производство. Высокое качество.'
        ]);
    }
}

