<?php

namespace App\Controller;

use App\Entity\Product;
use App\Utils\AppVariables;
use Symfony\Component\Routing\Annotation\Route;

class MantleController extends BaseController
{
    /**
     * categoryId = 2
     * @Route("/nakidki/", name="mantles")
     */
    public function index()
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findBy([
                'category' => 2
            ],
                ['id'=>'ASC']
            );

//        dd($products);
        $related = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy([
                'id' => 101
            ]);
        $slug = 'mantle';
       $title = 'Накидки на Сиденье Автомобиля из Алькантары, Купить по Низкой Цене | carstoris.ru';
        $desc = 'Купить по доступной цене накидки из Алькантары в интернет-магазина Сarstoris! Изготовим, упакуем и 
        отправим заказ в течение 48 часов. Огромный выбор цветов материала и окантовки. Собственное производство. Отзывы на сайте. Звоните: +7 (927) 241-05-74';
        $breadcrumb = 'Накидки';
        return $this->render('product/index.html.twig', [
            'products' => $products,
            'brands' => $this->getBrands(),
            'logos' => $this->getLogos(),
            'slug' => $slug,
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
            'related' => $related,
            'related_slag' => 'bags',
            'first_product' => $products[1],
            'isDev' => $_ENV['APP_ENV'] == 'dev'
        ]);
    }
}
