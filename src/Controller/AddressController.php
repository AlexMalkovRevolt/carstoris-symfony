<?php

namespace App\Controller;

use App\Utils\Dadata;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AddressController extends AbstractController
{
    /**
     * @Route("/address/get_cords/{city}", name="address_cords")
     * @param Request $request
     * @param $city
     */
    public function index(Request $request, $city)
    {
        $submittedToken = $request->request->get('token');
        if ($this->isCsrfTokenValid('address', $submittedToken)) {
            $dadata = new Dadata();
            $dadata->init();
            $fields = array("query" => $city, "count" => 1);
            $result = $dadata->suggest("address", $fields);
            $cords = [];
            $cords['latitude'] = $result['suggestions'][0]['data']['geo_lat'];
            $cords['longitude'] = $result['suggestions'][0]['data']['geo_lon'];

            echo json_encode($cords);
            $dadata->close();
        }
    }
}
