<?php

namespace App\Controller;

use App\Utils\AppVariables;

use Symfony\Component\Routing\Annotation\Route;

class ForanimalsController extends BaseController
{
    /**
     * categoryId = 6
     * @Route("/dlya-sobak/", name="for_animals")
     */
    public function index()
    {
        $slug = 'foranimals';
        $title = 'Автогамак для перевозки собак в автомобиле на заказ';
        $desc = 'Заказать гамак для собак в машину, купить по выгодной стоимости  автогамак для своего любимого питомца и сохраните в чистоте салон своего автомобиля ';
        $breadcrumb = 'Для животных';
        return $this->render('product/index.html.twig', [
            'products' => $this->getProducts(6),
            'brands' => $this->getBrands(),
            'logos' => $this->getLogos(),
            'slug' => $slug,
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
        ]);
    }
}
