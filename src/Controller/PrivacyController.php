<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PrivacyController extends AbstractController
{
    /**
     * @Route("/politika-konfidencialnosti/", name="privacy")
     */
    public function index()
    {
        $breadcrumb = 'Политика конфиденциальности';
        return $this->render('privacy/index.html.twig', [
            'controller_name' => 'PrivacyController',
            'breadcrumb' => $breadcrumb,
            'title' => $breadcrumb,
            'desc' => "Политика конфидициальности сайта carstoris.ru"
        ]);
    }
}
