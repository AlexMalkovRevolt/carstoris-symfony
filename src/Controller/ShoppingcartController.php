<?php
namespace App\Controller;


use App\Entity\Gift;
use App\Entity\Product;
use App\ShoppingCart\ShoppingCart;
use App\ShoppingCart\ShoppingCartActions;
use App\ShoppingCart\ShoppingCartItem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class ShoppingcartController extends BaseController
{
    /**
     * @Route("/cart/", name="shoppingcart")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
//        dd($this->getCart()->getPartner()->getTuser());die;
//        $this->getCart()->cleanCart();
//        dd($this->getCart());
        // если метод 'POST', получаем данные для добавления в карзину.
        if($request->getMethod() == 'POST'){
            $action = $request->get('action');
            $images = json_decode($request->get('images'), true);
//            var_dump($images);die;
                if(isset($action)){
                    if($action == 'add-item'){
                        try{
                            $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy(
                                [
                               'id' =>  $request->get('product-id')
                                ]
                            );
                            $params = (array) $request->request;
                            $params = array_shift($params);

                            array_pop($params);
                            array_pop($params);
                            $quantity = ($request->get('quantity'))? $request->get('quantity') : 1;
                            $cartItem = new ShoppingCartItem($product, $quantity, $images, $params);
                            $cartItem;
                                $this->getCart()
                                    ->addItem($cartItem);
                        }
                        catch(\Throwable $e){
                            echo $e;
                        }
                    } elseif ($action == 'set-quantity'){
                        $this->getCart()
                            ->getItem(($request->get('item-id')))
                            ->setQuantity($request->get('quantity'));

                    } elseif ( $action == 'decrease-quantity' ){
                        try{
                            $this->getCart()
                                ->getItem(($request->get('item-id')))
                                ->minusQuantity(
                                    $request->get('quantity')
                                );
                            $this->getCart()->setLastEditedItem((int) $request->get('item-id'));
                        }
                        catch(\Throwable $e){
                            echo $e;
                        }

                    }
                    elseif ( $action == 'increase-quantity' ){
                        try{
                            $this->getCart()
                                ->getItem((int) $request->get('item-id'))
                                ->addQuantity(
                                    $request
                                        ->get('quantity')
                                );

                            $this->getCart()->setLastEditedItem((int) $request->get('item-id'));

                        }
                        catch(\Throwable $e){
                            echo $e;
                        }
                    }
                    elseif ($action == 'remove-item'){
                        $this->getCart()->removeItem(
                            $request->get('item-id')
                        );
                    }elseif ($action == 'clean-cart'){
                        $this
                            ->getCart()
                            ->cleanCart();
                    }

                    try{
                        echo $this->serialiser->serialize($this->getCart(), 'json'); die;
                    }catch(\Throwable $e){
                        echo $e;
                    }
                }
                else{
                    die('Упс, что то пошло не так! Попробуйте позже.');
                }
            die;
        }

        $title = 'Корзина покупок';
        $desc = 'На данной странице расположена корзина покупок компании CarStoris';
        $breadcrumb = $title;
        $gifts = $this->getDoctrine()->getRepository(Gift::class)->findAll();

        return $this->render('shoppingcart/shoppingcart.html.twig', [
            'title' => $title,
            'desc' => $desc,
            'breadcrumb' => $breadcrumb,
            'message' => '',
            'gifts' => $gifts,
            'shoppingCart' => $this
                            ->getCart(),
            'shoppingCatItems' => $this
                ->getCart() ->getItems()
        ]);
     }

    /**
     *  @Route("/cart/gift/{id}", name="add-gift", requirements={"id"="\d+"}, methods={"POST", "GET"})
     * @param ShoppingCart $cart
     */
    public function addGift($id)
    {
        $answer = [
            'answer' => 'success',
            'message' => 'Выбранный подарок добавлен'
        ];

            if($this->getCart()->getGift() && $this->getCart()->getGift()->getId() == $id){
                $this->getCart()->removeGift();
                $answer['message'] = 'Подарок удален';

            }else{

                $gift = $this->getDoctrine()->getRepository(Gift::class)->findBy(
                    [
                        'id' => $id,
                        'isActive' => true
                     ]
                );
                if($gift){
                    $this->getCart()->setGift($gift[0]);
                    $answer = [
                        'answer' => 'success',
                        'message' => 'Выбранный подарок добавлен'
                    ];
                }
                else{
                    $answer = [
                        'answer' => 'error',
                        'message' => 'К сожалению, такого подарка не существует! :((('
                    ];

                }
            }

            $answer['cartQuantity'] = $this->getCart()->getItemsQuantity();
             echo json_encode($answer);die;
     }
     
}
