<?php

namespace App\Controller;

use App\Utils\MailNotificator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\NamedAddress;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\RouterInterface;

class MailerController extends AbstractController
{
    /**
     * @Route("/mailer/discount_modal", name="modal_get_discount")
     * @param MailerInterface $mailer
     * @param Request $request
     * @param RouterInterface $router
     * @return RedirectResponse
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function index(MailerInterface $mailer, MailNotificator $notificator, Request $request, RouterInterface $router)
    {
        if($request->getMethod() == 'POST'){

            $userName = $request->get('user_name');
            $userPhone = $request->get('user_phone');

            $notificator->sendNotification(
                'Запрос на скидку',
                'email/get-discount.html.twig',
                [
                    'userName' => $userName,
                    'userPhone' => $userPhone
                ]
            );
            echo('SKIDKA'); die;

        }else{
            $response = new RedirectResponse(
                $router
                    ->generate('page404'));

            // Set the response to be processed
           return $response;
        }
    }

    /**
     * @Route("/mailer/wait-for-call", name="wait_for_call")
     */
    public function waitForCall(MailerInterface $mailer, MailNotificator $notificator, Request $request, RouterInterface $router)
    {
        if($request->getMethod() == 'POST'){
            $userName = $request->get('user_name');
            $userPhone = $request->get('user_phone');
            echo $userName . '  ' . $userPhone;
            $notificator->sendNotification(
                'Запрос на обратный звонок',
                '/email/call-me-template.html.twig',
                [
                    'userName' => $userName,
                    'userPhone' => $userPhone
                ]
            );
            echo('ok'); die;
            //        return $this->render('mailer/index.html.twig', [
            //            'controller_name' => 'MailerController',
            //        ]);
        }else{
            $response = new RedirectResponse(
                $router
                    ->generate('page404'));

            // Set the response to be processed
            return $response;
        }
    }
    /**
     * @Route("/mailer/call_me_modal", name="modal_call_me")
     * @param MailerInterface $mailer
     * @param Request $request
     * @param RouterInterface $router
     * @return RedirectResponse
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function callMeModal(MailNotificator $notificator, Request $request, RouterInterface $router)
    {
        if($request->getMethod() == 'POST'){
            $userName = $request->get('user_name');
            $userPhone = $request->get('user_phone');
            $notificator->sendNotification(
                'Запрос на обратный звонок',
                '/email/call-me-template.html.twig',
                [
                    'userName' => $userName,
                    'userPhone' => $userPhone
                ]
            );
            echo('SKIDKA'); die;
            //        return $this->render('mailer/index.html.twig', [
            //            'controller_name' => 'MailerController',
            //        ]);
        }else{
            $response = new RedirectResponse(
                $router
                    ->generate('page404'));

            // Set the response to be processed
            return $response;
        }
    }

    /**
     * @Route("/mailer/order-with-discount", name="order_with_discount")
     * @param MailerInterface $mailer
     * @param Request $request
     * @param RouterInterface $router
     * @return RedirectResponse
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function orderWithDiscount(MailerInterface $mailer, MailNotificator $notificator, Request $request, RouterInterface $router)
    {
        if($request->getMethod() == 'POST'){
            $userName = $request->get('user_name');
            $userPhone = $request->get('user_phone');

            $notificator->sendNotification(
                'Заказ со скидкой',
                '/email/call-me-template.html.twig',
                [
                    'userName' => $userName,
                    'userPhone' => $userPhone
                ]
            );
            echo('SKIDKA'); die;
            //        return $this->render('mailer/index.html.twig', [
            //            'controller_name' => 'MailerController',
            //        ]);
        }else{
            $response = new RedirectResponse(
                $router
                    ->generate('page404'));

            // Set the response to be processed
            return $response;
        }
    }

    /**
     * @Route("/mailer/become_dealer_modal", name="become_dealer")
     * @param MailNotificator $notificator
     * @param Request $request
     * @param RouterInterface $router
     * @return RedirectResponse
     */
    public function becomeDealer(MailNotificator $notificator, Request $request, RouterInterface $router)
    {
        if($request->getMethod() == 'POST'){
            $userName = $request->get('user_name');
            $userPhone = $request->get('user_phone');

            $notificator->sendNotification(
                'Хочу стать дилером',
                '/email/become-dealer-template.html.twig',
                [
                    'userName' => $userName,
                    'userPhone' => $userPhone
                ]
            );
            echo('SKIDKA'); die;
            //        return $this->render('mailer/index.html.twig', [
            //            'controller_name' => 'MailerController',
            //        ]);
        }else{
            $response = new RedirectResponse(
                $router
                    ->generate('page404'));

            // Set the response to be processed
            return $response;
        }

    }

    /**
     * @Route("/mailer/become_partner_modal", name="become_partner")
     * @param MailNotificator $notificator
     * @param Request $request
     * @param RouterInterface $router
     * @return RedirectResponse
     */
    public function becomePartner(MailNotificator $notificator, Request $request, RouterInterface $router)
    {
        if($request->getMethod() == 'POST'){
            $userName = $request->get('user_name');
            $userPhone = $request->get('user_phone');

            $notificator->sendNotification(
                'Хочу стать партнером.',
                '/email/become-partner-template.html.twig',
                [
                    'userName' => $userName,
                    'userPhone' => $userPhone
                ]
            );
            echo('SKIDKA'); die;
            //        return $this->render('mailer/index.html.twig', [
            //            'controller_name' => 'MailerController',
            //        ]);
        }else{
            $response = new RedirectResponse(
                $router
                    ->generate('page404'));

            // Set the response to be processed
            return $response;
        }

    }
}
