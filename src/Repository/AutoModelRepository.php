<?php

namespace App\Repository;

use App\Entity\AutoModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AutoModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method AutoModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method AutoModel[]    findAll()
 * @method AutoModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutoModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AutoModel::class);
    }

     /**
      * @return AutoModel[] Returns an array of AutoModel objects
      */

    public function findAllByBrandAndYear($brandId,$year)
    {
//        $dql = "SELECT model FROM App\Entity\AutoModel model WHERE model.brand = 25  ";
//
//        $query = $this->getEntityManager()->createQuery($dql);

        $db = $this->createQueryBuilder('model')
            ->andWhere('model.productionYear = :year')
            ->andWhere('model.brand = :brand')
            ->setParameter('brand', $brandId)
            ->setParameter('year', $year )
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
        return $db;

    }

    public function findAllByBrand($brandId)
    {
        $db = $this->createQueryBuilder('model')
            ->andWhere('model.brand = :brand')
            ->setParameter('brand', $brandId)
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
        return $db;
}

    /*
    public function findOneBySomeField($value): ?AutoModel
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
