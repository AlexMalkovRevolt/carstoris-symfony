<?php

namespace App\Repository;

use App\Entity\ProductPartnerBonus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProductPartnerBonus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPartnerBonus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPartnerBonus[]    findAll()
 * @method ProductPartnerBonus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPartnerBonusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductPartnerBonus::class);
    }

    // /**
    //  * @return ProductPartnerBonus[] Returns an array of ProductPartnerBonus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductPartnerBonus
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
