<?php

namespace App\Repository;

use App\Entity\Bonus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Bonus|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bonus|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bonus[]    findAll()
 * @method Bonus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BonusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bonus::class);
    }

    /**
     * @param $partner
     * @return \Doctrine\ORM\QueryBuilder
     */

    public function findByPartner($partner)
    {
        $db = $this->createQueryBuilder('b')
            ->andWhere('b.partner = :partner')
//            ->andWhere('b.isPayed IS NULL')
            ->setParameter('partner', $partner)
            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
////            ->getResult()
        ;

        return $db;
    }

    public function findAllAmounts()
    {
        $db = $this->createQueryBuilder('b')
            ->select('b, sum(b.amount)')
            ->groupBy('b.partner')
            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
////            ->getResult()
        ;

        return $db;
    }

    /*
    public function findOneBySomeField($value): ?Bonus
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
