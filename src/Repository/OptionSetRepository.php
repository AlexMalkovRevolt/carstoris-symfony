<?php

namespace App\Repository;

use App\Entity\OptionSet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OptionSet|null find($id, $lockMode = null, $lockVersion = null)
 * @method OptionSet|null findOneBy(array $criteria, array $orderBy = null)
 * @method OptionSet[]    findAll()
 * @method OptionSet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OptionSetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OptionSet::class);
    }

    // /**
    //  * @return OptionSet[] Returns an array of OptionSet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OptionSet
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
