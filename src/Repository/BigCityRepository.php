<?php

namespace App\Repository;

use App\Entity\BigCity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BigCity|null find($id, $lockMode = null, $lockVersion = null)
 * @method BigCity|null findOneBy(array $criteria, array $orderBy = null)
 * @method BigCity[]    findAll()
 * @method BigCity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BigCityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BigCity::class);
    }

    // /**
    //  * @return BigCity[] Returns an array of BigCity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BigCity
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
