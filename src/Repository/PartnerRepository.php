<?php

namespace App\Repository;

use App\Entity\Partner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Partner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Partner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Partner[]    findAll()
 * @method Partner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartnerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Partner::class);
    }

//     /**
//      * @return Partner[] Returns an array of Partner objects
//      */
//
//    public function findAllAmounts()
//    {
//        return $this->createQueryBuilder('p')
//            ->select('sum(p.amount)')
//            ->andWhere('p.exampleField = :val')
//            ->orderBy('p.id', 'ASC')
//            ->groupBy('p.')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }



    public function findOneByUserId($id)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.tuser = :id')
            ->setParameter('id', $id)
            ->getQuery()
        ;
    }


    /*
    public function findOneBySomeField($value): ?Partner
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
