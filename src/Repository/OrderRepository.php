<?php

namespace App\Repository;

use App\Entity\TOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method TOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method TOrder[]    findAll()
 * @method TOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TOrder::class);
    }

     /**
      * @return Order[] Returns an array of Order objects
      */

    public function findByExampleField($partnerId)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.partner_id = :partner')
            ->setParameter('partner', $partnerId)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
