var Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
// directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if your JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/app.js')
    //.addEntry('page1', './assets/js/page1.js')
    //.addEntry('page2', './assets/js/page2.js')

    .addEntry('popper', './assets/js/popper.min.js')
    .addEntry('bootstrap-js', './assets/js/bootstrap.min.js')
    .addEntry('md-bootstrap-js', './assets/js/mdb.min.js')
    .addEntry('md-datatables-js', './assets/js/addons/datatables.min.js')
    // .addEntry('swiper-js', './assets/js/swiper.min.js')
    .addEntry('custom-js', './assets/js/custom.js')
    .addEntry('map-js', './assets/js/map.js')
    .addEntry('order-js', './assets/js/order.js')
    .addEntry('masked-input-js', './assets/js/jquery.maskedinput.js')
    .addEntry('multiselect-js', './assets/js/multiselect.js')
    .addEntry('carpets-js', './assets/js/templates-js/carpets.js')
    .addEntry('bags-js', './assets/js/templates-js/bags.js')
    .addEntry('mantle-js', './assets/js/templates-js/mantle.js')
    .addEntry('folders-js', './assets/js/templates-js/folders.js')
    .addEntry('foranimals-js', './assets/js/templates-js/foranimals.js')
    .addEntry('forkids-js', './assets/js/templates-js/forkids.js')
    .addEntry('infopages-js', './assets/js/templates-js/infopages.js')
    .addEntry('admin-js', './assets/js/templates-js/admin.js')
    .addEntry('partner-js', './assets/js/templates-js/partner.js')
    .addEntry('suggestions-js', './assets/js/jquery.suggestions.min.js')
    .addEntry('splitlines-js', './assets/js/jquery.splitlines.js')
    .addEntry('instagramFeedJs', './assets/js/modules/InstagramFeed.js')

    .addStyleEntry('page404', './assets/scss/page404.scss')
    .addStyleEntry('bootstrap-style', './assets/css/bootstrap.min.css')
    .addStyleEntry('md-bootstrap-style', './assets/css/mdb.min.css')
    .addStyleEntry('md-datatables-style', './assets/css/addons/datatables.min.css')
    .addStyleEntry('custom-style', './assets/scss/style.scss')
    .addStyleEntry('swiper-style', './assets/css/swiper.min.css')
    .addStyleEntry('customFont', './assets/font/carstoris/styles.css')
    // .addEntry('yMap', 'https://api-maps.yandex.ru/2.1/?apikey=be89b249-4e22-4842-9992-1e6787b5c742&lang=ru_RU&mode=debug')

    // When enabled, Webpack "splits" your files into smaller pieces for greater optimization.
    .splitEntryChunks()

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()
Encore
    .enableSassLoader()
    .autoProvidejQuery()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })

// enables Sass/SCSS support
//.enableSassLoader()

// uncomment if you use TypeScript
//.enableTypeScriptLoader()

// uncomment to get integrity="..." attributes on your script & link tags
// requires WebpackEncoreBundle 1.4 or higher
//.enableIntegrityHashes(Encore.isProduction())

// uncomment if you're having problems with a jQuery plugin
//.autoProvidejQuery()

// uncomment if you use API Platform Admin (composer req api-admin)
//.enableReactPreset()
//.addEntry('admin', './assets/js/admin.js')
;

module.exports = Encore.getWebpackConfig();

