<?php

use EasyCorp\Bundle\EasyDeployBundle\Deployer\DefaultDeployer;

return new class extends DefaultDeployer
{
    public function configure()
    {
        return $this->getConfigBuilder()
            // SSH connection string to connect to the remote server (format: user@host-or-IP:port-number)
            ->server('root@176.53.161.124')
            // the absolute path of the remote server directory where the project is deployed
            ->deployDir('/var/www/test-onpromo.ru/data/www/carstoris.test-onpromo.ru')
            // the URL of the Git repository where the project code is hosted
            ->repositoryUrl('git@bitbucket.org:dameerv/carstoris-symfony.git')
            // the repository branch to deploy
            ->repositoryBranch('backend')
        ;
    }

    // run some local or remote commands before the deployment is started
    public function beforeStartingDeploy()
    {

//        $this->runRemote('./bin/console app:generate-xml-sitemap --env=dev');
        // $this->runLocal('./vendor/bin/simple-phpunit');
    }

    // run some local or remote commands after the deployment is finished
    public function beforeFinishingDeploy()
    {
        // $this->runRemote('{{ console_bin }} app:my-task-name');
        // $this->runLocal('say "The deployment has finished."');
    }
};
